import { injectGlobal } from 'styled-components';
import fontExtrabold from './fonts/proxima-nova--extrabold-webfont.woff';
import fontBold from './fonts/proxima-nova--bold-webfont.woff';
import fontSemibold from './fonts/proxima-nova--semibold-webfont.woff';
import fontRegular from './fonts/proxima-nova--regular-webfont.woff';
import fontLight from './fonts/proxima-nova--light-webfont.woff';

// eslint-disable-next-line
injectGlobal`
  @font-face {
    font-family: 'Proxima';
    font-style:  normal;
    font-weight: 800;
    font-display: swap;
    src: url(${fontExtrabold}) format('woff');
  }
  @font-face {
    font-family: 'Proxima';
    font-style:  normal;
    font-weight: 700;
    font-display: swap;
    src: url(${fontBold}) format('woff');
  }
  @font-face {
    font-family: 'Proxima';
    font-style:  normal;
    font-weight: 600;
    font-display: swap;
    src: url(${fontSemibold}) format('woff');
  }
  @font-face {
    font-family: 'Proxima';
    font-style:  normal;
    font-weight: 400;
    font-display: swap;
    src: url(${fontRegular}) format('woff');
  }
  @font-face {
    font-family: 'Proxima';
    font-style:  normal;
    font-weight: 300;
    font-display: swap;
    src: url(${fontLight}) format('woff');
  }

  @keyframes slideUpHeader {
    from {
      top: 0;
    }
    to {
      top: -50px;
    }
  }
  @keyframes slideDownHeader {
    from {
      top: -50px;
    }
    to {
      top: 0;
    }
  }

  html {
    box-sizing: border-box;
    height: 100%;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }

  body {
    height: 100%;
    min-height: 100%;
    font-family: 'Proxima', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Droid Sans', 'Helvetica Neue', sans-serif;
    font-size: 15px;
    font-weight: 400;
    color: #333;
     
  }

  #root { height: 100%; }

  /* HEADING */
  h1 {
    font-weight: 800;
    font-size: 41px;
    line-height: 50px;
  }
  h2 {
    font-weight: 700;
    font-size: 36px;
    line-height: 44px;
  }
  h3 {
    font-weight: 700;
    font-size: 32px;
    line-height: 39px;
  }
  h4 {
    font-weight: 700;
    font-size: 28px;
    line-height: 34px;
  }
  h5 {
    font-weight: 700;
    font-size: 24px;
    line-height: 29px;
  }
  h6 {
    font-weight: 600;
    font-size: 20px;
    line-height: 24px;
  }

  a {
    color: #bf0016;
    text-decoration: none;
  }

  iframe {
    max-width: initial !important;
  }
`;
