import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import store from './stores';
import './globalStyle';

import registerServiceWorker from './registerServiceWorker';
import asyncComponent from './AsyncComponent';

const Routes = asyncComponent(() => import('./routes'));

render(
  // eslint-disable-next-line
  <Provider store={store}>
    <Routes />
  </Provider>,
  document.getElementById('root'),
);

registerServiceWorker();
