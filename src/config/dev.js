// Application-only config for DEVELOPMENT environment.
import baseConfig from './base';

const finalConfig = Object.assign(baseConfig, {
  apis: {
    facebook: {
      appId: '1279730752073945',
    },
    google: {
      appId: '202986039989-3870mh7c08hu3t6env97o8lvcvnte76p.apps.googleusercontent.com',
    },
    laravel: {
      url: 'https://id.techinasia.com/apis/2.0/',
    },
    wordpress: {
      url: 'https://id.techinasia.com/wp-json/techinasia/2.0',
      ajax: 'https://id.techinasia.com/core/wp-admin/admin-ajax.php',
      nlp: true,
    },
    restapi: {
      url: 'https://id.techinasia.com/wp-json/techinasia/3.0',
      nlp: true,
    },
    tunasdata: {
      url: 'https://tunasdata.techinasia.com',
    },
    comment: {
      url: 'https://id.techinasia.com',
    },
    nodejs: {
      url: 'https://id.techinasia.com',
    },
    search: {
      url: 'https://apiid.techinasia.com/v4.0',
    },
  },
  ga: {
    trackId: 'UA-44046755-3',
  },
  bugsnag: {
    apiKey: '5f111a7bd58c46e3deee07c4b0bfce0e',
    releaseStage: 'staging',
  },
  stream: {
    key: '4zr7rtcbpm8j',
    appId: '16020',
  },
});

// finalConfig.analytics['Tech in Asia'] = {
//   rootUrl: 'https://fp.techinasia.com/dev',
// };

export default finalConfig;
