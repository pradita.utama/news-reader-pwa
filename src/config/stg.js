// Application-only config for STAGING environment.
import baseConfig from './base';

const finalConfig = Object.assign(baseConfig, {
  apis: {
    facebook: {
      appId: '1279730752073945',
    },
    google: {
      appId: '202986039989-3870mh7c08hu3t6env97o8lvcvnte76p.apps.googleusercontent.com',
    },
    laravel: {
      url: 'http://devid.techinasia.com/apis/2.0/',
    },
    wordpress: {
      url: 'http://devid.techinasia.com/wp-json/techinasia/2.0',
      ajax: 'http://devid.techinasia.com/core/wp-admin/admin-ajax.php',
      nlp: true,
    },
    restapi: {
      url: 'http://devid.techinasia.com/wp-json/techinasia/3.0',
    },
    tunasdata: {
      url: 'https://tunasdata.techinasia.com',
    },
    comment: {
      url: 'http://devid.techinasia.com',
    },
    nodejs: {
      url: 'http://devid.techinasia.com:3000',
    },
    search: {
      url: 'https://apiid.techinasia.com/v4.0',
    },
  },
  ga: {
    trackId: 'UA-44046755-3',
  },
  bugsnag: {
    apiKey: '5f111a7bd58c46e3deee07c4b0bfce0e',
    releaseStage: 'staging',
  },
  stream: {
    key: '4zr7rtcbpm8j',
    appId: '16020',
  },
});
/*
finalConfig.analytics['Tech in Asia'] = {
  rootUrl: 'https://fp.techinasia.com/stg',
};
*/

export default finalConfig;
