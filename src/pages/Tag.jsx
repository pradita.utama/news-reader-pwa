import React from 'react';

import asyncComponent from '../AsyncComponent';
import TagPostList from '../modules/TagPostList';


const ErrorBoundary = asyncComponent(() => import('../commons/errors/ErrorBoundary'));

const Tag = props => (
  <React.Fragment>
    <ErrorBoundary>
      <TagPostList {...props} />
    </ErrorBoundary>
  </React.Fragment>
);

export default Tag;
