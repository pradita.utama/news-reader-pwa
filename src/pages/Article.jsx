import React from 'react';

import asyncComponent from '../AsyncComponent';
import ArticlePost from '../modules/ArticlePost';

const ErrorBoundary = asyncComponent(() => import('../commons/errors/ErrorBoundary'));

const Article = state => (
  <ErrorBoundary>
    <ArticlePost {...state} />
  </ErrorBoundary>
);

export default Article;
