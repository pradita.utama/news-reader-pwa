import React from 'react';
import ErrorBoundary from '../commons/errors/ErrorBoundary';


const Dummy = props => (
  <ErrorBoundary>
    <div>{console.log(props)}
      <h1>Dummy</h1>
    </div>
  </ErrorBoundary>
);


export default Dummy;
