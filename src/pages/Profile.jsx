import React from 'react';

import asyncComponent from '../AsyncComponent';

const Profile = asyncComponent(() => import('../modules/Profile'));
const EditProfile = asyncComponent(() => import('../modules/Profile/EditProfile'));
const ErrorBoundary = asyncComponent(() => import('../commons/errors/ErrorBoundary'));

const ProfilePage = (state) => {
  const isEdit = state.match.path;

  return (
    <ErrorBoundary>
      {
        isEdit === '/profile/:name/edit'
          ? <EditProfile {...state} />
          : <Profile {...state} />
      }
    </ErrorBoundary>
  );
};

export default ProfilePage;
