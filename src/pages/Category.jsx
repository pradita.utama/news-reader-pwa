import React from 'react';

import asyncComponent from '../AsyncComponent';
import CategoryPostList from '../modules/CategoryPostList';


const ErrorBoundary = asyncComponent(() => import('../commons/errors/ErrorBoundary'));

const Category = props => (
  <React.Fragment>
    <ErrorBoundary>
      <CategoryPostList {...props} />
    </ErrorBoundary>
  </React.Fragment>
);

export default Category;
