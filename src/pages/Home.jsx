import React from 'react';

import asyncComponent from '../AsyncComponent';
import { renderDefaultHelmet } from '../utils/helmetUtils';


const PostList = asyncComponent(() => import('../modules/PostList'));
const ErrorBoundary = asyncComponent(() => import('../commons/errors/ErrorBoundary'));
const FeaturedStories = asyncComponent(() => import('../modules/FeaturedStories'));

const Home = () => (
  <React.Fragment>
    <ErrorBoundary>
      <div>
        <FeaturedStories />
        <PostList isHome />
      </div>
    </ErrorBoundary>
    {renderDefaultHelmet()}
  </React.Fragment>
);

export default Home;
