import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const Container = styled.div`
  padding: 15px;
  margin-bottom: 50px;

  img {
    width: 100%
    height: auto;
  }

  h1 {
    font-size: 18px;
    line-height: 22px;
    font-weight: 600;
    margin: 5px 0 20px 0;
    text-decoration: none;
    color: #000;
  }
`;

const eventsData = [
  {
    source: 'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2018/03/tia-pdc-2017-1-featured-img.jpg',
    title: 'Product Development Conference 2018',
    link: 'https://pdc.techinasia.com',
  },
  {
    source: 'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2017/07/TIA-JAKARTA-2017-1for1-feature-image.png',
    title: 'Jakarta 2018',
    link: 'https://www.techinasia.com/events',
  },
  {
    source: 'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2018/03/1200x675-changes.png',
    title: 'Singapore 2018',
    link: 'https://www.techinasia.com/events',
  },
];

const Events = () =>
  (
    <Container>
      {
        eventsData.map((event, index) => (
          <Link key={index.toString()} to={event.link} target="_blank">
            <div>
              <img src={event.source} alt={event.title} />
              <h1>{event.title}</h1>
            </div>
          </Link>
        ))
      }
    </Container>
  );

export default Events;
