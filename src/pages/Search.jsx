import React from 'react';

import asyncComponent from '../AsyncComponent';
import SearchPostList from '../modules/SearchPostList';

const ErrorBoundary = asyncComponent(() => import('../commons/errors/ErrorBoundary'));

const Search = state => (
  <React.Fragment>
    <ErrorBoundary>
      <SearchPostList {...state} />
    </ErrorBoundary>
  </React.Fragment>
);

export default Search;
