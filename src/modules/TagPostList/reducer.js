import CONSTANT from './constant';
import { startLoading, finishLoading, errorLoading } from '../../utils/reducerUtils';

const initialState = {
  posts: null,
};

const tagPostsReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANT.REQUEST_TAG_POSTS: {
      return startLoading(state);
    }
    case CONSTANT.REQUEST_TAG_POSTS_ERROR: {
      return errorLoading(state, action);
    }
    case CONSTANT.REQUEST_TAG_POSTS_SUCCESS: {

      const finish = finishLoading(state);
      return {
        ...finish,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
};

export { tagPostsReducer };
