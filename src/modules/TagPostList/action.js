import { getPosts } from './api';
import CONSTANT from './constant';

const requestTagPosts = tag => ({
  type: CONSTANT.REQUEST_TAG_POSTS,
  payload: {
    promise: getPosts('tags', tag),
  },
});

export { requestTagPosts };
