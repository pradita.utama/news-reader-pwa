import call from '../../commons/api/RestApi';

const getPosts = (type, slug) => call(`/${type}/${slug}/posts?page=1&per_page=20`);

export {
  getPosts,
};
