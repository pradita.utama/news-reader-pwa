import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// import Helmet from 'react-helmet';
// import LazyLoad from 'react-lazyload';

import ErrorPage from 'commons/errors/ErrorPage';
import EmptyFeed from 'commons/errors/EmptyFeed';
import Loading from 'commons/loading/LoadingPreviewArticle';
import { requestTagPosts } from './action';
import PostTemplate from '../PostList/components/PostTemplate';


class TagPostList extends React.Component {
  static propTypes = {
    location: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    requestTagPosts: PropTypes.func.isRequired,
    tagPostsReducer: PropTypes.object.isRequired,
  };

  componentDidMount() {
    window.scrollTo(0, 0);
    console.log(this.props);
    const { match } = this.props;

    this.props.requestTagPosts(match.params.tag);
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.onRouteChanged();
    }
  }

  onRouteChanged = () => {
    const { match } = this.props;
    this.props.requestTagPosts(match.params.tag);
  }

  render() {
    const {
      posts,
      isLoading,
      hasError,
      isLoaded,
    } = this.props.tagPostsReducer;

    if (isLoading) {
      return <Loading />;
    }

    if (hasError) {
      return <ErrorPage />;
    }

    return isLoaded === true && (
      <React.Fragment>
        {
          posts !== null ? posts.map(post => (
            <PostTemplate key={post.id.toString()} {...post} />
          )) : <EmptyFeed />
        }
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ tagPostsReducer }) => ({
  tagPostsReducer,
});

export default connect(mapStateToProps, { requestTagPosts })(TagPostList);
