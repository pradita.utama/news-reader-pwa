import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Loading from 'commons/loading/LoadingPreviewArticle';
import { requestCategoryPostList } from 'modules/PostList/action';
import PostTemplate from 'modules/PostList/components/PostTemplate';

class CategoryPostList extends React.Component {
  static propTypes = {
    categoryPostListReducer: PropTypes.shape({
      posts: PropTypes.arrayOf(PropTypes.object),
      isLoaded: PropTypes.bool,
      isLoading: PropTypes.bool,
      hasError: PropTypes.bool,
    }).isRequired,
    requestCategoryPostList: PropTypes.func.isRequired,
    currentPage: PropTypes.number,
    perPage: PropTypes.number,
    isLoading: PropTypes.bool,
  };

  static defaultProps = {
    currentPage: 1,
    perPage: 20,
    isLoading: false,
  }

  // constructor(props) {
  //   super(props);
  //   this.handleScroll = this.handleScroll.bind(this);
  // }

  componentDidMount() {
    // window.addEventListener('scroll', this.handleScroll);

    this.props.requestCategoryPostList('startups', 1, 20);
  }

  // componentWillUnmount() {
  //   window.removeEventListener('scroll', this.handleScroll);
  // }

  // isBottom = el => el.getBoundingClientRect().bottom <= window.innerHeight;

  // handleScroll = () => {
  //   const wrappedElement = document.getElementById('container');
  //   if (this.isBottom(wrappedElement)) {
  //     console.log('header bottom reached');
  //     document.removeEventListener('scroll', this.trackScrolling);
  //   }
  // };

  loadMorePosts = () => {
    const { currentPage, perPage, isLoading } = this.props;

    if (isLoading) {
      return;
    }

    this.props.requestCategoryPostList('startups', currentPage + 1, perPage);
  }

  uniqueID = () => Math.random().toString(36).substr(2, 9);

  render() {
    const { posts, isLoading } = this.props.categoryPostListReducer;

    return (
      <React.Fragment>
        {posts.map(post => <PostTemplate key={this.uniqueID()} {...post} />)}
        {isLoading ? <Loading /> : null}
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ categoryPostListReducer }) => ({
  categoryPostListReducer,
});

export default connect(mapStateToProps, { requestCategoryPostList })(CategoryPostList);

