import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';


import PostTemplate from '../PostList/components/PostTemplate';
import { Item, StyledModal, FlexContainer, FlexWrapper, SearchTag, SearchQuery, ResultCount } from './style';
import BackButton from '../../commons/ui-kit/Button/IconButton/BackButton';
import SearchButton from '../../commons/ui-kit/Button/IconButton/SearchButton';
import LoadingBar from './LoadingBar';
import Paginator from '../Paginator';
import ErrorPage from '../../commons/errors/ErrorPage';
import EmptySearch from '../../commons/errors/EmptySearch';


import searchPosts from './action';
import { getQueryFromURL, uniqueID } from './utils';


// eslint-disable-next-line
class SearchPostList extends React.Component {
  static propTypes = {
    searchPosts: PropTypes.func.isRequired,
    searchReducer: PropTypes.object,
    activeRequests: PropTypes.number,
    location: PropTypes.object,
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const PrevQuery = prevState.query;
    const NextQuery = getQueryFromURL(nextProps);

    if (PrevQuery !== NextQuery) {
      return {
        query: NextQuery.keyword,
      };
    }
    return null;
  }

  state = {
    query: '',
  }

  componentDidMount() {
    const query = getQueryFromURL(this.props);
    this.props.searchPosts(query.keyword, 1);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.query !== prevState.query) {
      this.props.searchPosts(this.state.query, 1);
    }
  }

  handleResultCount = () => {
    const { total } = this.props.searchReducer;
    if (!total) {
      return '';
    }
    return (
      <ResultCount>
        {total.toLocaleString()} hasil ditemukan
      </ResultCount>
    );
  }
  handlePaginator = () => {
    const { total } = this.props.searchReducer;
    const query = getQueryFromURL(this.props);
    if (!total) {
      return '';
    }
    return <Paginator defaultQuery={query.keyword} {...this.props} />;
  }

  handleResult = () => {
    const { searchReducer: { items, activeRequests, hasError } } = this.props;
    if (activeRequests) {
      return <LoadingBar progress={activeRequests} />;
    }

    if (hasError) {
      return <ErrorPage />;
    }

    if (items.length === 0) {
      return <EmptySearch />;
    }

    const results = items.slice(0, 5);

    return results.map(item => <PostTemplate isSearch key={uniqueID()} {...item} />);
  }

  handleQuery = () => {
    const query = getQueryFromURL(this.props);
    if (query.keyword.length > 25) {
      const res = query.keyword.slice(0, 22);
      return `${res}...`;
    }

    return query.keyword;
  }


  render() {
    const {
      handleResult, handleResultCount, handlePaginator, handleQuery,
    } = this;

    return (
      <React.Fragment>
        <StyledModal
          isOpen
          closeTimeoutMS={250}
        >
          <Item id="pagination-scroll-to-this">
            <FlexContainer>
              <FlexWrapper>
                <BackButton {...this.props} />
              </FlexWrapper >

              <FlexWrapper wide>
                <SearchTag>Pencarian</SearchTag>
                <SearchQuery>{handleQuery()}</SearchQuery>
              </FlexWrapper >
              <FlexWrapper>
                <SearchButton {...this.props} />
              </FlexWrapper >

            </FlexContainer>
          </Item>
          {handleResultCount()}
          {handleResult()}
          {handlePaginator()}
        </StyledModal>
      </React.Fragment>
    );
  }
}

SearchPostList.defaultProps = {
  searchReducer: null,
  activeRequests: null,
  location: null,
};

const mapStateToProps = ({ searchReducer }) => ({
  searchReducer,
});

export default connect(mapStateToProps, { searchPosts })(SearchPostList);
