const getQueryFromURL = (props) => {
  const { keyword, page } = props.match.params;
  return {
    keyword,
    page,
  };
};

const uniqueID = () => Math.random().toString(36).substr(2, 9);

export {
  getQueryFromURL,
  uniqueID,
};
