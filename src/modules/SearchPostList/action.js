// Action creators for single Post page.

import debounce from 'lodash/debounce';
import CONSTANTS from './constants';
import api from './api';

const fetchSearchPosts = debounce((dispatch, query, page) => {
  dispatch({
    type: CONSTANTS.REQUEST_SEARCH,
    payload: {
      promise: api.searchPosts(query, page),
      query,
    },
  });
}, 500);

function searchPosts(query, page) {
  return (dispatch) => {
    // Force update the UI to the new query.
    dispatch({
      type: CONSTANTS.REQUEST_SEARCH,
      payload: { query, page },
    });

    // Cancels any existing debounce functions to prevent flooding of the API.
    fetchSearchPosts.cancel();

    // Call again.
    fetchSearchPosts(dispatch, query, page);
  };
}

export default searchPosts;

