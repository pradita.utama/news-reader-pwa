import call from '../../commons/api/SearchApi';

const api = {
  searchPosts: (query, page) => call('/search/', 'GET', { query, page }),
};

export default api;
