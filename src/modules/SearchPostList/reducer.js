// Search reducer
import selectn from 'selectn';
import CONSTANTS from './constants';
import * as ReducerUtils from '../../utils/reducerUtils';


const initialState = ReducerUtils.appendLoadingStates({
  currentPage: 1,
  perPage: 20,
  items: [],
  query: '',
  time: 0,
  total: 0,
  totalPages: 0,

  // this state is for loading progressbar
  activeRequests: 0,
});

/**
 * Search reducer function.
 *
 * @param  {Object} state  Current state; defaults to the initial state.
 * @param  {Object} action Action payload.
 * @return {Object}        Next state.
 */
export default function searchReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.REQUEST_SEARCH:
      return Object.assign({}, ReducerUtils.startLoading(state), {
        query: action.payload.query,
        activeRequests: state.activeRequests + 1,
      });

    case CONSTANTS.REQUEST_SEARCH_ERROR:
      return Object.assign({}, ReducerUtils.errorLoading(state, action.error));

    case CONSTANTS.REQUEST_SEARCH_SUCCESS: {
      const searchResponse = selectn('payload', action);

      if (!searchResponse) {
        return state;
      }

      return Object.assign({}, ReducerUtils.finishLoading(state), {
        ...searchResponse,
        activeRequests: state.activeRequests - 2,
      });
    }

    default:
      return state;
  }
}
