import React from 'react';
import PropTypes from 'prop-types';
import SimpleLoadingBar from 'react-simple-loading-bar';
import { LoadingContainer, LoadingContent } from './style';

const LoadingBar = props => (
  <LoadingContainer>
    <LoadingContent>
      <SimpleLoadingBar activeRequests={props.progress} color="#C01820" />
      <br />
      <span>
        sedang mencari artikel...
      </span>
    </LoadingContent>
  </LoadingContainer>
);

LoadingBar.defaultProps = {
  progress: null,
};

LoadingBar.propTypes = {
  progress: PropTypes.number,
};


export default LoadingBar;
