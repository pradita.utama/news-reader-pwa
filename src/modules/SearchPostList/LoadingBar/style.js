import styled from 'styled-components';


const LoadingContainer = styled.div`
    width: auto;
    height: 500px;
    background-color: white;
    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.16);
    outline: none;
`;

const LoadingContent = styled.div`
    margin: 15em auto;
    width: 300px;
    text-align: center;

    span{
      color: #474747;
      opacity: 0.5;
      font-size: 12px;
    }
`;

export {
  LoadingContainer,
  LoadingContent,
};
