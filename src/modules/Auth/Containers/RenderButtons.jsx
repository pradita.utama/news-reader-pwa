import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Icon from 'commons/ui-kit/Icon';
import { facebookSignOn, googleSignOn } from '../action';
import { FacebookButton, GoogleButton, ButtonText } from '../style';

const IconFacebook = () => <Icon name="facebook" />;

const IconGooglePlus = () => <Icon name="google-plus" />;

const RenderButtons = (props) => {
  const stringId = props.type === 'login' ? 'login' : 'register';
  const stringLabel = props.type === 'login' ? 'Log In' : 'Sign Up';
  const stringButtonText = props.type === 'login' ? 'login' : 'sign up';

  return (
    <React.Fragment>
      <FacebookButton
        id={`${stringId}Facebook`}
        size="medium"
        label={`${stringLabel} with Facebook`}
        iconWithLabel={IconFacebook}
        onClick={props.facebookSignOn}
      />
      <GoogleButton
        id={`${stringId}Google`}
        size="medium"
        label={`${stringLabel} with Google`}
        iconWithLabel={IconGooglePlus}
        onClick={props.googleSignOn}
      />

      <ButtonText onClick={props.toggleForm}>
        {`or ${stringButtonText} using e-mail`}
      </ButtonText>
    </React.Fragment>
  );
};

RenderButtons.propTypes = {
  type: PropTypes.string.isRequired,
  toggleForm: PropTypes.func.isRequired,
  facebookSignOn: PropTypes.func.isRequired,
  googleSignOn: PropTypes.func.isRequired,
};

export default connect(null, {
  facebookSignOn,
  googleSignOn,
})(RenderButtons);
