import React from 'react';
import { withFormik, Form, Field } from 'formik';
import { object, string } from 'yup';
import { connect } from 'react-redux';

import Button from 'commons/ui-kit/Button/BaseButton';
import { register } from '../action';
import { StyledForm, InputWrapper, ErrorWrapper } from '../style';

const NormalForm = ({
  // eslint-disable-next-line
  values, errors, touched, className, errorUser
}) => (
  <Form className={className}>
    <InputWrapper value={values.firstName}>
      <Field name="firstName" />
      <label>First Name</label>
      { touched.firstName && errors.firstName && (
        <ErrorWrapper>{ errors.firstName }</ErrorWrapper>
      )}
    </InputWrapper>

    <InputWrapper value={values.lastName}>
      <Field name="lastName" />
      <label>Last Name</label>
      { touched.lastName && errors.lastName && (
        <ErrorWrapper>{ errors.lastName }</ErrorWrapper>
      )}
    </InputWrapper>

    <InputWrapper value={values.email}>
      <Field name="email" />
      <label>Email</label>
      { touched.email && errors.email && (
        <ErrorWrapper>{ errors.email }</ErrorWrapper>
      )}
    </InputWrapper>

    <InputWrapper value={values.password}>
      <Field type="password" name="password" />
      <label>Password</label>
      { touched.password && errors.password && (
        <ErrorWrapper>{ errors.password }</ErrorWrapper>
      )}

      { errorUser && (
        <ErrorWrapper>{ errorUser.message }</ErrorWrapper>
      )}
    </InputWrapper>

    <Button primary type="submit" size="medium" label="Submit" />
  </Form>
);

const FormWithStyle = StyledForm(NormalForm);

const FormRegister = withFormik({
  mapPropsToValues: () => ({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
  }),
  validationSchema: object().shape({
    firstName: string()
      .required('First Name is required'),
    lastName: string()
      .required('Last Name is required'),
    email: string()
      .email('Invalid email address')
      .required('Email is required!'),
    password: string()
      .required('Password is required'),
  }),
  handleSubmit: (values, { props }) => {
    props.register(
      values.firstName,
      values.lastName,
      values.email,
      values.password,
    );
  },
  // helps with React DevTools
  displayName: 'FormRegister',
})(FormWithStyle);

const mapStateToProps = ({ authReducer }) => ({
  errorUser: authReducer.error,
});

export default connect(mapStateToProps, { register })(FormRegister);
