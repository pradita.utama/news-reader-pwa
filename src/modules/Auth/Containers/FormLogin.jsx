import React from 'react';
import { withFormik, Form, Field } from 'formik';
import { object, string } from 'yup';
import { connect } from 'react-redux';

import Button from 'commons/ui-kit/Button/BaseButton';
import { checkLogin } from '../action';
import { StyledForm, InputWrapper, ErrorWrapper } from '../style';

const NormalForm = ({
  // eslint-disable-next-line
  values, errors, touched, className, errorUser
}) => (
  <Form className={className}>
    <InputWrapper value={values.email}>
      <Field type="email" name="email" />
      <label>Email</label>
      { touched.email && errors.email && (
        <ErrorWrapper>{ errors.email }</ErrorWrapper>
      )}
    </InputWrapper>

    <InputWrapper value={values.password}>
      <Field type="password" name="password" />
      <label>Password</label>
      { touched.password && errors.password && (
        <ErrorWrapper>{ errors.password }</ErrorWrapper>
      )}

      { errorUser && (
        <ErrorWrapper>{ errorUser.message }</ErrorWrapper>
      )}
    </InputWrapper>

    <Button primary type="submit" size="medium" label="Submit" />
  </Form>
);

const FormWithStyle = StyledForm(NormalForm);

const FormLogin = withFormik({
  mapPropsToValues: () => ({
    email: '',
    password: '',
  }),
  validationSchema: object().shape({
    email: string()
      .email('Invalid email address')
      .required('Email is required!'),
    password: string()
      .required('Password is required'),
  }),
  handleSubmit: (values, { props }) => {
    props.checkLogin(values.email, values.password);
  },
  // helps with React DevTools
  displayName: 'FormLogin',
})(FormWithStyle);

const mapStateToProps = ({ authReducer }) => ({
  errorUser: authReducer.error,
});

export default connect(mapStateToProps, { checkLogin })(FormLogin);
