import React from 'react';
import PropTypes from 'prop-types';

import { ButtonText } from '../style';
import FormRegister from './FormRegister';
import FormLogin from './FormLogin';

// eslint-disable-next-line
class RenderForm extends React.Component {

  render() {
    const { type, toggleForm } = this.props;
    const stringBackTo = type === 'login' ? 'Log In' : 'Sign Up';

    return (
      <React.Fragment>
        <ButtonText onClick={toggleForm}>
          {`Back for ${stringBackTo} using Social Media`}
        </ButtonText>

        { type === 'login'
          ? <FormLogin />
          : <FormRegister />
        }
      </React.Fragment>
    );
  }
}

RenderForm.propTypes = {
  type: PropTypes.string.isRequired,
  toggleForm: PropTypes.func.isRequired,
};

export default RenderForm;
