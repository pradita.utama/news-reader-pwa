import React from 'react';
import { withFormik, Form, Field } from 'formik';
import { object, string } from 'yup';
import { connect } from 'react-redux';

import Button from 'commons/ui-kit/Button/BaseButton';
import { resetPasswordEmail } from '../action';
import { StyledForm, InputWrapper, ErrorWrapper, ButtonText } from '../style';

const NormalForm = ({
  // eslint-disable-next-line
  values, errors, touched, className, toggleFormReset
}) => (
  <React.Fragment>
    <ButtonText onClick={toggleFormReset}>
      Back to Login
    </ButtonText>

    <Form className={className}>
      <InputWrapper value={values.email}>
        <Field type="email" name="email" />
        <label>Email</label>
        { touched.email && errors.email && (
          <ErrorWrapper>{ errors.email }</ErrorWrapper>
        )}
      </InputWrapper>

      <Button primary type="submit" size="medium" label="Submit" />
    </Form>
  </React.Fragment>
);

const FormWithStyle = StyledForm(NormalForm);

const FormResetPassword = withFormik({
  mapPropsToValues: () => ({
    email: '',
  }),
  validationSchema: object().shape({
    email: string()
      .email('Invalid email address')
      .required('Email is required!'),
  }),
  handleSubmit: (values, { props }) => {
    props.resetPasswordEmail(values.email);
  },
  // helps with React DevTools
  displayName: 'FormResetPassword',
})(FormWithStyle);

export default connect(null, { resetPasswordEmail })(FormResetPassword);
