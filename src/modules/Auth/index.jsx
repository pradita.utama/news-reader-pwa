import React from 'react';
import PropTypes from 'prop-types';

import Icon from 'commons/ui-kit/Icon';
import { Close, StyledModal } from './style';
import ModalLogin from './Components/ModalLogin';
import ModalRegister from './Components/ModalRegister';

class ModalContainer extends React.Component {
  state = {
    openForm: false,
  }

  componentDidMount() {
    this.createScript('https://connect.facebook.net/en_US/sdk.js', 'facebook-jssdk');
    this.createScript('https://smartlock.google.com/client', 'google-script');
  }

  createScript = (url, name) => {
    // eslint-disable-next-line
    (function(doc, script, id) {
      const fjs = doc.getElementsByTagName(script)[0];
      if (doc.getElementById(id)) { return; }
      const js = doc.createElement(script);
      js.id = id;
      js.src = url;
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', name));
  }

  toggleForm = () => {
    this.setState(prevState => ({
      openForm: !prevState.openForm,
    }));
  };

  render() {
    const {
      modalId, modalIsOpen, changeModalId, closeModal,
    } = this.props;
    const { openForm } = this.state;

    return (
      <StyledModal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        contentLabel={modalId}
        closeTimeoutMS={250}
        {...this.props}
      >
        <Close onClick={closeModal}>
          <Icon name="close-grey" />
        </Close>

        { modalId === 'login'
          ? (
            <ModalLogin
              openForm={openForm}
              toggleForm={this.toggleForm}
              changeModalId={changeModalId}
            />
          )
          : (
            <ModalRegister
              openForm={openForm}
              toggleForm={this.toggleForm}
            />
          )
        }
      </StyledModal>
    );
  }
}

ModalContainer.propTypes = {
  modalId: PropTypes.string.isRequired,
  modalIsOpen: PropTypes.bool.isRequired,
  changeModalId: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
};

export default ModalContainer;
