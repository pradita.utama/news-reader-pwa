import call from 'commons/api/RestApi';
import wordpressCall from 'commons/api/WordPressApi';

const api = {
  connectFacebook: token => wordpressCall('/auth-tokens/facebook', 'POST', {
    access_token: token,
  }),

  connectGoogle: token => wordpressCall('/auth-tokens/google', 'POST', {
    id_token: token,
  }),

  getMe: () => call('/users/me'),

  getMyProfile: () => call('/users/me/profile'),

  changePassword: data => wordpressCall('/users/me/password', 'PUT', data),

  resetPasswordEmail: email => wordpressCall('/users/reset-password', 'POST', { email }),

  resetPassword: data => wordpressCall('/users/reset-password', 'PUT', data),

  getProfileById: id => call(`/users/${id}/profile`),

  logout: () => wordpressCall('/auth-tokens', 'DELETE'),

  postLogin: (email, password) => wordpressCall('/auth-tokens', 'POST', {
    username: email,
    password,
  }),

  register: (firstName, lastName, email, password) => wordpressCall('/users', 'POST', {
    first_name: firstName,
    last_name: lastName,
    email,
    password,
  }),

  // registerWithAvatar: data => call('/users', 'POST', data),

  getTalkArticleByStatus: (slug, status, page, perPage) => call(`/users/${slug}/talk`, 'GET', { page, per_page: perPage || 10, status }),
};

export default api;
