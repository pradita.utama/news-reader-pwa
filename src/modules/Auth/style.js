import styled, { css } from 'styled-components';
import ModalAdapter from './Components/ModalAdapter';
import Icon from '../../commons/ui-kit/Icon';
import Button from '../../commons/ui-kit/Button/BaseButton';

const facebookColor = '#3b5998';
const googleColor = '#d8001e';

const Close = styled.button`
  background: transparent;
  border: 0;
  float: right;
`;

const ModalWrapper = styled.div`
  width: 75%;
  min-width: 245px;
  margin: 40px auto 20px;
  text-align: center;
  font-size: 16px;

  p { margin: 0 0 20px; }
`;

const StyledModal = styled(ModalAdapter)`

  &__Overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 100;
    opacity: 0;
    transform: translateY(-10%);
    transition: all 250ms ease-in;
  }

  &__Overlay--after-open {
    opacity: 1;
    transform: translateY(0);
  }

  &__Overlay--before-close {
    opacity: 0;
    transform: translateY(-10%);
  }

  &__Content {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: #fff;
    padding: 20px;
    overflow: auto;
  }
`;

const StyledIcon = styled(Icon)`
  height: 50px;
  width: 245px;
  background-size: 245px auto;
  margin: 0 0 15px;
`;

const FacebookButton = styled(Button)`
  background: ${facebookColor};
  color: #fff;
  width: 100%;
  border-radius: 4px;
  margin-bottom: 15px;
`;

const GoogleButton = FacebookButton.extend`
  background: ${googleColor};
  
  > span {
    background-size: 2.5em;
  }
`;

const ButtonText = styled.button`
  display: block;
  margin: 0 auto;
  padding: 20px 10px;
  font-weight: 600;
  color: #bf0016;
  background: transparent;
  border: 0;
`;

const StyledForm = component => styled(component)`
  width: 100%;
  padding-bottom: 25px;

  input {
    display: block;
    width: 100%;
    height: 40px;
    line-height: 40px;
    border: 0;
    border-bottom: 1px solid #ddd;
    margin: 0 0 5px;
    cursor: text;
    outline: none;
  }

  input + label {
    position: absolute;
    top: 0;
    left: 0;
    color: rgba(0, 0, 0, .38);
    font-weight: 600;
    line-height: 40px;
    transition: all .2s ease-in;
  }

  button {
    width: 80%;
    margin: 0 auto;
    border-radius: 4px;
  }
`;

const InputWrapper = styled.div`
  position: relative;
  padding-bottom: 25px;

  ${props => props.value && css`
    &&& input + label {
      font-size: 12px;
      line-height: 12px;
      color: #18a0e0;
      transform: translateY(-100%);
    }
  `}
`;

const ErrorWrapper = styled.div`
  font-size: 14px;
  text-align: left;
  color: #bf0016;
`;

export {
  Close,
  ModalWrapper,
  StyledModal,
  StyledIcon,
  FacebookButton,
  GoogleButton,
  ButtonText,
  StyledForm,
  InputWrapper,
  ErrorWrapper,
};
