import React from 'react';
import PropTypes from 'prop-types';

import RenderForm from '../Containers/RenderForm';
import RenderButtons from '../Containers/RenderButtons';
import { ModalWrapper, StyledIcon } from '../style';

const ModalRegister = ({ openForm, toggleForm }) => (
  <ModalWrapper>
    <StyledIcon name="tia-logo-full" />
    <p>Sign Up to join the Tech in Asia community</p>

    { openForm
      ? <RenderForm type="register" toggleForm={toggleForm} />
      : <RenderButtons type="register" toggleForm={toggleForm} />
    }
  </ModalWrapper>
);

ModalRegister.propTypes = {
  openForm: PropTypes.bool.isRequired,
  toggleForm: PropTypes.func.isRequired,
};

export default ModalRegister;
