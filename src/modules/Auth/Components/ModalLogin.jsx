import React from 'react';
import PropTypes from 'prop-types';

import Button from 'commons/ui-kit/Button/BaseButton';
import RenderForm from '../Containers/RenderForm';
import RenderButtons from '../Containers/RenderButtons';
import FormResetPassword from '../Containers/FormResetPassword';
import { ModalWrapper, StyledIcon, ButtonText } from '../style';

class ModalLogin extends React.Component {
  state = {
    openFormReset: false,
  }

  toggleFormReset = () => {
    this.setState(prevState => ({
      openFormReset: !prevState.openFormReset,
    }));
  };

  render() {
    const { openForm, toggleForm, changeModalId } = this.props;
    const { openFormReset } = this.state;

    return (
      <ModalWrapper>
        <StyledIcon name="tia-logo-full" />

        { openFormReset
          ? <FormResetPassword toggleFormReset={this.toggleFormReset} />
          : (
            <React.Fragment>
              <p>Log In to join the Tech in Asia community</p>

              { openForm
                ? <RenderForm type="login" toggleForm={toggleForm} />
                : <RenderButtons type="login" toggleForm={toggleForm} />
              }

              <p>
                <strong>Don't have an account?</strong>
              </p>

              <Button
                primary
                size="medium"
                label="Sign Up"
                onClick={() => changeModalId('register')}
              />

              <ButtonText onClick={this.toggleFormReset}>
                Forgot Password?
              </ButtonText>
            </React.Fragment>
          )

        }
      </ModalWrapper>
    );
  }
}

ModalLogin.propTypes = {
  openForm: PropTypes.bool.isRequired,
  toggleForm: PropTypes.func.isRequired,
  changeModalId: PropTypes.func.isRequired,
};

export default ModalLogin;
