import {
  appendLoadingStates,
  resetLoading,
  startLoading,
  finishLoading,
  errorLoading,
} from 'utils/reducerUtils';
import CONSTANT from './constant';
// import initGa from 'analytics/ga';

const initialState = appendLoadingStates({
  user: {},
  userProfile: {},
});

/**
 * Tracks user actions and push to analytics.
 *
 * @param  {Object} user
 * @param  {Object} actionType
 * @return {void}
 *
 *  function trackAction(user, actionType) {
 *    initGa(); // Initialize
 *
 *    switch (actionType) {
 *      case CONSTANT.REGISTER_USER_SUCCESS:
 *        ga('send', 'event', 'SignUpDone');
 *        break;
 *      case CONSTANT.CHECK_LOGIN_SUCCESS:
 *        break;
 *      case CONSTANT.FACEBOOK_SIGNON_SUCCESS:
 *        break;
 *      case CONSTANT.GOOGLE_SIGNON_SUCCESS:
 *        break;
 *      default:
 *        // Do nothing
 *    }
 *  }
 */

/**
 * Auth reducer function.
 *
 * @param  {Object} state  Current state; defaults to the initial state.
 * @param  {Object} action Action payload.
 * @return {Object}        Next state.
 */
const auth = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANT.RESET_LOADING:
      return Object.assign({}, resetLoading(state));

    case CONSTANT.REQUEST_ME:
    case CONSTANT.CHECK_LOGIN:
    case CONSTANT.FACEBOOK_SIGNON:
    case CONSTANT.GOOGLE_SIGNON:
    case CONSTANT.REQUEST_LOGOUT:
    case CONSTANT.REGISTER_USER:
      return Object.assign({}, startLoading(state));

    case CONSTANT.REQUEST_ME_SUCCESS:
    case CONSTANT.CHECK_LOGIN_SUCCESS:
    case CONSTANT.REGISTER_USER_SUCCESS:
    case CONSTANT.FACEBOOK_SIGNON_SUCCESS:
    case CONSTANT.GOOGLE_SIGNON_SUCCESS: {
      const user = action.payload;
      const finish = finishLoading(state);

      // if (!user) {
      //   return state;
      // }

      // trackAction(user, action.type);

      return {
        ...finish,
        user,
      };
    }

    case CONSTANT.REQUEST_LOGOUT_SUCCESS:
      // ga('set', 'dimension1', 'Not Logged In');

      return Object.assign({}, finishLoading(state), {
        user: {},
        userProfile: {},
      });

    case CONSTANT.REQUEST_ME_ERROR: {
      // ga('set', 'dimension1', 'Not Logged In');
      // Force log out since there is an error requesting auth.
      const loginEmailError = { responseBody: action.error };
      return Object.assign({}, errorLoading(state, loginEmailError), {
        user: {},
        userProfile: {},
      });
    }

    case CONSTANT.CHECK_LOGIN_ERROR:
    case CONSTANT.CHECK_LOGOUT_ERROR:
    case CONSTANT.REGISTER_USER_ERROR: {
      return Object.assign({}, errorLoading(state, action.error));
    }

    case CONSTANT.GOOGLE_SIGNON_ERROR: {
      const googleError = { responseBody: { code: 'techinasia_google_signon_failure' } };
      return Object.assign({}, errorLoading(state, googleError));
    }

    case CONSTANT.FACEBOOK_SIGNON_ERROR: {
      const fbError = { responseBody: { code: 'techinasia_facebook_signon_failure' } };
      return Object.assign({}, errorLoading(state, fbError));
    }

    default:
      return state;
  }
};

export default auth;

const initialResetPassword = appendLoadingStates({
  hasSendResetPasswordEmail: false,
});

export const resetPassword = (state = initialResetPassword, action) => {
  switch (action.type) {
    case CONSTANT.RESET_PASSWORD_EMAIL:
      return Object.assign({}, startLoading(state));

    case CONSTANT.RESET_PASSWORD_EMAIL_SUCCESS: {
      const { success } = action.payload;

      return Object.assign({}, finishLoading(state), {
        hasSendResetPasswordEmail: success,
      });
    }

    case CONSTANT.RESET_PASSWORD_EMAIL_ERROR:
      return Object.assign({}, errorLoading(state, action.error));

    default:
      return state;
  }
};
