import config from 'config';
import CONSTANT from './constant';
import api from './api';

/**
 * Check auth action creator.
 *
 * @return {Object}
 */
const checkAuth = () => ({
  type: CONSTANT.REQUEST_ME,
  payload: {
    promise: api.getMe(),
  },
});

/**
 * Check login action creator.
 *
 * @return {Object}
 */
const checkLogin = (...args) => ({
  type: CONSTANT.CHECK_LOGIN,
  payload: {
    promise: api.postLogin(...args),
  },
});

/**
 * Completes the Facebook & Google sign-on process.
 *
 * @param  {Object} res
 * @return {Object}
 */
const completeFacebookSignOn = token => ({
  type: CONSTANT.FACEBOOK_SIGNON,
  payload: {
    promise: api.connectFacebook(token),
  },
});

const completeGoogleSignOn = token => ({
  type: CONSTANT.GOOGLE_SIGNON,
  payload: {
    promise: api.connectGoogle(token),
  },
});

/**
 * Facebook sign-on action creator.
 *
 * @return {Object}
 */
const facebookSignOn = () => (dispatch) => {
  window.FB.init({
    appId: config.apis.facebook.appId,
    autoLogAppEvents: true,
    xfbml: true,
    version: 'v3.0',
  });

  // We need to check if user is already connected to Facebook.
  window.FB.getLoginStatus((res) => {
    if (res.status === 'connected') {
      const { accessToken } = res.authResponse;
      dispatch(completeFacebookSignOn(accessToken));
    } else {
      window.FB.login((loginRes) => {
        if (loginRes.authResponse) {
          // Call WordPress API for session token.
          const { accessToken } = loginRes.authResponse;
          dispatch(completeFacebookSignOn(accessToken));
        } else {
          dispatch({ type: CONSTANT.FACEBOOK_SIGNON_ERROR });
        }
      }, { scope: 'email' });
    }
  });
};

/**
 * Google sign-on action creator.
 *
 * @return {Object}
 */
const googleSignOn = () => (dispatch) => {
  // Google sign-on via Google API.
  window.googleyolo
    .hint({
      supportedAuthMethods: [
        'https://accounts.google.com',
      ],
      supportedIdTokenProviders: [
        {
          uri: 'https://accounts.google.com',
          clientId: config.apis.google.appId,
        },
      ],
    })
    .then((res) => {
      const { idToken } = res;
      dispatch(completeGoogleSignOn(idToken));
    }, (error) => {
      console.log('error', error);
      dispatch({ type: CONSTANT.GOOGLE_SIGNON_ERROR });
    });
};

/**
 * Logout action creator.
 *
 * @return {Object}
 */
const logout = () => (dispatch) => {
  window.googleyolo.cancelLastOperation().then(() => {
    // Credential selector closed.
    dispatch({
      type: CONSTANT.REQUEST_LOGOUT,
      payload: {
        promise: api.logout(),
      },
    });
  });
};

/**
 * Register action creator.
 *
 * @return {Object}
 */
const register = (...args) => ({
  type: CONSTANT.REGISTER_USER,
  payload: {
    promise: api.register(...args),
  },
});

/**
 * Reset loading action creator.
 *
 * @return {Object}
 */
const resetLoading = () => ({
  type: CONSTANT.RESET_LOADING,
});

const resetPasswordEmail = email => ({
  type: CONSTANT.RESET_PASSWORD_EMAIL,
  payload: {
    promise: api.resetPasswordEmail(email),
  },
});

export {
  checkAuth,
  checkLogin,
  facebookSignOn,
  googleSignOn,
  logout,
  register,
  resetLoading,
  resetPasswordEmail,
};
