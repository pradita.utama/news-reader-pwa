import styled from 'styled-components';

const ContainerStyle = styled.div`
  width: 100%;
  height: 100%;
  padding: 2px 5px 2px;

  .ql-editor{
    min-height: 141px !important;
    height: 200px;
    overflow: hidden;
    overflow-y: scroll;
    overflow-x: scroll;
  }

   .ql-toolbar.ql-snow {
    background: #ffffff;
    border-bottom: none;
    border: 1px solid #eaeaea;   
  }

  .ql-container.ql-snow {
    min-height: 10em;
    background: #ffffff;
    border: 1px solid #eaeaea;
  }
  .ql-toolbar.ql-snow .ql-formats {
    margin-right: 10px;
  }
  .ql-editor .ql-video {
    display: block;
    max-width: 200px;
    width: 200px;
    height: 100px;
  }
  .button-contain {
    display: flex;
    justify-content: flex-end;
    margin: 10px 0 0;
  }
  .ql-editor.ql-blank::before {
    word-break: break-word;
  }
`;

export default ContainerStyle;
