import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

import Button from '../../commons/ui-kit/Button/BaseButton';

import ContainerStyle from './style';

class Editor extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    isSend: PropTypes.bool,
    placeHolder: PropTypes.string.isRequired,
    isError: PropTypes.bool,
  }
  static defaultProps = {
    isSend: false,
    isError: false,
  }
  constructor(props) {
    super(props);
    this.state = {
      editorHtml: '',
      isEmpty: true,
      send: false,
      label: 'Kirim Komentar',
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSend) {
      this.handleIsSend();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.isError !== prevProps.isError) {
      this.handleError();
    }
  }

  handleIsSend = () => {
    this.setState({
      editorHtml: '  ',
      send: false,
      label: 'Kirim Komentar',
    });
  }

  handleError = () => {
    this.setState({
      send: false,
      label: 'Kirim Komentar',
    });
  }

  handleChange = (html, delta, source, editor) => {
    // eslint-disable-next-line
    this.setState({ editorHtml: html });
    if (editor.getText().length > 1) this.setState({ isEmpty: false });
    else this.setState({ isEmpty: !this.state.isEmpty });
  }
  handleClick() {
    this.setState({
      send: true,
      label: 'Mengirim...',
    });
    this.props.onSubmit(this.state.editorHtml);
  }
  renderButton = () => {
    if (this.state.isEmpty) {
      return null;
    }
    return (
      <Button
        style={
          this.state.send ? ({
          background: '#690000',
          opacity: '0.9',
          color: '#dec7c7',
        }) : ({
          opacity: '1',
        })
        }
        label={this.state.label}
        primary
        onClick={this.state.send ? () => '' : () => this.handleClick()}
      />
    );
  }

  render() {
    return (
      <ContainerStyle>
        <ReactQuill
          style={
            this.state.send ? { opacity: '0.4' } : { opacity: '1' }
          }
          theme="snow"
          onChange={this.handleChange}
          readOnly={this.state.send}
          value={this.state.editorHtml}
          modules={Editor.modules}
          formats={Editor.formats}
          bounds=".app"
          placeholder={this.props.placeHolder}
        />
        <div className="button-contain" >
          {this.renderButton()}
        </div>
      </ContainerStyle>
    );
  }
}

Editor.modules = {
  toolbar: [
    ['bold'], ['italic'], ['underline'], ['strike'],
    ['link'], [{ list: 'bullet' }], [{ list: 'ordered' }],
    ['video'], ['clean'],
  ],
  clipboard: {
    matchVisual: false,
  },
};

Editor.formats = [
  'header', 'font', 'size',
  'bold', 'italic', 'underline', 'strike', 'blockquote',
  'list', 'bullet', 'indent',
  'link', 'image', 'video',
];

export default Editor;
