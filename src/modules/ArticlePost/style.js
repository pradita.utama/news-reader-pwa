import styled from 'styled-components';

const ContentStyled = styled.div`
  padding: 0 20px;
  word-wrap: break-word;
  border-bottom: 1px solid #EDEDED;

  blockquote {
    -webkit-font-smoothing: antialiased;
    font-style: italic;
    padding-bottom: 0;
    padding-top: 0;
  }

  blockquote {
    padding: 0 20px 0 20px;
    margin: 0 0 21px;
    font-size: 24px;
    border-left: 5px solid #999;
  }

  .pull-quote {
    border-left-width: 0;
    padding: 30px;
    position: relative;
    margin-right: -20px;
    margin-left: -20px;
    margin-bottom: 20px;
    margin-top: 20px;
    max-width: none;
    box-shadow: none;
    background: #fafafa;
    text-align: center;
    &:before,
    &:after {
      color: red;
      font-size: 140px;
      font-style: normal;
      line-height: 1;
      position: absolute;
      quotes: '“' '”' '‘' '’';
    }
  
    &:before {
      content: open-quote;
      left: 50%;
      margin-left: -40px;
      top: 0px;
      font-weight: 700;
    }
  
    &:after {
      bottom: 0;
      right: 0;
    }
  
    p{
      margin: 0;
      font-size: 20px;
      border: none;
      background: #ffffff;
      padding: 2em 1em 1em 1em;
  
    }
    .quote-author{
      font-size: 14px;
      font-weight: bold;
      padding: 1em;
      background: #ffffff;
      border: none;
      font-style: initial;
      #author{
        font-weight: bold;
        margin-right: 2px;
        &:before{
          position: relative;
          display: inline-block;
          height: 2px;
          width: 10%;
          top: -3px;
          margin-right: 10px;
          background: #000;
        }
      }
    }
  }

  /* .pull-quote {
    padding: 25.35px 48px;
    background: #fafafa;
    margin-left: -20px;
    width: 100vw;
    border: none;
  }

  .pull-quote p {
    font-size: 20px;
    border: none;
    margin: 0;
    background: #fff;
    padding: 2em 1em 1em;
  }

  .pull-quote .quote-author {
    font-size: 12px;
    font-weight: 400;
    padding: 1em;
    background: #fff;
    border-color: #ededed;
    border-style: solid;
    border-width: 0 1px 1px;
  }

  .pull-quote .quote-author #author {
    font-weight: 700;
    margin-right: 2px;
  }

  .pull-quote .quote-author #author&:before {
    position: relative;
    display: inline-block;
    content: '';
    height: 2px;
    width: 10%;
    top: -3px;
    margin-right: 10px;
    background: #000;
  } */

  .embed-youtube {
    text-align: center;
    display: block;
    margin-left: 0px;
  }

  iframe {
    width: 100vw;
    height: 300px;
    margin-left: -20px;
  }

  img {
    width: 100vw;
    height: auto;
    margin-left: -20px;
  }

  /* img.size-full, img.size-large {
    width: 100vw;
    height: auto;
    margin-left: -20px;
  } */

  h3, h4, h5 {
    font-size: 20px;
    line-height: 30px;
  }

  ul {
    padding-left: 23px;
  }

  ul li {
    margin-bottom: 15px;
    font-size: 16px;
    line-height: 26px;
  }

  p {
    font-size: 16px;
    line-height: 26px;
    word-wrap: break-word;
  }

  a { color: #bf0016; }

  /* CAPTION IMAGE */
  .wp-caption-text {
    color: #999;
    font-family: PT Sans, Helvetica Neue, Helvetica, Arial, sans-serif;
    font-size: 14px;
    font-style: italic;
  }

  /* READ ALSO */
  .read-also {
    margin: 25px auto;
    padding: 0px 15px 10px 15px;
    border: 1px solid #ccc;
    border-radius: 3px;
  }
  .read-also__image img {
    display: none;
  }
  .read-also__copy {
    font-size: 14px;
    font-weight: 500;
    color: #777;
    margin-bottom: 5px;
  }
  .read-also__post-title {
    margin: 0;
  }

  /* MULTI APP */
  .multiapp-wrapper {
    position: relative;
    border-top: solid 1px #EDEDED;
    padding: 20px;
    width: 100vw;
    margin-left: -20px;
    padding: 20px;
  }
  .multiapp-description {
    padding-bottom: 15px;

    > .row {
      overflow: hidden;
      margin: 0;
    }
  }
  .multiapp-description__image {
    float: left;
    width: 80px;
    padding: 0 10px 0 5px;
  }
  .multiapp-image img {
    max-height: 150px;
    width: 100%;
    margin: 0;
    border-radius: 20px;
  }
  .multiapp-description__detail {
    float: left;    
    width: 70%;
    margin-top: -5px;
  }
  .multiapp-title {
    font-size: 20px;
    font-weight: 600;
    line-height: 30px;
    color: #333;
  }
  .multiapp-rating img {
    height: 16px;
    width: 16px;
    margin: 0px 2px;
  }
  .multiapp-meta,
  .multiapp-meta__size {
    font-size: 12px;
    margin-top: 5px;
    margin-left: 1px;
  }
  .multiapp-meta__downloader {
    margin-left: 15px;
  }
  .multiapp-description__price {
    display: none;
  }
  .multiapp-dl {
    text-align: center;
    font-size: 12px;
    font-weight: 600;
    margin-left: -20px;
    width: 100vw;
    border-top: 1px solid #EDEDED;
  }
  .multiapp-dl__wrapper {
    display: block;
    padding: 15px 20px 10px 20px;
    -webkit-box-shadow: 0px 2px 4px -1px rgba(0,0,0,0.2);
    -moz-box-shadow: 0px 2px 4px -1px rgba(0,0,0,0.2);
    box-shadow: 0px 2px 4px -1px rgba(0,0,0,0.2);
    text-align: center;

    span {
      display: block;
      padding: 0;
      font-size: 18px;
      font-weight: 700;
      text-transform: uppercase;
    }
  }
  .multiapp-dl__button {
    display: inline-block;

    img {
      margin: 0;
      width: 40vw;
      margin: 5px;
    }
  }

  video {
    width: 100vw;
    margin-left: -20px;
  }

  .wp-video {
    width: 100% !important;
  }

  #mc_embed_signup {
    width: 100vw;
    margin-left: -20px;
  }
`;

const GaType = styled.span`
  display: inline-block;
  color: #bf0016;
  font-size: 16px;
`;

const TitleStyled = styled.h1`
  padding: 0;
  margin: 10px 0;
  font-size: 28px;
  line-height: 35px;
  font-weight: 600;
`;

const Container = styled.div`
  padding: 20px;
  min-height: 200px;
`;

const Badge = styled.span`
  color: #FFF;
  text-transform: capitalize;
  padding: 4px 6px 4px 6px;
  background-color: #00aaa5;
  border-radius: 4px;
  font-size: 13px;
  text-decoration: none;
`;
const BadgeBC = styled.span`
  color: #FFF;
  text-transform: capitalize;
  padding: 4px 6px 4px 6px;
  background-color: #f0ad4e;
  border-radius: 4px;
  // margin-left: 48px;
  font-size: 13px;
  text-decoration: none;
`;

const BrandedContentStyle = styled.div`
  margin: 10px 0 10px 0;

  div {
    color: #47474794;
    font-size: 14px;
  }

  img {
    height: 45px;
  }
`;

export {
  ContentStyled,
  GaType,
  TitleStyled,
  Container,
  Badge,
  BrandedContentStyle,
  BadgeBC,
};
