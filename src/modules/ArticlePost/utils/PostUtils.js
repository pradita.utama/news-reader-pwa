import { diffForHumans, formatFullDate } from '../../../utils/dateUtils';

const formatContent = content =>
  content
    .replace(/<hr \/>/g, '')
    .replace(/img /g, 'img alt="Gambar dalam artkel" onerror="this.src=\'/static/images/fallback-image.svg\'" ')
    .replace(/https:\/\/id\.techinasia\.com\/wp-content\/themes\/techinasia\/img\/icon_googleplay\.png/g, '/static/images/google-play.png')
    .replace(/https:\/\/id\.techinasia\.com\/wp-content\/themes\/techinasia\/img\/icon_appstore\.png/g, '/static/images/app-store.png');

const getParentCategory = (categories) => {
  let foundParentCategoryIndex = 0;
  for (let i = 0; i < categories.length; i += 1) {
    if (categories[i].parent === null) {
      foundParentCategoryIndex = i;
      break;
    }
  }
  return categories[foundParentCategoryIndex];
};

const capitalizeFirstLetter = string => string.charAt(0).toUpperCase() + string.slice(1);

const getLink = link => link.replace('https://id.techinasia.com', '');

const humanReadableDate = date => diffForHumans(date);

const fullDate = date => formatFullDate(date);

export {
  formatContent,
  getParentCategory,
  getLink,
  humanReadableDate,
  fullDate,
  capitalizeFirstLetter,
};
