import { getPost } from './api';
import CONSTANT from './constant';

const requestPost = slug => ({
  type: CONSTANT.REQUEST_POST,
  payload: {
    promise: getPost(slug),
  },
});

export { requestPost };
