import styled from 'styled-components';

const Container = styled.div`
  position: fixed;
  height: 50px;
  width: 100%;
  background-color: #fff;
  z-index: 100;
  bottom: 0;
  display: flex;
  box-shadow: rgba(0, 0, 0, 0.5) 0px 1px 3px 0px;

  .footer-article-container {
    width: 100%;
    display: flex;
  }

  input[type="button"]{
    outline:none;
  }

  .share-icon-container {
    width: 100%;
    display: flex;
    margin-left: 10px;
    align-items: center;

    button {
      border-radius: 100%;
      width: 35px;
      height: 35px;
      border: none;
      outline:none;
      -webkit-box-shadow: 0px 1px 8px -2px rgba(0,0,0,0.75);
      -moz-box-shadow: 0px 1px 8px -2px rgba(0,0,0,0.75);
      box-shadow: 0px 1px 8px -2px rgba(0,0,0,0.75);
  
      
    }
  }

  // ICONS BUTTONS
  div[role="button"] {
    -webkit-box-shadow: 0px 1px 8px -2px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 1px 8px -2px rgba(0,0,0,0.75);
    box-shadow: 0px 1px 8px -2px rgba(0,0,0,0.75);
    width: 35px;
    height: 35px;
    position: absolute;
    border-radius: 100%;
  }

  // FACEBOOK
  // FACEBOOK ICON STYLING

  .facebook-icon {
    animation-name: slide-and-rotate-out-facebook;
    animation-duration: 0.5s;
    left: calc(100% - 50px);
    background-color: #3b5998;
    z-index: 2;
  }

  .facebook-icon-hide {
    animation-name: slide-and-rotate-in-facebook;
    animation-duration: 0.5s;
    left: 15px;
    opacity: 0;
    background-color: #3b5998;
    z-index: 2;
  }

  // TWITTER
  .twitter-icon {
    animation-name: slide-and-rotate-out-twitter;
    animation-duration: 0.5s;
    left: calc(75% - 35px);
    z-index: 4;
    background-color: #00aced;
  }

  .twitter-icon-hide {
    animation-name: slide-and-rotate-in-twitter;
    animation-duration: 0.5s;
    left: 15px;
    opacity: 0;
    z-index: 4;
    background-color: #00aced;
  }

  // LINKEDIN
  .linkedin-icon {
    background-color: #0077B5;
    animation-name: slide-and-rotate-out-linkedin;
    animation-duration: 0.5s;
    left: calc(50% - 17.5px);
    z-index: 6;
  }

  .linkedin-icon-hide {
    background-color: #0077B5;
    animation-name: slide-and-rotate-in-linkedin;
    animation-duration: 0.5s;
    left: 15px;
    opacity: 0;
    z-index: 6;
  }

  // GOOGLE PLUS
  .google-plus-icon {
    background-color: #d34836;
    animation-name: slide-and-rotate-out-google-plus;
    animation-duration: 0.5s;
    left: 25%;
    z-index: 8;
  }

  .google-plus-icon-hide {
    background-color: #d34836;
    animation-name: slide-and-rotate-in-google-plus;
    animation-duration: 0.5s;
    left: 15px;
    opacity: 0;
    z-index: 8;
  }


  .close-icon {
    background-color: black;
    padding: 10px;
    position: absolute;
    left: 15px;
    z-index: 10;
    animation-name: slide-and-rotate-out-close;
    animation-duration: 0.5s;
    position: absolute;
  }  

  .close-icon-hide {
    background-color: black;
    padding: 10px;
    position: absolute;
    left: 15px;
    opacity: 0;
    z-index: 10;
    animation-name: slide-and-rotate-in-close;
    animation-duration: 0.5s;
    position: absolute;
  }

  .left-footer-button-container {
    flex: 1;

    &__comment-button {
      float: left;
      width: 50px;
    }
  }

  .right-footer-button-container {
    flex: 2;
    text-align: right;
    padding: 5px 10px;
  }

  @keyframes slide-and-rotate-out-close {
    from {opacity: 0;}
    to {-webkit-transform: rotate(360deg); transform:rotate(360deg); opacity: 1;}
  }

  @keyframes slide-and-rotate-out-facebook {
    from {left: 15px; opacity: 0; }
    to {left: calc(100% - 50px); -webkit-transform: rotate(360deg); transform:rotate(360deg);opacity: 1;}
  }

  @keyframes slide-and-rotate-out-twitter {
    from {left: 15px; opacity: 0; }
    to {left: calc(75% - 35px); -webkit-transform: rotate(360deg); transform:rotate(360deg);opacity: 1;}
  }

  @keyframes slide-and-rotate-out-linkedin {
    from {left: 15px; opacity: 0; }
    to {left: calc(50% - 17.5px); -webkit-transform: rotate(360deg); transform:rotate(360deg);opacity: 1;}
  }

  @keyframes slide-and-rotate-out-google-plus {
    from {left: 15px;  opacity: 0;}
    to {left: 25%; -webkit-transform: rotate(360deg); transform:rotate(360deg);opacity: 1;}
  }

  // slide and rotate in

  @keyframes slide-and-rotate-in-facebook {
    from {left: calc(100% - 50px);  opacity: 1;}
    to {left: 15px; opacity: 0; -webkit-transform: rotate(-360deg); transform:rotate(-360deg);}
  }

  @keyframes slide-and-rotate-in-linkedin {
    from {left: calc(50% - 17.5px); opacity: 1; }
    to {left: 15px; opacity: 0; -webkit-transform: rotate(-360deg); transform:rotate(-360deg);}
  }

  @keyframes slide-and-rotate-in-google-plus {
    from {left: 25%;  opacity: 1;}
    to {left: 15px; -webkit-transform: rotate(360deg); transform:rotate(360deg);opacity: 0;}
  }

  @keyframes slide-and-rotate-in-twitter {
    from {left: calc(75% - 35px); opacity: 1; }
    to {left: 15px; -webkit-transform: rotate(360deg); transform:rotate(360deg);opacity: 0;}
  }

  @keyframes slide-and-rotate-in-close {
    from {opacity: 1;}
    to {-webkit-transform: rotate(-360deg); transform:rotate(-360deg); opacity: 0;}
  }
`;

const FooterButtonIcon = {
  borderRadius: '0',
  width: '50px',
  height: '50px',
  margin: '0',
  border: 'none',
};

const ButtonIcon = {
  height: '30px',
  padding: '2px',
  width: '70px',
  fontSize: 'medium',
};

const Comments = styled.div`
  border-radius: 100px;
  background-color: #333333;
  color: #fff;
  font-size: 9px;
  width: 20px;
  height: 20px;
  text-align: center;
  padding-top: 5px;
  position: fixed;
  left: 25px;
  bottom: 21px;
`;

export {
  Container,
  FooterButtonIcon,
  ButtonIcon,
  Comments,
};
