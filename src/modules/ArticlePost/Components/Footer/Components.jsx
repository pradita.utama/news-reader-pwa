import React from 'react';
import PropTypes from 'prop-types';

import iconComments from '../../../../images/Navigation/footer-comments.svg';
import iconShare from '../../../../images/Navigation/footer-share.svg';
import Button from '../../../../commons/ui-kit/Button/BaseButton';
import { Comments } from './style';

const CommentsIcon = ({ style }) => (
  <Button
    icon="true"
    iconSize="medium"
    src={iconComments}
    style={style}
  />
);

CommentsIcon.defaultProps = {
  style: null,
};

CommentsIcon.propTypes = {
  style: PropTypes.object,
};

const ShareIcon = ({ style, onClick }) => (
  <Button
    icon="true"
    iconSize="medium"
    src={iconShare}
    style={style}
    onClick={onClick}
  />
);

ShareIcon.defaultProps = {
  style: null,
  onClick: null,
};

ShareIcon.propTypes = {
  style: PropTypes.object,
  onClick: PropTypes.func,
};

const CommentsCount = ({ commentsCount }) => (
  <Comments>
    {commentsCount > 1000 ? '1K' : commentsCount}
  </Comments>
);

CommentsCount.defaultProps = {
  commentsCount: 0,
};

CommentsCount.propTypes = {
  commentsCount: PropTypes.number,
};

export {
  CommentsIcon,
  ShareIcon,
  CommentsCount,
};

