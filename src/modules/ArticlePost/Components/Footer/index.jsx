import React from 'react';
import {
  FacebookShareButton,
  TwitterShareButton,
  LinkedinShareButton,
  GooglePlusShareButton,
  FacebookIcon,
  TwitterIcon,
  LinkedinIcon,
  GooglePlusIcon,
} from 'react-share';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';

import FaceButton from 'commons/ui-kit/Button/IconButton/FaceButton';
import LoginButton from 'commons/ui-kit/Button/CommonButton/LoginButton';
import RegisterButton from 'commons/ui-kit/Button/CommonButton/RegisterButton';
import CloseIcon from 'images/icons/close.svg';
import ModalLoginOrRegister from 'modules/Auth';
import { CommentsIcon, ShareIcon, CommentsCount } from './Components';
import { FooterButtonIcon, Container } from './style';

class ArticleFooter extends React.Component {
  static propTypes = {
    link: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  }

  state = {
    opened: false,
    animateIcons: false,
    modalIsOpen: false,
    modalId: 'login',
    isLogin: false,
  };

  componentDidMount() {
    // set login on navigate from another page
    // eslint-disable-next-line
    const { user } = this.props;
    const isEmptyObject = isEmpty(user);

    if (!isEmptyObject) {
      this.setLogin();
    }
  }

  componentDidUpdate(prevProps) {
    // set login on direct link
    // eslint-disable-next-line
    const { user } = this.props;
    const isEmptyObject = isEmpty(user);

    if ((prevProps.user !== user) && !isEmptyObject) {
      this.setLogin();
    }
  }

  setLogin = () => {
    this.setState({
      modalIsOpen: false,
      isLogin: true,
    });
  }

  openModal = (e) => {
    const { id } = e.currentTarget;

    this.setState({
      modalIsOpen: true,
      modalId: id,
    });
  }

  changeModalId = (id) => {
    this.setState({ modalId: id });
  }

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  }

  goToComments = () => {
    // Scroll to a comments section
    window.scroll({
      top: document.body.scrollHeight + 200,
      left: 0,
      behavior: 'smooth',
    });
  };

  sharePost = ({ props }) => (e) => {
    e.preventDefault();
    // console.log(props);
    if (window.navigator.share) {
      window.navigator.share({
        title: props.title.replace(/<(.|\n)*?>/g, ''),
        text: props.excerpt.replace(/<(.|\n)*?>/g, ''),
        url: props.link,
      })
        .then(() => null)
        .catch(error => console.log('Error sharing', error));
    } else {
      /** show fancy sharing button animation */
      this.toggleShareIcons();
    }
  };

  toggleShareIcons = () => {
    const { opened } = this.state;
    if (opened) {
      this.setState({ animateIcons: true });
      setTimeout(() => {
        this.setState({ opened: false });
      }, 500);
    } else {
      this.setState({
        opened: true,
        animateIcons: false,
      });
    }
  };

  renderShareIcons = (link, title) => (
    <Container>
      <div id="share-icons" className="share-icon-container">
        <button className={this.state.animateIcons ? 'close-icon-hide' : 'close-icon'} onClick={this.toggleShareIcons}>
          <img src={CloseIcon} alt="close icon" />
        </button>

        <GooglePlusShareButton
          url={link}
          className={this.state.animateIcons ? 'google-plus-icon-hide' : 'google-plus-icon'}
        >
          <GooglePlusIcon
            size={35}
            round
          />
        </GooglePlusShareButton>
        <LinkedinShareButton
          url={link}
          title={title.replace(/<(.|\n)*?>/g, '')}
          windowWidth={750}
          windowHeight={600}
          className={this.state.animateIcons ? 'linkedin-icon-hide' : 'linkedin-icon'}
        >
          <LinkedinIcon
            size={35}
            round
          />
        </LinkedinShareButton>
        <TwitterShareButton
          url={link}
          title={title.replace(/<(.|\n)*?>/g, '')}
          className={this.state.animateIcons ? 'twitter-icon-hide' : 'twitter-icon'}
        >
          <TwitterIcon
            size={35}
            round
          />
        </TwitterShareButton>
        <FacebookShareButton
          url={link}
          className={this.state.animateIcons ? 'facebook-icon-hide' : 'facebook-icon'}
        >
          <FacebookIcon
            size={35}
            round
          />
        </FacebookShareButton>
      </div>
    </Container>
  );

  renderMainContent = () => (
    <Container id="footer-article-container">
      <div id="footer-article" className="footer-article-container">
        <div className="left-footer-button-container">
          <div className="left-footer-button-container__comment-button" onClick={this.goToComments} role="presentation">
            <CommentsCount {...this.props} />
            <CommentsIcon style={FooterButtonIcon} />
          </div>
          <ShareIcon
            onClick={this.sharePost(this, { ...this.props })}
            style={FooterButtonIcon}
          />
        </div>
        <div className="right-footer-button-container">
          {
            this.state.isLogin
              ? <FaceButton user={this.props.user} />
              : (
                <React.Fragment>
                  <LoginButton HandleClick={this.openModal} />
                  <RegisterButton HandleClick={this.openModal} />
                </React.Fragment>
              )
          }
        </div>
      </div>
    </Container>
  );


  render() {
    const { link, title } = this.props;
    const { opened, modalId, modalIsOpen } = this.state;

    return (
      <React.Fragment>
        {
          opened
            ? this.renderShareIcons(link, title)
            : this.renderMainContent()
        }

        {/* LOGIN MODAL Or Register */}
        <ModalLoginOrRegister
          modalId={modalId}
          modalIsOpen={modalIsOpen}
          changeModalId={this.changeModalId}
          closeModal={this.closeModal}
        />
      </React.Fragment>
    );
  }
}

export default ArticleFooter;
