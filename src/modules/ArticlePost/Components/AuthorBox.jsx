import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import PostMeta from './PostMeta';

const AuthorBoxContainer = styled.div`
  padding-bottom: 20px;
  float: left;

  .author-box {
    margin: 10px 0;
    width: 100vw;
    -webkit-box-shadow: 0px 1px 2px -1px rgba(0,0,0,0.2);
    -moz-box-shadow: 0px 1px 2px -1px rgba(0,0,0,0.2);
    box-shadow: 0px 1px 2px -1px rgba(0,0,0,0.2);
    
  }
  .author-box__heading {
    padding: 6px 21px;
  }
  .author-box__heading {
    background-color: #fafafa;
    font-size: 14px;
    font-weight: 600;
    margin: 0 0 10px;
    padding: 15px 20px 15px 25px;
    text-transform: uppercase;
    color: rgba(71,71,71,0.6);
  }
  .author-box__description {
    line-height: 26.25px;
    word-wrap: break-word;
  }
  .author-box__description .about-description {
    padding: 6px 24px;
  }
  .author-box__description .about-detail {
    padding: 6px 36px 12px 12px;
  }

  .author-box p {
    color: #999;
  }
`;

const AuthorBox = ({ author }) => (
  <AuthorBoxContainer>
    <div className="author-box">
      <p className="author-box__heading">About Writer</p>
      <div className="author-box__description">
        <div className="row">
          <div className="about-description">
            <PostMeta authorMeta={author} hidePublishDate />
            <p>{author.description}</p>
          </div>
        </div>
      </div>
    </div>
  </AuthorBoxContainer>
);

AuthorBox.propTypes = {
  author: PropTypes.object.isRequired,
};

export default AuthorBox;
