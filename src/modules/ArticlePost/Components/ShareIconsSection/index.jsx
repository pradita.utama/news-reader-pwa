import React from 'react';
import {
  FacebookShareButton,
  TwitterShareButton,
  LinkedinShareButton,
  GooglePlusShareButton,
  FacebookIcon,
  TwitterIcon,
  LinkedinIcon,
  GooglePlusIcon,
  FacebookShareCount,
} from 'react-share';

import { Container } from './style';

class ShareIcons extends React.Component {
  renderShareIcons = props => (
    <Container>
      <FacebookShareCount
        url={props.link}
      >
        {count =>
          (
            <span className="share-count">
              {(count / 1000) > 1 ? `${(count / 1000).toFixed(1)}K` : count}<i className="right" />
            </span>
          )}
      </FacebookShareCount>
      <FacebookShareButton
        url={props.link}
        className="icon-button facebook"
      >
        <FacebookIcon
          size={31}
        />
      </FacebookShareButton>
      <TwitterShareButton
        url={props.link}
        title={props.title.replace(/<(.|\n)*?>/g, '')}
        className="icon-button twitter"
      >
        <TwitterIcon
          size={31}
        />
      </TwitterShareButton>
      <LinkedinShareButton
        url={props.link}
        title={props.title.replace(/<(.|\n)*?>/g, '')}
        windowWidth={750}
        windowHeight={600}
        className="icon-button linkedin"
      >
        <LinkedinIcon
          size={31}
        />
      </LinkedinShareButton>
      <GooglePlusShareButton
        url={props.link}
        className="icon-button google-plus"
      >
        <GooglePlusIcon
          size={31}
        />
      </GooglePlusShareButton>
    </Container>
  );

  render() {
    return this.renderShareIcons(this.props);
  }
}

export default ShareIcons;
