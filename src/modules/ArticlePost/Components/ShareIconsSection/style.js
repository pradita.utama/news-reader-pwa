import styled from 'styled-components';

const Container = styled.div`
  padding: 0 20px 20px;
  display: flex;
  align-items: center;
  justify-content: center;

  div[role="button"] {
    padding-top: 1px;
    width: 100px;
    height: 33px;
    margin: 0px 5px;
    border-radius: 4px;
    display: flex;
    justify-content: center;
    -webkit-box-shadow: 0px 1px 8px -2px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 1px 8px -2px rgba(0,0,0,0.75);
    box-shadow: 0px 1px 8px -2px rgba(0,0,0,0.75);
  }

  .facebook {
    background-color: #3b5998;
  }

  .linkedin {
    background-color: #007fb1;
  }

  .twitter {
    background-color: #00aced;
  }

  .google-plus {
    background-color: #dd4b39;
  }

  .share-count {
    border: 2px solid #eaeaea;
    height: 33px;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 50px;
    margin-right: 5px;
    border-radius: 4px;
    font-size: 16px;
    font-weight: normal;

    i {
      border: solid #eaeaea;
      border-width: 0 2px 2px 0;
      display: inline-block;
      padding: 3px;
      background-color: #fff;

      margin: 0;
      right: -20px;
      position: relative;
    }
  }

  .right {
    transform: rotate(-45deg);
    -webkit-transform: rotate(-45deg);
  }
`;

export { Container };
