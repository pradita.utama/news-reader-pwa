import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

class ConvertLink extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = ({ props }) => (e) => {
    const urlRegex = RegExp('id.techinasia.com');
    e.preventDefault();

    if (urlRegex.test(props.href)) {
      props.history.push(props.href.replace('https://id.techinasia.com', ''));
      return;
    }

    window.open(props.href, '_blank');
  }

  render() {
    const { staticContext, children, ...props } = this.props;

    return (
      <a onClick={this.handleClick(this, { ...props })} staticcontext={staticContext} {...props} role="presentation">
        {children}
      </a>
    );
  }
}

ConvertLink.defaultProps = {
  staticContext: null,
  children: [],
};

ConvertLink.propTypes = {
  children: PropTypes.array,
  staticContext: PropTypes.object,
};

const Link = withRouter(ConvertLink);

export default Link;
