import call from '../../../../commons/api/RestApi';

const getRelatedPosts = slug => call(`/posts/${slug}/related`);

export {
  getRelatedPosts,
};
