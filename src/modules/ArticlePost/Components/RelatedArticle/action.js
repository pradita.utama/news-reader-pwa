import { getRelatedPosts } from './api';
import CONSTANT from './constant';

const requestRelatedPost = slug => ({
  type: CONSTANT.REQUEST_RELATED_POST,
  payload: {
    promise: getRelatedPosts(slug),
  },
});

export {
  requestRelatedPost,
};
