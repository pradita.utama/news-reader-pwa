import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ErrorFeed from '../../../../commons/errors/ErrorFeed';
import { RelatedArticles } from './Components';
import { RelatedArticleSection } from './style';

import { requestRelatedPost } from './action';

class RelatedArticleContainer extends React.Component {
  static propTypes = {
    slug: PropTypes.string.isRequired,
    requestRelatedPost: PropTypes.func.isRequired,
    relatedPostReducer: PropTypes.object.isRequired,
  }

  componentDidMount() {
    const { slug } = this.props;
    this.props.requestRelatedPost(slug);
  }

  reloadRelatedArticles = () => {
    const { slug } = this.props;
    this.props.requestRelatedPost(slug);
  };

  render() {
    const {
      posts,
      isLoading,
      hasError,
    } = this.props.relatedPostReducer;

    // if (isLoading) {
    //   return (
    //     <React.Fragment>
    //       <h2 className="related-article-title">Artikel yang bisa kamu baca selanjutnya.</h2>
    //       <RelatedArticles isLoading />
    //     </React.Fragment>
    //   );
    // }

    if (hasError) {
      return (
        <React.Fragment>
          <RelatedArticleSection>
            <h2>Artikel yang bisa kamu baca selanjutnya.</h2>
            <ErrorFeed onClick={this.reloadRelatedArticles} />
          </RelatedArticleSection>
        </React.Fragment>
      );
    }

    return posts !== null && (
      <React.Fragment>
        <RelatedArticleSection>
          <h2>Artikel yang bisa kamu baca selanjutnya.</h2>
          { isLoading ? <RelatedArticles isLoading /> : <RelatedArticles posts={posts} /> }
        </RelatedArticleSection>
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ relatedPostReducer }) => ({
  relatedPostReducer,
});

export default connect(mapStateToProps, { requestRelatedPost })(RelatedArticleContainer);

