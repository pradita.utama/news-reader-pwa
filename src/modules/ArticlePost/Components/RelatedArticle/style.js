import styled from 'styled-components';

// const Container = styled.div`
//   padding: 0 20px;

//   .related-article-title {
//     font-size: 21px;
//     line-height: 25px;
//     font-weight: 600;
//   }
// `;

const Image = styled.img`
  width: 100%;
  min-height: 200px;
`;

const Container = styled.div`
  padding: 0 20px 0 20px;

    .card-big {
      .category {
        margin: 10px 0;
        color: #bf0016;
        font-size: 14px;
        text-transform: uppercase;
      }
      .title {
        font-size: 19px;
        line-height: 23px;
        margin: -8px 0 10px 0;
        color: #000;
      }
  
      img {
        border-radius: 4px;
        width: 100%;
        height: 180px;
        object-fit: cover;
      }

      .loading-container {
        position: relative;
        height: 180px;
        padding-top: 53px;
        background-color: #EDEDED;
        border-radius: 4px;
    
        div > div {
          width: 250px;
        }
      }
    }

    
    .card-small {
      height: 93px;
      float: right;
      width: 100%;
      margin-bottom: 10px;
      border-radius: 4px;
      -webkit-box-shadow: 5px 4px 20px -5px rgba(0,0,0,0.42);
      -moz-box-shadow: 5px 4px 20px -5px rgba(0,0,0,0.42);
      box-shadow: 5px 6px 7px -5px rgba(0,0,0,0.12);
      padding-right: 10px;

      .category {
        margin: 2px 0 14px 0;
        color: #bf0016;
        font-size: 14px;
        text-transform: uppercase;
      }
      .title {
        font-size: 15px;
        line-height: 16px;
        margin: -8px 0 10px 60px;
        color: #000;
      }
  
      img {
        width: 50px;
        height: 93px;
        object-fit: cover;
        float: left;
        margin-right: 10px;
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
      }

      .imageloader, 
      .imageloader-failed {
        img {
          width: 50px;
          float: left;
          margin-right: 10px;
        }

      }

      .loading-error {

      }

      .loading-container {
        position: relative;
        height: 93px;
        width: 50px;
        padding-top: 53px;
        float: left;
        background-color: #EDEDED;
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
        margin-right: 10px;
    
        div > div {
          width: 20px;
        }
      }
    }
    
  .loading-container {
    position: relative;
    height: 180px;
    padding-top: 53px;

    div > div {
      width: 250px;
    }
  }

  .loading-image {
    position: relative;
    padding-top: 55px;
    border-radius: 4px;
    height: 180px;
    background-color: #EDEDED;
  }

  .loading-text {
    width: 100%;
    text-align: center;
    font-size: 12px;
    color: #47474778;
    margin: 40px 0;
  }

  h2 {
    font-weight: 600;
    font-size: 20px;
    line-height: 23px;
  }
`;

const RelatedArticleSection = styled.div`
  padding-bottom: 30px;
  overflow: auto;
  
  h2 {
    font-weight: 600;
    font-size: 20px;
    line-height: 23px;
    margin: 20px;
  }
`;

export {
  Container,
  Image,
  RelatedArticleSection,
};
