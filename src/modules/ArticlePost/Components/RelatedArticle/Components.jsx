import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import ImageLoader from 'react-load-image';

import LoadingBar from '../../../../commons/loading/LoadingBar';
import { Container, Image } from './style';
import { getParentCategory } from '../../utils/PostUtils';

const ErrorLoad = () => <Image src="/static/images/fallback-image.svg" alt="Gambar tidak dapat diunduh" />;

const CardBig = ({
  title, link, featuredImage, categories,
}) => (
  <React.Fragment>
    <Link
      to={link.replace('https://id.techinasia.com', '')}
      alt={title}
    >
      <div className="card-big">
        <ImageLoader
          src={featuredImage.source}
        >
          <Image title={title} />
          <ErrorLoad />
          <div className="loading-container"><LoadingBar /></div>
        </ImageLoader>
        <p className="card-big category">{getParentCategory(categories).name}</p>
        <h2 className="card-big title" dangerouslySetInnerHTML={{ __html: title }} />
      </div>
    </Link>
  </React.Fragment>
);


const CardSmall = ({
  title, link, featuredImage, categories,
}) => (
  <React.Fragment>
    <Link
      to={link.replace('https://id.techinasia.com', '')}
      alt={title}
    >
      <div className="card-small">
        <ImageLoader
          src={featuredImage.attachmentMeta.sizes.medium.url}
        >
          <img title={title} alt={title} />
          <img className="loading-error" src="/static/images/fallback-image.svg" alt="Gambar tidak dapat diunduh" />
          <div className="loading-container">&nbsp;</div>
        </ImageLoader>
        <p className="card-big category">{getParentCategory(categories).name}</p>
        <h2 className="card-big title" dangerouslySetInnerHTML={{ __html: title }} />
      </div>
    </Link>
  </React.Fragment>
);

const RelatedArticlesList = ({ posts }) => (
  posts.slice(0, 5).map((post, idx) => {
    if (idx === 0) {
      return <CardBig key={post.id} {...post} />;
    }
    return <CardSmall key={post.id} {...post} />;
  })
);

const LoadingArticles = () => (
  <React.Fragment>
    <Container>
      <div className="loading-container">
        <LoadingBar />
        <div className="loading-text">Artikel sedang dalam perjalanan...</div>
      </div>
    </Container>
  </React.Fragment>
);

const RelatedArticles = ({ isLoading, posts }) =>
  (
    <React.Fragment>
      <Container>
        { isLoading ? <LoadingArticles /> : <RelatedArticlesList posts={posts} /> }
      </Container>
    </React.Fragment>
  );

RelatedArticles.defaultProps = {
  isLoading: false,
  posts: null,
};

RelatedArticles.propTypes = {
  isLoading: PropTypes.bool,
  posts: PropTypes.array,
};

CardBig.propTypes = {
  title: PropTypes.string.isRequired,
  featuredImage: PropTypes.object.isRequired,
  categories: PropTypes.array.isRequired,
  link: PropTypes.string.isRequired,
};

CardSmall.propTypes = {
  title: PropTypes.string.isRequired,
  featuredImage: PropTypes.object.isRequired,
  categories: PropTypes.array.isRequired,
  link: PropTypes.string.isRequired,
};

export {
  CardBig,
  RelatedArticles,
  LoadingArticles,
};
