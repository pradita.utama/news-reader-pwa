import { getPopularPosts } from './api';
import CONSTANT from './constant';

const requestPopularPost = (categories, postId) => ({
  type: CONSTANT.REQUEST_POPULAR_POST,
  payload: {
    promise: getPopularPosts(categories, postId),
  },
});

export {
  requestPopularPost,
};
