import CONSTANT from './constant';
import { startLoading, finishLoading, errorLoading } from '../../../../utils/reducerUtils';

const initialState = {
  posts: null,
};

const popularPostReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANT.REQUEST_POPULAR_POST: {
      return startLoading(state);
    }
    case CONSTANT.REQUEST_POPULAR_POST_ERROR: {
      return errorLoading(state, action);
    }
    case CONSTANT.REQUEST_POPULAR_POST_SUCCESS: {
      const finish = finishLoading(state);
      const popularPosts = [];
      Object.keys(action.payload).forEach((key) => {
        const obj = {};
        obj.category = key;
        obj.posts = action.payload[key].posts;
        popularPosts.push(obj);
      });
      return {
        ...finish,
        posts: popularPosts,
      };
    }
    default: {
      return state;
    }
  }
};

export {
  popularPostReducer,
};
