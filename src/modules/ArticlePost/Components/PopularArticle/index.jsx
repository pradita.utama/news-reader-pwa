import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ErrorFeed from '../../../../commons/errors/ErrorFeed';
import { PopularArticles, LoadingArticles } from './Components';
import { PopularArticleSection } from './style';

import { requestPopularPost } from './action';

class PopularPostContainer extends React.Component {
  static propTypes = {
    categories: PropTypes.array.isRequired,
    requestPopularPost: PropTypes.func.isRequired,
    popularPostReducer: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
  }

  componentDidMount() {
    const { categories, id } = this.props;
    this.props.requestPopularPost(categories.map(category => category.slug).toString(), id);
  }

  reloadPopularArticles = () => {
    const { categories, id } = this.props;
    this.props.requestPopularPost(categories.map(category => category.slug).toString(), id);
  };

  render() {
    const {
      posts,
      isLoading,
      hasError,
    } = this.props.popularPostReducer;

    if (isLoading) {
      return (
        <PopularArticleSection>
          <h2>Mungkin kamu menyukai artikel berikut.</h2>
          <LoadingArticles />
        </PopularArticleSection>
      );
    }

    if (hasError) {
      return (
        <PopularArticleSection>
          <h2>Mungkin kamu menyukai artikel berikut.</h2>
          <ErrorFeed onClick={this.reloadPopularArticles} />
        </PopularArticleSection>
      );
    }

    return posts !== null && (
      <PopularArticleSection>
        <h2>Mungkin kamu menyukai artikel berikut.</h2>
        <PopularArticles posts={posts} />
      </PopularArticleSection>
    );
  }
}

const mapStateToProps = ({ popularPostReducer }) => ({
  popularPostReducer,
});

export default connect(mapStateToProps, { requestPopularPost })(PopularPostContainer);

