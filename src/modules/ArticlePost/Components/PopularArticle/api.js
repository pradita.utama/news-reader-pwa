import call from '../../../../commons/api/RestApi';

const getPopularPosts = (categories, postId) => call(`/popular?categories=${categories}&on_read=${postId}`);

export {
  getPopularPosts,
};
