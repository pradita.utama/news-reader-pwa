import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import ImageLoader from 'react-load-image';

import LoadingBar from '../../../../commons/loading/LoadingBar';
import { Container, Image } from './style';

const ErrorLoad = () => <Image src="/static/images/fallback-image.svg" alt="Gambar tidak dapat diunduh" />;

const CardBig = ({
  title, link, featuredImage, categories,
}) => (
  <Link
    to={link.replace('https://id.techinasia.com', '')}
    alt={title}
  >
    <div className="card-big">
      <ImageLoader
        src={featuredImage.source}
      >
        <Image title={title} />
        <ErrorLoad />
        <div className="loading-container"><LoadingBar /></div>
      </ImageLoader>
      <p className="card-big category">{categories[0].name}</p>
      <h2 className="card-big title" dangerouslySetInnerHTML={{ __html: title }} />
    </div>
  </Link>
);

const CardSmall = ({
  title, link, featuredImage,
}) => (
  <Link
    to={link.replace('https://id.techinasia.com', '')}
    alt={title}
  >
    <div className="card-small">
      <ImageLoader
        src={featuredImage.attachmentMeta.sizes.medium.url}
      >
        <img title={title} alt={title} />
        <img className="loading-error" src="/static/images/fallback-image.svg" alt="Gambar tidak dapat diunduh" />
        <div className="loading-container">&nbsp;</div>
      </ImageLoader>
      <h2 className="card-big title" dangerouslySetInnerHTML={{ __html: title }} />
    </div>
  </Link>
);

const LoadingArticles = () => (
  <Container>
    <div className="loading-container">
      <LoadingBar />
      <div className="loading-text">Artikel sedang dalam perjalanan...</div>
    </div>
  </Container>
);

const PopularArticles = ({ posts }) => (
  <Container>
    {
      posts.map(item =>
        (
          <React.Fragment key={item.category}>
            <div className="category"><div><span>{item.category}</span></div></div>
            {
              item.posts.slice(0, 5).map(post => <CardSmall key={post.id} {...post} />)
            }
          </React.Fragment>
        ))
    }
  </Container>
);

PopularArticles.defaultProps = {
  posts: null,
};

PopularArticles.propTypes = {
  posts: PropTypes.array,
};

CardBig.propTypes = {
  title: PropTypes.string.isRequired,
  featuredImage: PropTypes.object.isRequired,
  categories: PropTypes.array.isRequired,
  link: PropTypes.string.isRequired,
};

CardSmall.propTypes = {
  title: PropTypes.string.isRequired,
  featuredImage: PropTypes.object.isRequired,
  link: PropTypes.string.isRequired,
};

export {
  CardBig,
  PopularArticles,
  LoadingArticles,
};
