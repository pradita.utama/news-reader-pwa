import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { GaType, TitleStyled, BadgeBC, Badge } from '../style';

const displayCategory = (category, type) => {
  if (category.slug === 'sponsored-tech') {
    return <BrandedContentPostBadge />;
  }

  if (type === 'talk') {
    return <CommunityPostBadge />;
  }
  return <Link to={`/category/${category.slug}`}><GaType>{category.name.toUpperCase()}</GaType></Link>;
};

const Title = ({
  category,
  label,
  type,
}) => (
  <React.Fragment>
    {displayCategory(category, type)}
    <TitleStyled dangerouslySetInnerHTML={{ __html: label }} />
  </React.Fragment>
);

const CommunityPostBadge = () => (
  <Link to="/community">
    <Badge>Community Post</Badge>
  </Link>
);

const BrandedContentPostBadge = () => (
  <Link to="/category/sponsored-tech">
    <BadgeBC>Sponsored Tech</BadgeBC>
  </Link>
);

Title.defaultProps = {
  category: { 
    name: 'Startup',
    slug: 'startups',
  },
  isBrandedContent: false,
};

Title.propTypes = {
  category: PropTypes.object,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};

export default Title;
