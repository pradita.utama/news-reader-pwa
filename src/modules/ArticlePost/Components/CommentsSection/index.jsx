import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ErrorFeed from 'commons/errors/ErrorFeed';
import LoadingBar from 'commons/loading/LoadingBar';
import CommentSection from './Components';
import { requestComments } from './action';
import { Container } from './style';

class CommentsContainer extends React.Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    requestComments: PropTypes.func.isRequired,
    commentsReducer: PropTypes.object.isRequired,
    authReducer: PropTypes.object.isRequired,
  };

  componentDidMount() {
    const { id } = this.props;
    const postId = id;
    this.props.requestComments(postId);
  }

  reloadComments = () => {
    const { id } = this.props;
    const postId = id;
    this.props.requestComments(postId);
  };

  errorPostComment = () => setTimeout(() => {
    document.getElementById('error-post-comment').style.display = 'none';
  }, 2000)

  countComments = () => {
    const { comments } = this.props.commentsReducer;
    if (Array.isArray(comments)) {
      return comments.length;
    }
    return Object.getOwnPropertyNames(comments).length;
  }

  render() {
    const { user } = this.props.authReducer;
    const {
      id, comments, isLoading, hasError, hasErrorPostComment,
    } = this.props.commentsReducer;
    if (isLoading) {
      return (
        <Container>
          <LoadingBar />
        </Container>
      );
    }
    if (hasErrorPostComment) {
      document.getElementById('error-post-comment').style.display = 'block';
      this.errorPostComment();
    }
    if (hasError) {
      return (
        <Container>
          <ErrorFeed onClick={this.reloadComments} message="Gagal memuat komentar." />
        </Container>
      );
    }

    return (
      comments !== null && (
        <Container id={id}>
          <CommentSection
            postId={this.props.id}
            {...this.props.commentsReducer}
            auth={user}
            count={this.countComments()}
          />
        </Container>
      )
    );
  }
}

const mapStateToProps = ({ commentsReducer, authReducer }) => ({
  authReducer,
  commentsReducer,
});

export default connect(mapStateToProps, { requestComments })(CommentsContainer);
