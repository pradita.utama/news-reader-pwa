import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';

import ModalLoginOrRegister from '../../../Auth';
import CommentEditor from './Components/CommentEditor';
import CommentsList from './Components/CommentList';

const CommentsHeaderLogin = ({
  // eslint-disable-next-line
  modalId, changeModalId, modalIsOpen, openModal, closeModal
}) => (
  <React.Fragment>
    <div id="comments-section" className="comments-login">
      <span id="login" onClick={openModal} role="presentation" >Masuk</span> atau <span id="register" onClick={openModal} role="presentation" >Daftar</span> terlebih dahulu untuk mengirim komentar...
    </div>

    <ModalLoginOrRegister
      modalId={modalId}
      modalIsOpen={modalIsOpen}
      changeModalId={changeModalId}
      closeModal={closeModal}
    />
  </React.Fragment>
);

class CommentSection extends React.Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    count: PropTypes.number.isRequired,
    user: PropTypes.object,
  }

  static defaultProps = {
    user: {},
  }

  state = {
    modalIsOpen: false,
    modalId: 'login',
    isLogin: false,
    hasError: false,
  };

  componentDidMount() {
    this.checkLogin();
  }

  componentDidUpdate(prevProps) {
    const { user } = this.props;
    const isEmptyObject = isEmpty(user);

    if ((prevProps.user !== user) && !isEmptyObject) {
      this.setLogin();
    }

    // this code was a bad. Refactor!!
    // if (prevProps !== this.props) {
    //   this.setError();
    // }
  }

  setError = () => {
    this.setState({
      hasError: true,
    });
  }

  setLogin = () => {
    this.setState({
      modalIsOpen: false,
      isLogin: true,
    });
  }

  checkLogin = () => {
    const { auth } = this.props;
    if (Object.getOwnPropertyNames(auth).length > 0) {
      this.setLogin();
    }
  }

  openModal = (e) => {
    const { id } = e.currentTarget;

    this.setState({
      modalIsOpen: true,
      modalId: id,
    });
  }

  changeModalId = (id) => {
    this.setState({ modalId: id });
  }

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  }

  handleCommentEditor = () => {
    const error = {
      date: Date.now(),
    };
    if (this.state.hasError) {
      return (<CommentEditor
        hasError={error}
        {...this.props}
      />);
    }
    return <CommentEditor {...this.props} />;
  }

  render() {
    const { modalId, modalIsOpen, isLogin } = this.state;
    const { comments, count } = this.props;
    return (
      <React.Fragment>
        {
          !isLogin ? (
            <CommentsHeaderLogin
              modalId={modalId}
              changeModalId={this.changeModalId}
              modalIsOpen={modalIsOpen}
              openModal={this.openModal}
              closeModal={this.closeModal}
            />
          ) : (
            this.handleCommentEditor()
          )
        }
        <CommentsList {...comments} count={count} />
      </React.Fragment>
    );
  }
}


CommentsHeaderLogin.propTypes = {
  modalId: PropTypes.string.isRequired,
  modalIsOpen: PropTypes.bool.isRequired,
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
};

CommentSection.propTypes = {
  auth: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  comments: PropTypes.object.isRequired,
};

export default CommentSection;
