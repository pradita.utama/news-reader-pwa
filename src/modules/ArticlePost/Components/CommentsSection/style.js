import styled from 'styled-components';
import Button from '../../../../commons/ui-kit/Button/BaseButton';

const Container = styled.div`
  position: relative;
  float: left;
  width: 100%;
  margin-bottom: 20px;
  padding: 0 20px;

  span {
    color: #bf0016;
    cursor: pointer;
  }

  .comments-login {
    position: relative;
    padding: 20px;
    background-color: #eee;
    color: #999;
    text-align: center;
  }

  .comments-list-container {
    color: #555;
    font-size: 14px;

    &__header {
      display: flex;
      border-bottom: 1px solid #EDEDED;
      padding: 10px 0;

      &__total {
        flex: 1;
      }
  
      &__dropdown {
        flex: 1
      }
    } 

    &__comments {
      &__item {
        padding: 10px 0;

        .avatar {
          position: absolute;
          width: 40px;
        }
      
        .comment {
          word-break: break-word;
          margin-left: 50px;
          display: block;

          p {
            margin: 0;
            line-height: 1.31em;
          }

          > span {
            display: flex;
            justify-content: space-between;
            padding-top: 12px;
            time {
              font-size: 10px;
              color: #0000008a;
              font-weight: 200;
              margin-top: 2px;
            }
          }
        }
      }
    }
  }
  .comment-paragrapgh {
    margin: 25px 0 10px;
    iframe {
      width: 250px;
    }
  }

  .comment-button-wrapper {
    display: flex;
    flex-flow: row wrap;
    
      .comment-button{
        display: flex;
      }
      .comment-kebab {
        float: right;
        float: right;
        margin-left: auto;
      }
              
  }
`;

const CommentContainer = styled.div`
  width: 100%;
  height: 389px;
  background-color: #fafafa;

  .avatar-container {
    display: inline-flex;
    padding: 12px 0 0 12px;
    margin-bottom: 5px;
  }
  .comment-by {
    font-size: 12px;
    line-height: 1.33;
    font-weight: normal;
    color: #999999;
    margin-left: 10px;
    p {
      margin: 0
    }
    h6 {
      margin: 0;
      font-size: 16px;
      font-weight: normal;
      line-height: 1.31;
      color: #000000;
    }
  }
`;

const Error = styled.div`
  position: fixed;
  width: 100%;
  height: 31px;
  background-color: #f2543f;
  z-index: 100;
  bottom: 50px;
  display: none;
  transition: all 0.2s;
  transform: translateY(0%);
  
  .error-post-text {
    color: white;
    margin: 7px 0 0 20px;
    font-family: 'Proxima';
    font-weight: normal;
    font-size: 13px;
  }
`;

const Notify = styled.div`
  position: fixed;
  width: 100%;
  height: 31px;
  background-color: green;
  z-index: 100;
  bottom: 50px;
  display: none;
  transition-property: all;
  transition-duration: .5s;
  transition-timing-function: cubic-bezier(0, 1, 0.5, 1);
  
  .error-post-text {
    color: white;
    margin: 7px 0 0 20px;
    font-family: 'Proxima';
    font-weight: normal;
    font-size: 13px;
  }
`;

const ButtonLike = styled(Button)`
  color: #fff;
  width: 100%;
  border-radius: 4px;
  margin-bottom: 15px;
`;
export {
  Container,
  CommentContainer,
  Error,
  ButtonLike,
  Notify,
};
