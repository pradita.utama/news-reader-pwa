import {
  getComments,
  postComment,
  removeComment,
  upvotePostComment,
  replyPostComment,
  editPostComment,
} from './api';
import CONSTANT from './constant';
import generateUUID from '../../../../utils/idHelper';

const requestComments = slug => ({
  type: CONSTANT.REQUEST_COMMENTS,
  payload: {
    promise: getComments(slug),
  },
});

const submitComment = (postId, content, currentUser) => {
  if (content === '') {
    return {};
  }
  const tempId = generateUUID();
  return {
    type: CONSTANT.SUBMIT_COMMENT,
    payload: {
      promise: postComment(postId, content),
      content,
      currentUser,
      postId,
      tempId,
    },
  };
};

const deleteComment = id => ({
  type: CONSTANT.DELETE_COMMENT,
  payload: {
    promise: removeComment(id),
    id,
  },
});

const upvoteComment = (postId, commentId) => ({
  type: CONSTANT.UPVOTE_COMMENT,
  payload: {
    promise: upvotePostComment(commentId),
    commentId,
    postId,
  },
});

const replyComment = (postId, commentId, currentUser, content) => {
  if (content === '') {
    return {};
  }
  const tempId = generateUUID();
  return {
    type: CONSTANT.REPLY_COMMENT,
    payload: {
      promise: replyPostComment(commentId, content),
      content,
      commentId,
      currentUser,
      tempId,
      postId,
    },
  };
};

const editComment = (commentId, content) => ({
  type: CONSTANT.UPDATE_COMMENT,
  payload: {
    promise: editPostComment(commentId, content),
    content,
    commentId,
  },
});

export {
  requestComments,
  submitComment,
  deleteComment,
  upvoteComment,
  replyComment,
  editComment,
};
