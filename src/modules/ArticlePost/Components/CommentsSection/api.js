import call from '../../../../commons/api/RestApi';
import WordPressApi from '../../../../commons/api/WordPressApi';

const getComments = postId => call(`/posts/${postId}/comments?context=state`);

const postComment = (postId, content) => WordPressApi(`/posts/${postId}/comments`, 'POST', { content });

const removeComment = commentId => WordPressApi(`/comments/${commentId}`, 'DELETE');

const upvotePostComment = commentId => WordPressApi(`/comments/${commentId}/upvotes`, 'POST');

const replyPostComment = (commentId, content) => WordPressApi(`/comments/${commentId}`, 'POST', { commentId, content });

const editPostComment = (commentId, content) => WordPressApi(`/comments/${commentId}`, 'PUT', { commentId, content });

export {
  getComments,
  postComment,
  removeComment,
  upvotePostComment,
  replyPostComment,
  editPostComment,
};
