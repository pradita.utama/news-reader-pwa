const startLoadingPostComment = state => ({
  ...state,
  isLoadedPostComment: false,
  isLoadingPostComment: true,
  hasErrorPostComment: false,
});

const finishLoadingPostComment = state => ({
  ...state,
  isLoadedPostComment: true,
  isLoadingPostComment: false,
  hasErrorPostComment: false,
});

const resetLoading = state => ({
  ...state,
  isLoadedPostComment: false,
  isLoadingPostComment: false,
  hasErrorPostComment: false,
});

const errorPostComment = (state, error) => ({
  ...state,
  isLoadedPostComment: false,
  isLoadingPostComment: false,
  hasErrorPostComment: true,
  error,
});

export {
  startLoadingPostComment,
  finishLoadingPostComment,
  resetLoading,
  errorPostComment,
};
