import CONSTANT from './constant';
import {
  startLoading,
  finishLoading,
  errorLoading,
} from '../../../../utils/reducerUtils';
import {
  startLoadingPostComment,
  finishLoadingPostComment,
  errorPostComment,
} from './utils/commentsUtils';

const initialState = {
  comments: {},
  notify: '',
};

const commentsReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANT.REQUEST_COMMENTS: {
      return startLoading(state);
    }
    case CONSTANT.REQUEST_COMMENTS_ERROR: {
      return errorLoading(state, action);
    }
    case CONSTANT.REQUEST_COMMENTS_SUCCESS: {
      const finish = finishLoading(state);
      return {
        ...finish,
        ...action.payload,
      };
    }
    case CONSTANT.SUBMIT_COMMENT: {
      return startLoadingPostComment(state);
    }
    case CONSTANT.SUBMIT_COMMENT_SUCCESS: {
      const obj = {};
      const finish = finishLoadingPostComment(state);
      obj[action.payload.id] = { ...action.payload };

      return {
        ...finish,
        notify: CONSTANT.NOTIFY_POST_SUCCESS,
        comments: {
          ...obj,
          ...state.comments,
        },
      };
    }
    case CONSTANT.SUBMIT_COMMENT_ERROR: {
      return {
        ...errorPostComment(state, action.error),
        notify: CONSTANT.NOTIFY_POST_ERROR,
      };
    }
    case CONSTANT.DELETE_COMMENT: {
      return state;
    }
    case CONSTANT.DELETE_COMMENT_SUCCESS: {
      /* eslint-disable */
      let newComment;
      delete state.comments[action.payload.id];
      Object.entries(state.comments).forEach(([key]) => {
        if (state.comments[key].children) {
          newComment = state.comments[key].children.filter(child => child.id !== action.payload.id);
          state.comments[key].children = newComment;
        }
      });
      const finish = finishLoadingPostComment(state);
      return {
        ...finish,
        comments: {...finish.comments},
        notify: CONSTANT.NOTIFY_DELETE_SUCCESS
      };
    }
    case CONSTANT.DELETE_COMMENT_ERROR: {
      return {
        ...errorPostComment(state, action.error),
        notify: CONSTANT.NOTIFY_DELETE_ERROR
      }
    }
    case CONSTANT.UPVOTE_COMMENT_SUCCESS: {
      Object.entries(state.comments).forEach(([key, value]) => {
        const upvotes = state.comments[action.payload.id].upvotes
        if (key === action.payload.id) {
          state.comments[action.payload.id].upvotes = upvotes + 1;
          state.comments[action.payload.id].hasUpvoted = true;
        }
        return state.comments;
      });
      return {
        ...state,
      };
    }
    case CONSTANT.REPLY_COMMENT: {
      return startLoadingPostComment(state);
    }
    case CONSTANT.REPLY_COMMENT_ERROR: {
      return {
        ...errorPostComment(state, action.error),
        notify: CONSTANT.NOTIFY_POST_ERROR,
      };
    }
    case CONSTANT.REPLY_COMMENT_SUCCESS: {
      const obj = {};
      const finish = finishLoadingPostComment(state);
      obj[action.payload.id] = { ...action.payload };

      return {
        ...finish,
        comments: {
          ...obj,
          ...state.comments,
        },
        notify: CONSTANT.NOTIFY_POST_SUCCESS,
      };
    }
    case CONSTANT.UPDATE_COMMENT: {
      return startLoadingPostComment(state);
    }
    case CONSTANT.UPDATE_COMMENT_SUCCESS: {
      const finish = finishLoadingPostComment(state);
      state.comments[action.payload.id] = {...action.payload}
      return {
        ...finish,
        comments: {
          ...state.comments
        },
        notify: CONSTANT.NOTIFY_UPDATE_SUCCESS,
      }
    }
    case CONSTANT.UPDATE_COMMENT_ERROR: {
      return { 
        ...errorPostComment(state, action.error),
        notify: CONSTANT.UPDATE_COMMENT_ERROR,
      }
    }

    default: {
      return state;
    }
  }
};

export { commentsReducer };
