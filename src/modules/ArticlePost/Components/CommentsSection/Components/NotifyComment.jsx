import React from 'react';
import PropTypes from 'prop-types';

import { Notify } from '../style';

const NotifyComponent = props => (
  <Notify id="notify-post-comment">
    <p className="error-post-text"> {props.message} </p>
  </Notify>
);

NotifyComponent.propTypes = {
  message: PropTypes.string.isRequired,
};

export default NotifyComponent;
