import React from 'react';
import PropTypes from 'prop-types';

import Avatar from '../../../../../commons/ui-kit/Avatar';

const CommentProfile = ({
  avatarUrl, displayName, firstName, lastName,
}) => (
  <div className="avatar-container"><Avatar src={avatarUrl} alt={displayName} />
    <div className="comment-by">
      <p> komentar sebagai </p>
      <h6> { `${firstName} ${lastName}` } </h6>
    </div>
  </div>
);

CommentProfile.propTypes = {
  avatarUrl: PropTypes.string.isRequired,
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
  displayName: PropTypes.string,
};

CommentProfile.defaultProps = {
  displayName: 'TIA',
};

export default CommentProfile;
