import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
  deleteComment,
  upvoteComment,
  replyComment,
  editComment,
} from '../action';

import Comments from './Comments';
import ButtonSection from './ButtonSection';
import CommentEditor from './CommentEditor';

class Comment extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    children: PropTypes.array,
    deleteComment: PropTypes.func.isRequired,
    editComment: PropTypes.func.isRequired,
    isNew: PropTypes.bool,
    post: PropTypes.string.isRequired,
    upvoteComment: PropTypes.func.isRequired,
    authReducer: PropTypes.shape({
      user: PropTypes.object,
    }).isRequired,
    replyComment: PropTypes.func.isRequired,
    commentsReducer: PropTypes.shape({
      isLoadedPostComment: PropTypes.bool,
      hasErrorPostComment: PropTypes.bool,
    }),
  };

  static defaultProps = {
    children: undefined,
    isNew: false,
    commentsReducer: {
      isLoadedPostComment: false,
      hasErrorPostComment: false,
    },
  }
  constructor(props) {
    super(props);
    this.state = {
      isReply: false,
      isEdit: false,
    };
  }

  componentDidUpdate(prevProps) {
    const { commentsReducer: { isLoadedPostComment } } = this.props;
    if (!prevProps.commentsReducer.isLoadedPostComment && isLoadedPostComment) {
      this.handleUpdateReducer();
    }
  }
  handleReply = () => {
    this.setState({
      isReply: !this.state.isReply,
      isEdit: false,
    });
  }

  handleDelete = () => {
    const { id } = this.props;
    this.props.deleteComment(id);
  }

  handleUpvote = () => {
    const { id, post } = this.props;
    this.props.upvoteComment(post, id);
  }

  handleEdit = () => {
    this.setState({
      isEdit: !this.state.isEdit,
      isReply: false,
    });
  }

  handleSubmit = (content) => {
    const {
      authReducer: { user },
      post,
      id,
    } = this.props;
    this.props.replyComment(post, id, user, content);
  }

  handleUpdate = (content) => {
    const { id } = this.props;
    this.props.editComment(id, content);
  }

  handleUpdateReducer = () => {
    this.setState({
      isReply: false,
      isEdit: false,
    });
  }

  htmlRemover = (content) => {
    const regex = /(<([^>]+)>)/ig;
    return content.replace(regex, '');
  }
  render() {
    const {
      content,
      children,
    } = this.props;
    const { authReducer: { user } } = this.props;
    return (
      <React.Fragment>
        <div
          style={this.props.isNew ? { background: '#ffed86', opacity: '0.8' } : { background: 'white' }}
        >
          <ButtonSection
            {...this.props}
            onUpvote={this.handleUpvote}
            onReply={this.handleReply}
            onDelete={this.handleDelete}
            onEdit={this.handleEdit}
            toggleOptions={this.handleUpdateReducer}
          />
          { this.state.isReply && (
            <CommentEditor
              auth={user}
              postId={this.props.post}
              onReply={this.handleSubmit}
              hasErrorPostComment={this.props.commentsReducer.hasErrorPostComment}
            />
            )}
          { this.state.isEdit && (
            <CommentEditor
              auth={user}
              postId={this.props.post}
              onUpdate={this.handleUpdate}
              placeHolder={this.htmlRemover(content)}
              hasErrorPostComment={this.props.commentsReducer.hasErrorPostComment}
            />
          )}
        </div>
        {children !== undefined ? <Comments comments={children} /> : null}
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ authReducer, commentsReducer }) => ({
  authReducer,
  commentsReducer,
});

export default connect(mapStateToProps, {
  deleteComment,
  upvoteComment,
  replyComment,
  editComment,
})(Comment);
