import styled from 'styled-components';
import ModalAdapter from './ModalAdapter';

const Close = styled.button`
  background: transparent;
  border: 0;
  float: right;
`;

const StyledModal = styled(ModalAdapter)`

  &__Overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 100;
    background-color: rgba(51, 51, 51, 0.5);
    opacity: 0;

  }

  &__Overlay--after-open {
    opacity:1;
    transition: opacity 0.2s ease-in;

     .sub-item{
       height: 50px;
       transition: height 0.33s ease-in;
     }
   
  }
  &__Overlay--before-close {
    opacity: 0;
    transition: opacity 0.2s ease-in;
  }

  &__Content {
    position: absolute;
    top: auto;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: white;
    padding: 0 10px 0;
    outline: none;
  }
`;

const ButtonContain = styled.div`
  padding: 5px 10px 2px;
`;

const ButtonModal = styled.button`
  line-height: 32px;
  font-size: 12px;
  outline: none;

  .active {
    color: #f2543f;
  }

`;

export {
  Close,
  StyledModal,
  ButtonModal,
  ButtonContain,
};

