import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import asyncComponent from 'AsyncComponent';

import { deleteComment } from '../action';

const ModalDelete = asyncComponent(() => import('./ModalDelete'));

class ButtonDelete extends Component {
  static propTypes = {
    canDelete: PropTypes.bool.isRequired,
    id: PropTypes.string.isRequired,
    deleteComment: PropTypes.func.isRequired,
    commentsReducer: PropTypes.shape({
      hasErrorPostComment: PropTypes.bool,
    }).isRequired,
  };

  state = {
    modalIsOpen: false,
  };

  componentDidUpdate(prevProps) {
    const { commentsReducer: { hasErrorPostComment } } = this.props;
    if (prevProps.commentsReducer.hasErrorPostComment !== hasErrorPostComment) {
      this.closeModal();
    }
  }

  openModal = () => {
    this.setState({ modalIsOpen: true });
  }


  closeModal = () => {
    this.setState({ modalIsOpen: false });
  }

  handleDelete = () => {
    const { id } = this.props;
    this.props.deleteComment(id);
  }

  render() {
    const { canDelete } = this.props;
    const { modalIsOpen } = this.state;
    if (canDelete) {
      return (
        <React.Fragment>
          <button
            style={{
              lineHeight: '32px',
              fontSize: '12px',
              fontWeight: 'normal',
              fontStyle: 'normal',
              fontStretch: 'normal',
              letterSpacing: 'normal',
              color: '#f2543f',
              outline: 'none',
            }}
            onClick={this.openModal}
          >
            Hapus
          </button>
          <ModalDelete
            open={modalIsOpen}
            handleClose={this.closeModal}
            handleDelete={this.handleDelete}
          />
        </React.Fragment>
      );
    }
    return null;
  }
}

export default connect(null, {
  deleteComment,
})(ButtonDelete);
