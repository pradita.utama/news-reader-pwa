import React from 'react';
import LazyLoad from 'react-lazyload';
import PropTypes from 'prop-types';

import Comment from './CommentItem';

class Comments extends React.Component {
  state = {
    isNew: 0,
  }

  componentDidUpdate(prevProps) {
    if (prevProps.comments.length < this.props.comments.length) {
      this.handelIsNew();
    }
  }

  handelIsNew = () => {
    this.setState({
      isNew: this.props.comments[0].id,
    });
    this.handleSetTimeout();
  };

  handleSetTimeout = () => setTimeout(() => {
    this.setState({
      isNew: 0,
    });
  }, 2400)

  render() {
    const { comments } = this.props;
    return (
      <React.Fragment>
        {
        comments.map(comment => (
          <LazyLoad key={comment.id.toString()} height={200} offset={10}>
            <React.Fragment>
              {
                comment.id === this.state.isNew ? (
                  <Comment isNew {...comment} />
                ) : (
                  <Comment {...comment} />
                )
              }
            </React.Fragment>
          </LazyLoad>
        ))
      }
      </React.Fragment>
    );
  }
}

Comments.propTypes = {
  comments: PropTypes.array.isRequired,
};

export default Comments;
