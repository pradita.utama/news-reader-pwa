import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';

Modal.setAppElement('#root');

const ModalAdapter = ({ className, ...props }) => {
  const contentClassName = `${className}__Content`;
  const overlayClassName = `${className}__Overlay`;

  return (
    <Modal
      portalClassName={className}
      className={contentClassName}
      overlayClassName={{
        base: overlayClassName,
        afterOpen: `${overlayClassName}--after-open`,
        beforeClose: `${overlayClassName}--before-close`,
      }}
      {...props}
    />
  );
};

ModalAdapter.propTypes = {
  className: PropTypes.string,
};

ModalAdapter.defaultProps = {
  className: '',
};

export default ModalAdapter;
