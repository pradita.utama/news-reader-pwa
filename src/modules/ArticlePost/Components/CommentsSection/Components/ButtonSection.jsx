import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from 'commons/ui-kit/Button/BaseButton';
import Icon from 'commons/ui-kit/Icon';
import Avatar from 'commons/ui-kit/Avatar';

import ButtonOptions from './ButtonOptionsComment';
import CommentAuthor from './CommentAuthor';
import ButtonModalDelete from './ButtonModalDelete';

class ButtonSection extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    parent: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    author: PropTypes.object.isRequired,
    hasUpvoted: PropTypes.bool.isRequired,
    upvotes: PropTypes.number.isRequired,
    onUpvote: PropTypes.func.isRequired,
    canDelete: PropTypes.bool.isRequired,
    canEdit: PropTypes.bool.isRequired,
    onReply: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired,
    toggleOptions: PropTypes.func.isRequired,
  };

  state = {
    showEdit: false,
  };

  setRenderButtonUpvotes = () => {
    const { hasUpvoted, upvotes } = this.props;
    if (hasUpvoted) {
      return (
        <Button
          style={{ paddingLeft: '37px' }}
          label={upvotes}
          primary
          iconWithLabel={this.IconLikeUpvoted}
        />
      );
    }
    return (
      <Button
        style={{ paddingLeft: '37px' }}
        label={upvotes}
        onClick={this.props.onUpvote}
        iconWithLabel={this.IconLike}
      />
    );
  };

  setRenderButtonEdit = () => {
    const { canEdit } = this.props;
    if (canEdit) {
      return <Button label="Ubah Komentar" onClick={this.props.onEdit} />;
    }
    return null;
  };
  IconLike = () => <Icon name="like" />;
  IconLikeUpvoted = () => <Icon name="like-upvoted" />;

  handleButtonOptions = () => {
    this.props.toggleOptions();
    this.setState({
      showEdit: !this.state.showEdit,
    });
  };

  render() {
    const {
      content, author, id, parent, canDelete, canEdit,
    } = this.props;
    return (
      <div
        id={id}
        className="comments-list-container__comments__item"
        style={parent !== '0' ? { marginLeft: '44px' } : null}
      >
        <div className="avatar">
          <Avatar src={author.avatarUrl} alt={author.displayName} />
        </div>
        <div className="comment">
          <CommentAuthor {...this.props} />
          <div className="comment-paragrapgh">
            <div dangerouslySetInnerHTML={{ __html: content }} />
          </div>
          <div className="comment-button-wrapper">
            <div className="comment-button">
              {!this.state.showEdit ? (
                <React.Fragment>
                  {this.setRenderButtonUpvotes()}
                  <Button label="Balas" onClick={this.props.onReply} />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <ButtonModalDelete {...this.props} />
                  {this.setRenderButtonEdit()}
                </React.Fragment>
              )}
            </div>
            <div className="comment-kebab">
              {canEdit || canDelete ? (
                <ButtonOptions
                  showEdit={this.state.showEdit}
                  onOptions={this.handleButtonOptions}
                  isEdit={this.state.showEdit}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ButtonSection;
