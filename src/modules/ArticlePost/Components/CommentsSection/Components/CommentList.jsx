import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Comments from './Comments';
import Dropdown from '../../../../../commons/ui-kit/Dropdown';

const getNestedChildren = (arr, parent) => {
  const out = [];
  for (let i = 0; i < arr.length; i += 1) {
    if (arr[i].parent === parent) {
      const children = getNestedChildren(arr, arr[i].id);

      if (children.length) {
        arr[i].children = children; // eslint-disable-line no-param-reassign
      }
      out.push(arr[i]);
    }
  }
  return out;
};

const commentsArray = (comments, option) => {
  let arrayComments = [];
  const arr = [];
  if (option === 'Terbaik') {
    const data = { ...comments };
    /* eslint-disable */
    Object.entries(data).forEach(([item, value]) => {
      arr.push(value);
    });
    const byUpvotes = arr.slice(0);
    byUpvotes.sort((a, b) => a.upvotes < b.upvotes);
    arrayComments = [...byUpvotes];
  } else {
    Object.entries(comments).forEach(([item, value]) => {
      if (option === 'Terbaru') {
        arrayComments = [value, ...arrayComments];
      } else if (option === 'Terlama') {
        arrayComments = [...arrayComments, value];
      }
    });
  }
    /* eslint-enable */
  return getNestedChildren(arrayComments, '0');
};

class CommentsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: 'Terbaru',
    };
  }

  handleChange = (option) => {
    this.setState({
      options: option,
    });
  }
  render() {
    const { count } = this.props;
    const options = ['Terbaru', 'Terlama', 'Terbaik'];
    return (
      <div className="comments-list-container">
        <div className="comments-list-container__header">
          <div className="comments-list-container__header__total">{count} komentar</div>
          <div className="comments-dropdown__header__dropdown">
            <Dropdown changed={this.handleChange} options={options} />
          </div>
        </div>
        <div className="comments-list-container__comments" id="comment">
          <Comments comments={commentsArray(this.props, this.state.options)} />
        </div>
      </div>
    );
  }
}

CommentsList.propTypes = {
  count: PropTypes.number.isRequired,
};

export default CommentsList;
