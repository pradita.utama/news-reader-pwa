import React from 'react';
import PropTypes from 'prop-types';

import icon from 'images/Navigation/icon-kebab.svg';
import Button from 'commons/ui-kit/Button/BaseButton';
import iconClose from 'images/icons/close-grey.svg';

const ButtonOptions = (props) => {
  const { isEdit, onOptions } = props;
  if (!isEdit) {
    return (
      <Button
        icon="true"
        src={icon}
        style={{
                transform: 'rotate(90deg)',
                padding: '3px',
              }}
        onClick={onOptions}
      />
    );
  }
  return (
    <Button
      icon="true"
      src={iconClose}
      style={{
            padding: '3px',
          }}
      onClick={onOptions}
    />
  );
};

ButtonOptions.propTypes = {
  onOptions: PropTypes.func.isRequired,
  isEdit: PropTypes.bool.isRequired,
};

export default ButtonOptions;
