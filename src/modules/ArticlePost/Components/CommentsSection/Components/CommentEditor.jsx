import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { CommentContainer } from '../style';
import Editor from '../../../../CommentEditor';
import { submitComment } from '../action';
import CommentProfile from './CommentProfile';


class CommentEditor extends Component {
  static propTypes = {
    submitComment: PropTypes.func.isRequired,
    postId: PropTypes.string.isRequired,
    hasErrorPostComment: PropTypes.bool,
    isLoadedPostComment: PropTypes.bool,
    auth: PropTypes.object.isRequired,
    onReply: PropTypes.func,
    onUpdate: PropTypes.func,
    placeHolder: PropTypes.string,
  }
  static defaultProps = {
    hasErrorPostComment: false,
    isLoadedPostComment: false,
    onReply: null,
    onUpdate: null,
    placeHolder: 'Apa yang ingin kamu sampaikan?',
  }

  handleSubmit = (content) => {
    const {
      postId, auth, onReply, onUpdate,
    } = this.props;
    if (onReply) {
      this.props.onReply(content);
    } else if (onUpdate) {
      this.props.onUpdate(content);
    } else {
      this.props.submitComment(postId, content, auth);
    }
  }

  handleSend = () => {
    const elmnt = document.getElementById('comment');
    setTimeout(() => { elmnt.scrollIntoView({ block: 'start', behavior: 'smooth' }); }, 300);
    document.getElementById('notify-post-comment').style.display = 'block';
    this.successPostComment();
  }

  successPostComment = () => setTimeout(() => {
    document.getElementById('notify-post-comment').style.display = 'none';
  }, 2000)

  render() {
    const {
      isLoadedPostComment, hasErrorPostComment, auth, placeHolder,
    } = this.props;
    if (isLoadedPostComment) {
      this.handleSend();
      return (
        <CommentContainer>
          <CommentProfile {...auth} />
          <Editor
            isSend
            onSubmit={this.handleSubmit}
            placeHolder={placeHolder}
          />
        </CommentContainer>
      );
    }

    if (hasErrorPostComment) {
      return (
        <CommentContainer>
          <CommentProfile {...auth} />
          <Editor
            isError
            onSubmit={this.handleSubmit}
            placeHolder={placeHolder}
          />
        </CommentContainer>
      );
    }

    return (
      <React.Fragment>
        <CommentContainer>
          <CommentProfile {...this.props.auth} />
          <Editor
            onSubmit={this.handleSubmit}
            placeHolder={placeHolder}
          />
        </CommentContainer>
      </React.Fragment>
    );
  }
}

export default connect(null, { submitComment })(CommentEditor);
