import React from 'react';
import PropTypes from 'prop-types';

import { StyledModal, ButtonModal, ButtonContain } from './style';

class Component extends React.Component {
  state = {
    disabled: false,
  }
  handleClick = () => {
    this.setState({
      disabled: true,
    });
    this.props.handleDelete();
  }

  render() {
    return (
      <StyledModal
        isOpen={this.props.open}
        onRequestClose={this.props.handleClose}
        closeTimeoutMS={250}
      >
        <ButtonContain>
          <ButtonModal
            className="active"
            style={{
              color: '#f2543f',
            }}
            onClick={this.handleClick}
            disabled={this.state.disabled}
          >
            Hapus Komentar
          </ButtonModal>
        </ButtonContain>
        <ButtonContain>
          <ButtonModal
            onClick={this.props.handleClose}
            disabled={this.state.disabled}
          >
            Batal
          </ButtonModal>
        </ButtonContain>
      </StyledModal>
    );
  }
}

Component.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
};

export default Component;
