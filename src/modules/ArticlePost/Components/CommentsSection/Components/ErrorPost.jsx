import React from 'react';
import PropTypes from 'prop-types';

import { Error } from '../style';

const ErrorComponent = props => (
  <Error id="error-post-comment">
    <p className="error-post-text"> {props.message} </p>
  </Error>
);

ErrorComponent.propTypes = {
  message: PropTypes.string.isRequired,
};

export default ErrorComponent;
