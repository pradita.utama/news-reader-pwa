import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { diffForHumans } from '../../../../../utils/dateUtils';

const Author = ({ author, date }) => (
  <span>
    <Link
      to={author.authorUrl.replace('https://id.techinasia.com', '')}
    >
      {author.displayName}
    </Link>
    <time>
      {diffForHumans(date)}
    </time>
  </span>
);

Author.propTypes = {
  author: PropTypes.object.isRequired,
  date: PropTypes.string.isRequired,
};

export default Author;

