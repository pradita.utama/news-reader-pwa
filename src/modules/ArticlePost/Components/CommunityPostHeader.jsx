import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import Icon from '../../../images/icons/info.svg';

const CommunityFlagWrapper = styled.div`
  display: inline-block;
  background-color: #fafafa;
  height: auto;
  border-bottom: 1px solid #EDEDED;
  position: relative;
  padding: 0 20px 0 20px;
`;

const CommunityFlag = styled.div`
  float: none;
  padding: 0;
`;

const CommunityFlagContent = styled.div`
  font-size: 16px;
  line-height: 26px;
  word-wrap: break-word;
  
  .icon {
    width: 25px;
    margin-top: 15px;
  }

  p {
    font-style: italic;
    margin-top: 0;
  }
`;

const CommunityPostHeader = ({ displayName }) => (
  <CommunityFlagWrapper>
    <CommunityFlag>
      <CommunityFlagContent>
        <img className="icon" src={Icon} alt="info icon" />
        <p>
          Ini adalah artikel <Link to="/community">Community Post </Link>
          Tech in Asia Indonesia. Community Post merupakan wadah bagi para
          profesional yang senang membuat konten, untuk berbagi pengetahuan
          dan pengalaman seputar ekosistem startup, teknologi, dan profesional.
          Isi di dalam artikel sepenuhnya menjadi tanggung jawab penulis.
          Opini penulis tidak mencerminkan pandangan Tech in Asia Indonesia.
        </p>
        <p>
          Kamu dapat membuat artikel Community Post seperti yang {displayName} lakukan,
          klik <Link to="/create/article">disini</Link>
        </p>
      </CommunityFlagContent>
    </CommunityFlag>
  </CommunityFlagWrapper>
);

CommunityPostHeader.propTypes = {
  displayName: PropTypes.string.isRequired,
};

export default CommunityPostHeader;
