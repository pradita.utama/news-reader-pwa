import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Avatar from '../../../commons/ui-kit/Avatar';
import {
  getLink,
  humanReadableDate,
  fullDate,
} from '../utils/PostUtils';

const Container = styled.div`
  width: 100%;
`;

const Ul = styled.ul`
  float: none;
  margin: 0;
  padding: 0;
`;

const AuthorImage = styled.li`
  margin-right: 12px;
  overflow: hidden;
  display: inline-block;
`;


const Li = styled.li`
  display: inline-block;
  vertical-align: top;
  margin-top: 4px;
`;

const Time = styled.time`
  color: #999;
  display: block;
  font-size: 13px;
`;

const renderDate = (date, ago) => (
  <Time datetime={date}>
    { ago ? humanReadableDate(date) : fullDate(date)}
  </Time>
);

const PostMeta = ({
  publishDate, authorMeta, ago, hidePublishDate,
}) => {
  const { avatarUrl, displayName, authorUrl } = authorMeta;

  return (
    <React.Fragment>
      <Container>
        <Ul >
          <AuthorImage>
            <Avatar src={avatarUrl} alt={displayName} />
          </AuthorImage>
          <Li>
            <Link rel="author" to={getLink(authorUrl)}>{displayName}</Link>
            { hidePublishDate ? null : renderDate(publishDate, ago) }
          </Li>
        </Ul>
      </Container>
    </React.Fragment>
  );
};

PostMeta.defaultProps = {
  ago: false,
  hidePublishDate: false,
  publishDate: null,
};

PostMeta.propTypes = {
  authorMeta: PropTypes.object.isRequired,
  ago: PropTypes.bool,
  publishDate: PropTypes.string,
  hidePublishDate: PropTypes.bool,
};

export default PostMeta;
