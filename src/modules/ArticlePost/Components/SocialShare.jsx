import React from 'react';
import PropTypes from 'prop-types';

import { TitleStyled } from '../style';

const SocialShare = ({ label }) => (
  <TitleStyled dangerouslySetInnerHTML={{ __html: label }} />
);

SocialShare.propTypes = {
  label: PropTypes.string.isRequired,
};

export default SocialShare;
