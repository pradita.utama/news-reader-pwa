import styled from 'styled-components';

const ReviewContainer = styled.div`
  border-bottom: 1px solid #EDEDED;

  .review-result-container {
    margin: 20px;
    background-color: #fff;
    border: 1px solid #EDEDED;
    box-shadow: 0 0px 1px 0 rgba(0,0,0,.16);
  }

  .review-header {
    background-color: #bf0016;
    padding: 10px;
    color: #ffF;
    height: 100px;
  }

  .review-result-score {
    background-color: #d8001e;
    width: 110px;
    height: 110px;
    border-radius: 100%;
    position: absolute;
    left: calc(50% - 55px);
    margin-top: 30px;
    color: #fff;
    position: absolute;
    text-align: center;
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.16);

    span {
      font-weight: 700;
      font-size: 50px;
      margin-top: 10px;
      position: relative;
      top: 20px;
    }

    p {
      position: relative;
      bottom: 5px;
    }
  }

  .review-scores {
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #fff;

    .score-item {
      background-color: #fff;
      border-radius: 100%;
      width: 70px;
      height: 70px;
      margin: 50px 10px 20px 10px;
      box-shadow: 0 1px 2px 0 rgba(0,0,0,.16);
      text-align: center;

      span {
        font-weight: 700;
        font-size: 28px;
        top: 17px;
        position: relative;
      }

      p {
        font-size: 11px;
        font-weight: 200;
      }
    }
  }

  .review-footer {
    text-align: center;
    padding: 30px 10px 10px 10px;
    height: 80px;

    hr {
      border: 0;
      height: 0;
      border-top: 1px solid rgba(0, 0, 0, 0.1);
      border-bottom: 1px solid rgba(255, 255, 255, 0.3);
      width: 80%;
    }

    img {
      width: 55px;
      padding: 10px;
      position: relative;
      bottom: 35px;
      background-color: #fff;
    }
  }

  .review-pro-container {
    margin: 20px;
    border: 1px solid #EDEDED;
    box-shadow: 0 0px 1px 0 rgba(0,0,0,.16);

    .review-pro {
      background-color: #5cb85c;
      color: #fff;
      height: 40px;
      display: flex;
      align-items: center;
      padding-left: 10px;
    }

    .review-pro-list {
      padding: 10px;
      color: #000000b5;
      font-size: 14px;

      ul {
        list-style: none;
        padding-left: 10px;
      }

      li {
        margin: 10px 0;
      }
    }
  }

  .review-cons-container {
    margin: 20px;
    border: 1px solid #EDEDED;
    box-shadow: 0 0px 1px 0 rgba(0,0,0,.16);

    .review-cons {
      background-color: #474747;
      color: #fff;
      height: 40px;
      display: flex;
      align-items: center;
      padding-left: 10px;
    }

    .review-cons-list {
      padding: 10px;
      color: #000000b5;
      font-size: 14px;

      ul {
        list-style: none;
        padding-left: 10px;
      }

      li {
        margin: 10px 0;
      }
    }
  }
`;

export { ReviewContainer };
