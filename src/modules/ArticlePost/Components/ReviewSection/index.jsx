import React from 'react';
import PropTypes from 'prop-types';
import { ReviewContainer } from './style';
import Icon from '../../../../images/logo.png';

const Review = ({ finalScore, score, summary }) => (
  <ReviewContainer>
    <div className="review-result-container">
      <div className="review-result-score">
        <span>{finalScore}</span>
        <p>Nilai Akhir</p>
      </div>
      <div className="review-header">Nilai</div>
      <div className="review-scores">
        {
          score.map(item => (
            item.criteria !== '' && (
              <React.Fragment key={item.criteria}>
                <div className="score-item">
                  <span>{item.score.length === 1 ? `${item.score}.0` : item.score}</span>
                  <p>{item.criteria}</p>
                </div>
              </React.Fragment>
            )
          ))
        }
      </div>
      <div className="review-footer">
        <hr />
        <img src={Icon} alt="TIA Logo" />
      </div>
    </div>

    <div className="review-pro-container">
      <div className="review-pro">Pros</div>
      <div className="review-pro-list">
        <ul>
          {
            summary.pros.map((item, index) => (<li key={index.toString()}>{item.trim().replace(/<(.|\n)*?>/g, '')}</li>))
          }
        </ul>
      </div>
    </div>

    <div className="review-cons-container">
      <div className="review-cons">Cons</div>
      <div className="review-cons-list">
        <ul>
          {
            summary.cons.map((item, index) => (<li key={index.toString()}>{item.trim().replace(/<(.|\n)*?>/g, '')}</li>))
          }
        </ul>
      </div>
    </div>
  </ReviewContainer>
);

Review.propTypes = {
  finalScore: PropTypes.string.isRequired,
  score: PropTypes.array.isRequired,
  summary: PropTypes.object.isRequired,
};

export default Review;
