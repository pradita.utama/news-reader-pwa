import React from 'react';
import PropTypes from 'prop-types';

// import FeaturedImage from './FeaturedImage';
import Title from './Title';
import PostMeta from './PostMeta';
import { Container, BrandedContentStyle } from '../style';

import { getParentCategory } from '../utils/PostUtils';

const PostHeader = ({
  title, author, date, type, categories, clientName, clientLogo,
}) => (
  <Container>
    <Title
      label={title}
      category={getParentCategory(categories)}
      type={type}
    />
    { getParentCategory(categories).slug === 'sponsored-tech' ? <BrandedContentClientLogo clientLogo={clientLogo} clientName={clientName} /> : null }
    <PostMeta authorMeta={author} publishDate={date} />
  </Container>
);

const BrandedContentClientLogo = ({ clientLogo }) => (
  <BrandedContentStyle>
    <div>Dipersembahkan oleh</div>
    <img src={clientLogo} alt="Logo" />
  </BrandedContentStyle>
);

BrandedContentClientLogo.propTypes = {
  clientLogo: PropTypes.string,
};

BrandedContentClientLogo.defaultProps = {
  clientLogo: null,
};

PostHeader.defaultProps = {
  clientName: null,
  clientLogo: null,
};

PostHeader.propTypes = {
  title: PropTypes.string.isRequired,
  author: PropTypes.object.isRequired,
  date: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  categories: PropTypes.array.isRequired,
  clientName: PropTypes.string,
  clientLogo: PropTypes.string,
};

export default PostHeader;
