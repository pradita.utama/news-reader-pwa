import React from 'react';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import convert from 'htmr';

import Link from './Link';
import CommunityPostHeader from './CommunityPostHeader';
import Review from './ReviewSection';
import RelatedArticleContainer from './RelatedArticle';
import { ContentStyled } from '../style';

import { formatContent } from '../utils/PostUtils';

const map = {
  a: Link,
};

class Content extends React.Component {
  componentDidMount() {

  }

  render() {
    const {
      content,
      type,
      author,
      slug,
      review,
    } = this.props;

    return (
      <React.Fragment>
        { type === 'talk' ? <CommunityPostHeader {...author} /> : null }
        <ContentStyled>
          {convert(formatContent(content), { map })}
        </ContentStyled>
        { review !== null ?
          <LazyLoad height={150} offset={10}>
            <Review {...review} />
          </LazyLoad>
          : null }
        <LazyLoad height={400} offset={10}>
          <RelatedArticleContainer slug={slug} />
        </LazyLoad>
      </React.Fragment>
    );
  }
}

Content.defaultProps = {
  review: null,
};

Content.propTypes = {
  content: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  author: PropTypes.object.isRequired,
  slug: PropTypes.string.isRequired,
  review: PropTypes.object,
};

export default Content;
