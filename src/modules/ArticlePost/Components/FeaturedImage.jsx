import React from 'react';
import styled, { keyframes } from 'styled-components';
import PropTypes from 'prop-types';
import ImageLoader from 'react-load-image';


const Image = styled.img`
  width: 100%;
  height: auto;
`;

const slide = keyframes`
  0% {
    left: 0;
  }
  50% {
    left: 75%;
  }
  100% {
    left: 0;
  }
`;

const StyledLoading = styled.div`
  position: relative;
  display: flex;
  height: 220px;
  width: 100%;
  align-items: center;
  justify-content: center;
  background-color: #d8d8d866;
`;

const StyledLine = styled.div`
  position: relative;
  display: inline-block;
  border-radius: 10px;
  width: 250px;
  height: 5px;
  background: #ddd;

  &:before{
    content: '';
    position: absolute;
    width: 25%;
    height: 5px;
    left: 0;
    background: #a5a0a185;
    animation: ${slide} 1.5s infinite linear;
  }
`;

// const Preloader = () => <div>LOADING</div>;
const ErrorLoad = () => <Image src="/static/images/fallback-image.svg" alt="Gambar tidak dapat diunduh" />;

const FeaturedImage = ({ featuredImage, title }) => (
  <React.Fragment>
    <ImageLoader
      src={featuredImage.source}
    >
      <Image title={title} />
      <ErrorLoad />
      <Loading />
    </ImageLoader>
  </React.Fragment>
);

const Loading = () => (
  <StyledLoading>
    <StyledLine />
  </StyledLoading>
);

FeaturedImage.propTypes = {
  featuredImage: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
};

export default FeaturedImage;
