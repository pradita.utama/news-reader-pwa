import call from '../../commons/api/RestApi';

const getPost = slug => call(`/posts/${slug}`);

export {
  getPost,
};
