import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import LazyLoad from 'react-lazyload';
import { Redirect } from 'react-router-dom';

import LoadingPlaceholder from 'commons/loading/LoadingPlaceholder';
import { requestPost } from './action';
import asyncComponent from '../../AsyncComponent';
import ErrorPage from '../../commons/errors/ErrorPage';
import Content from './Components/Content';
import PostHeader from './Components/PostHeader';
import FeaturedImage from './Components/FeaturedImage';
import ArticleFooter from './Components/Footer';
import CommentsContainer from './Components/CommentsSection';
import PopularArticleContainer from './Components/PopularArticle';
import ShareIcons from './Components/ShareIconsSection';

import { parseTitle as parseTitleFromHelmet } from '../../utils/helmetUtils';
import { stripTrailingSlash } from '../../helpers/UrlHelper';
import Error from './Components/CommentsSection/Components/ErrorPost';
import NotifyInfo from './Components/CommentsSection/Components/NotifyComment';

const AuthorBox = asyncComponent(() => import('./Components/AuthorBox'));

class Article extends React.Component {
  static propTypes = {
    location: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    requestPost: PropTypes.func.isRequired,
    postReducer: PropTypes.object.isRequired,
    commentsReducer: PropTypes.object.isRequired,
  };

  componentDidMount() {
    window.scrollTo(0, 0);
    const { location, match } = this.props;
    document.getElementsByTagName('nav')[0].style.display = 'none';

    if (location.state === undefined) {
      this.props.requestPost(match.params.slug);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.onRouteChanged();
    }
  }

  componentWillUnmount() {
    document.getElementsByTagName('nav')[0].style.display = '';
  }

  onRouteChanged = () => {
    const { match } = this.props;
    this.props.requestPost(match.params.slug);
  }

  scrollToComment = () => {
    window.scroll({
      top: document.body.scrollHeight + 200,
      left: 0,
      behavior: 'smooth',
    });
  }

  fetchArticle = () => {
    const {
      posts,
      isLoading,
      hasError,
    } = this.props.postReducer;

    const { notify } = this.props.commentsReducer;

    if (isLoading) {
      return <LoadingPlaceholder />;
    }

    if (hasError) {
      return <ErrorPage />;
    }

    return posts !== null && (
      <React.Fragment>
        {posts[0].type === 'talk' ? <Redirect push to={`/talk/${posts[0].slug}`} /> : null}
        {posts[0].type === 'page' ? this.renderPage(posts) : this.renderPost(posts, notify)}
      </React.Fragment>
    );
  }

  /**
   * Parse the current title based on props.
   *
   * @param  {object} props
   * @return {string}
   */
  parseTitle = (props) => {
    const { seo, title } = props;
    return (seo && seo.title) ? parseTitleFromHelmet(seo.title) : parseTitleFromHelmet(title);
  }

  renderPost = (posts, notify) => (
    <React.Fragment>
      { this.renderHelmet(posts[0]) }
      <PostHeader {...posts[0]} />
      <ShareIcons {...posts[0]} />
      <FeaturedImage {...posts[0]} />
      <Content {...posts[0]} />
      <LazyLoad height={200} offset={10}>
        <AuthorBox {...posts[0]} />
      </LazyLoad>
      <LazyLoad height={200} offset={10}>
        <CommentsContainer {...posts[0]} />
      </LazyLoad>
      <LazyLoad height={200} offset={10}>
        <PopularArticleContainer {...posts[0]} />
      </LazyLoad>
      <ArticleFooter {...posts[0]} />
      <Error message={notify} />
      <NotifyInfo message={notify} />
    </React.Fragment>
  )

  renderPage = () =>
    (
      <React.Fragment>
        <ErrorPage />;
      </React.Fragment>
    );

  renderArticle = () => {
    const { state } = this.props.location;
    return (
      <React.Fragment>
        { this.renderHelmet(state) }
        <PostHeader {...state} />
        <ShareIcons {...state} />
        <FeaturedImage {...state} />
        <Content {...state} />
        <LazyLoad height={200} offset={10}>
          <AuthorBox {...state} />
        </LazyLoad>
        <LazyLoad height={200} offset={10}>
          <CommentsContainer {...state} />
        </LazyLoad>
        <LazyLoad height={200} offset={10}>
          <PopularArticleContainer {...state} />
        </LazyLoad>
        <ArticleFooter {...state} />
        <Error />
      </React.Fragment>
    );
  }

  renderHelmet = (post) => {
    // const { posts } = this.props;
    // let firstPost = '';
    // if (posts.length > 0) {
    //   firstPost = { ...posts[0] };
    // }

    const firstPost = { ...post };

    if (!firstPost) {
      return '';
    }

    const {
      author,
      date,
      modified,
      seo,
      link,
      tags,
      categories,
      type,
      gaType,
    } = firstPost;

    const canonicalLink = stripTrailingSlash(link);

    const helmetMeta = [
      { name: 'description', content: seo.description },
      { property: 'og:type', content: 'article' },
      { property: 'og:title', content: this.parseTitle(firstPost) },
      { property: 'og:url', content: canonicalLink },
      { property: 'og:description', content: seo.description },
      { property: 'article:publisher', content: 'https://www.facebook.com/techinasiaID' },
      { property: 'article:published_time', content: date },
      { property: 'article:modified_time', content: modified },
      { name: 'twitter:card', content: 'summary' },
      { name: 'twitter:title', content: this.parseTitle(firstPost) },
      { name: 'twitter:url', content: canonicalLink },
      { name: 'twitter:description', content: seo.description },
      { name: 'prerender-status-code', content: '200' },
      { property: 'article:parent_category', content: gaType },
      { property: 'article:section', content: gaType },
    ];

    if (author.displayName) {
      helmetMeta.push({ property: 'article:author_display_name', content: author.displayName });
    }
    if (author.authorUrl) {
      helmetMeta.push({ property: 'article:author_url', content: author.authorUrl });
    }
    if (author.idTechinasia) {
      helmetMeta.push({ property: 'article:author_id', content: author.idTechinasia });
    }
    if (seo.image) {
      helmetMeta.push({ name: 'twitter:image', property: 'og:image', content: seo.image });
      // helmetMeta.push({ name: 'twitter:image', content: seo.image });
    }
    if (author.twitter) {
      const twitterHandle = author.twitter.substr(author.twitter.lastIndexOf('/') + 1);
      helmetMeta.push({ name: 'twitter:creator', content: `@${twitterHandle}` });
    }
    if (author.facebook) {
      const facebookHandle = author.facebook.substr(author.facebook.lastIndexOf('/') + 1);
      helmetMeta.push({ property: 'article:author', content: `https://www.facebook.com/${facebookHandle}` });
    }
    if (categories && categories.length > 0) {
      /* Because Facebook doesn't support multiple article:section, we take the first one for now.
          categories.forEach(function(category) {
          helmetMeta.push({ property: 'article:section', content: category.name });
          });
      */

      categories.forEach((category) => {
        helmetMeta.push({ property: 'article:category', content: category.name });
      });
    }

    if (tags && tags.length > 0) {
      tags.forEach((tag) => {
        helmetMeta.push({ property: 'article:tag', content: tag.name });
      });
    }

    const helmetLink = [{ rel: 'canonical', href: canonicalLink }];

    /* add meta amp */
    if (type !== 'page') {
      helmetLink.push({ rel: 'amphtml', href: `${canonicalLink}/amp/` });
    }

    if (author.google) {
      helmetLink.push({ rel: 'author', href: author.google });
    }

    if (this.props.location.pathname === '/category/games/android') {
      helmetMeta.push({ name: 'robots', content: 'follow, noindex' });
    }

    return (
      <Helmet
        title={this.parseTitle(firstPost)}
        titleTemplate="%s"
        meta={helmetMeta}
        link={helmetLink}
      />
    );
  }

  render() {
    const { location, postReducer } = this.props;

    if (location.hash.length > 0 && postReducer.isLoaded) {
      setTimeout(this.scrollToComment, 0);
    }

    return location.state === undefined
      ? this.fetchArticle()
      : this.renderArticle();
    // return this.fetchArticle();
  }
}

const mapStateToProps = ({ postReducer, commentsReducer }) => ({
  postReducer,
  commentsReducer,
});

export default connect(mapStateToProps, { requestPost })(Article);
