import CONSTANT from './constant';
import { startLoading, finishLoading, errorLoading } from '../../utils/reducerUtils';

const initialState = {
  posts: null,
};

const postReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANT.REQUEST_POST: {
      return startLoading(state);
    }
    case CONSTANT.REQUEST_POST_ERROR: {
      return errorLoading(state, action);
    }
    case CONSTANT.REQUEST_POST_SUCCESS: {
      /* track(state.currentPage > 1 ? 'Scroll' : 'Load', {
        category: 'Home Post List',
        value: state.currentPage,
        nonInteraction: state.currentPage <= 1
      }); */
      const finish = finishLoading(state);
      return {
        ...finish,
        ...action.payload,
      };
    }

    case CONSTANT.REQUEST_IS_ON_ARTICLE_PAGE: {
      return {
        isOnArticlePage: state.isOnArticlePage,
      };
    }
    default: {
      return state;
    }
  }
};

export { postReducer };
