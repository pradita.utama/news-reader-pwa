import React from 'react';
import PropTypes from 'prop-types';
import { Item, StyledModal, FlexContainer, FlexWrapper, Search } from './style';
import CloseButton from '../../commons/ui-kit/Button/IconButton/CloseButton';
import SearchButton from '../../commons/ui-kit/Button/IconButton/SearchButton';


class Component extends React.Component {
  static propTypes = {
    open: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    history: PropTypes.object,
  }
  state = {
    query: '',
  }

  handleQuery = (e) => {
    this.setState({
      query: e.target.value,
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { query } = this.state;
    const { handleClose, history } = this.props;

    if(!query.length){
      return;
    }
    handleClose();
    history.push({
      pathname: `/search/${query}`,
    });

    this.setState({
      query: '',
    });
  }

  render() {
    const { handleQuery, handleSubmit } = this;
    const { query } = this.state;
    const { open, handleClose } = this.props;
    return (
      <div>
        <StyledModal
          isOpen={open}
          onRequestClose={handleClose}
          closeTimeoutMS={250}
        >
          <form onSubmit={handleSubmit}>
            <Item>
              <FlexContainer>
                <FlexWrapper>
                  <CloseButton handleClose={handleClose} />
                </FlexWrapper >

                <FlexWrapper wide>
                  <Search autoFocus type="text" placeholder="Cari artikel dan startup terbaru" defaultValue="" value={query} onChange={handleQuery} />
                </FlexWrapper >
                <FlexWrapper>
                  <SearchButton handleSubmit={handleSubmit} modalOpened={open} />
                </FlexWrapper >

              </FlexContainer>
            </Item>
          </form>
        </StyledModal>
      </div>
    );
  }
}

Component.defaultProps = {
  history: null,
};


export default Component;
