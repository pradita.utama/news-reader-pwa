import styled from 'styled-components';
import ModalAdapter from './ModalAdapter';


const Item = styled.div`
  width: auto;
  height: auto;
  background-color: #ffffff;
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.16);
  outline: none;
`;

const Search = styled.input`
  width: 100%;
  border: 0;
  outline: none;
  &::placeholder{
    opacity: 0.5;
  }
`;

const FlexContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const FlexWrapper = styled.div`
  display: flex;
  flex: ${props => (props.wide ? '5' : '1')};
  padding: 7px;
`;
const StyledModal = styled(ModalAdapter)`

  &__Overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 100;
    background-color: rgba(51, 51, 51, 0.5);
    opacity: 0;
  }

  &__Overlay--after-open {

      opacity: 1;
      transition: opacity 0.2s ease-in; 
  }

  &__Overlay--before-close {
    opacity: 0;
    transition: opacity 0.2s ease-in;
  }

  &__Content {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: auto;
    background-color: white;
  }
`;


export {
  StyledModal,
  Item,
  FlexWrapper,
  FlexContainer,
  Search,
};
