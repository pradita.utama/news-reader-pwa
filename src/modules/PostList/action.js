import { getPostList, getCategoryPostList, getPopularPost } from './api';
import CONSTANT from './constant';

const requestPostList = (page, perPage) => ({
  type: CONSTANT.REQUEST_POSTLIST,
  payload: {
    promise: getPostList(page, perPage),
  },
});

const requestCategoryPostList = (slug, page, perPage) => ({
  type: CONSTANT.REQUEST_CATEGORY_POSTLIST,
  payload: {
    promise: getCategoryPostList(slug, page, perPage),
    category: slug,
  },
});

const requestPopularPost = () => ({
  type: CONSTANT.REQUEST_POPULAR_POST,
  payload: {
    promise: getPopularPost(),
  },
});

const clearCategoryPostList = () => ({
  type: CONSTANT.CLEAR_CATEGORY_POSTLIST,
});

const clearPostList = () => ({
  type: CONSTANT.CLEAR_POST_LIST,
});

export {
  requestPostList,
  requestCategoryPostList,
  clearCategoryPostList,
  requestPopularPost,
  clearPostList,
};
