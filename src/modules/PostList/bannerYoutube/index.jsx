import React from 'react';

import { Container, Header, SubscribeButton, ButtonWrapper } from './style';
import YouTubeSection from './YoutubeSection';
import YoutubeButton from './YoutubeButton';
import ButtonWithLink from '../../../commons/ui-kit/Button/BaseButton';

const YouTube = () => (
  <Container>
    <Header>
      <h2> Temukan video startup terupdate di channel Youtube Tech in Asia </h2>
    </Header>
    <YouTubeSection />
    <ButtonWrapper>
      <SubscribeButton>
        <YoutubeButton channel="UCZm5DYIbijZkjgbR3uZyF1Q" />
      </SubscribeButton>
      <ButtonWithLink
        outlinePrimary
        to="https://www.youtube.com/channel/UCZm5DYIbijZkjgbR3uZyF1Q"
        label="Lihat Video Lainnya"
      />
    </ButtonWrapper>
  </Container>
);

export default YouTube;
