import styled from 'styled-components';

const Container = styled.div`
  width: 100%;
  padding: 10px 17px 0;
  margin-bottom: 10px;
  background-color: #ffffff;
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.16);
`;

const Header = styled.div`
  width: 282px;
  height: 69px;
  margin-bottom: 12px;
  > h2 {
    margin: 10px 0 0;
    font-size: 21px;
    font-weight: 600;
    line-height: 1.1;
    text-align: left;
    color: #333333;
  }
`;

const ResizableIframe = styled.div`
  max-width: 392.4px;

  > div {
    position: relative;
    padding-bottom: 75%;
    height: 0;
  }
`;

const StyledIframe = styled.iframe`
  position: absolute;
  top: 0px;
  left: 0px;
  width: 100%;
  height: 100%;
  border-radius: 8px;
  border: none;
`;

const SubscribeButton = styled.div`
  margin: 7px 0 0 5px;
`;

const ButtonWrapper = styled.div`
  display: flex;
  margin: 5px 0 0 3px;
  justify-content: space-between;
  padding-bottom: 5px;
`;

export { Container, ResizableIframe, StyledIframe, Header, SubscribeButton, ButtonWrapper };
