import React from 'react';

import { ResizableIframe, StyledIframe } from './style';


const YoutubeSection = () => (
  <ResizableIframe>
    <div>
      <StyledIframe
        src="https://www.youtube.com/embed/o1ugNnMyeZc?autoplay=0&rel=0&modestbranding=1"
        allow="autoplay; encrypted-media"
        allowfullscreen
      />
    </div>
  </ResizableIframe>
);

export default YoutubeSection;
