import React from 'react';
import PropTypes from 'prop-types';

class YouTubeButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = ({ initialized: false });
  }

  componentDidMount() {
    if (this.state.initialized) {
      return;
    }

    const button = this.node;
    const script = document.createElement('script');
    script.src = 'https://apis.google.com/js/platform.js';
    button.parentNode.appendChild(script);

    this.initialized();
  }

  initialized() {
    this.setState({ initialized: true });
  }

  render() {
    /* eslint-disable */
    return (
      <div
        ref={node => (this.node = node)}
        className="g-ytsubscribe"
        data-channelid={this.props.channel}
        data-layout="default"
        data-theme="default"
        data-count="default"
      />
    );
    /* eslint-enable */
  }
}

YouTubeButton.propTypes = {
  channel: PropTypes.string.isRequired,
};

export default YouTubeButton;
