import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Loading from 'commons/loading/LoadingPreviewArticle';
import { requestPostList } from './action';
import CommunityHub from './communityHub';
import ErrorFeed from '../../commons/errors/ErrorFeed';
import PostTemplate from './components/PostTemplate';
import MenuHub from './menuHub';
import MenuHubApi from './menuHub/menuHubApi';
import InfiniteScroll from '../../commons/infiniteScroll';
import BannerYoutube from './bannerYoutube';
import CommunityBanner from './communityBanner';

class PostListSnipet extends Component {
  static propTypes = {
    posts: PropTypes.arrayOf(PropTypes.object).isRequired,
    isLoading: PropTypes.bool,
    hasError: PropTypes.bool,
    isHome: PropTypes.bool,
    error: PropTypes.object,
    requestPostList: PropTypes.func.isRequired,
    currentPage: PropTypes.number,
    perPage: PropTypes.number,
  };

  static defaultProps = {
    error: {},
  };

  static defaultProps = {
    currentPage: 1,
    perPage: 20,
    isLoading: true,
    hasError: false,
  };

  static defaultProps = {
    isHome: false,
  };

  loadMorePosts = () => {
    const { currentPage, perPage } = this.props;

    if (this.props.isLoading) {
      return;
    }

    this.props.requestPostList(currentPage + 1, perPage);
  }

  handleClick = () => {
    const { currentPage, perPage } = this.props;
    this.props.requestPostList(currentPage, perPage);
  }

  uniqueID = () => Math.random().toString(36).substr(2, 9);

  renderCommunityHub = () => {
    const { posts, isHome } = this.props;
    let array = [];
    if (isHome) {
      posts.map((post, idx) => {
        if (idx < 10) array.push(<PostTemplate key={post.slug} {...post} />);
        else if (idx === 10) {
          array = [
            ...array,
            <CommunityHub key={this.uniqueID()} />,
            <PostTemplate key={this.uniqueID()} {...post} />,
          ];
        } else if (idx === 15) {
          array = [
            ...array,
            <MenuHub data={MenuHubApi[0]} key={post.title} />,
            <PostTemplate key={this.uniqueID()} {...post} />,
          ];
        } else if (idx === 19) {
          array = [
            ...array,
            <PostTemplate key={this.uniqueID()} {...post} />,
            <MenuHub data={MenuHubApi[1]} key={post.title} />,
          ];
        } else if (idx === 24) {
          array = [
            ...array,
            <PostTemplate key={this.uniqueID()} {...post} />,
            <BannerYoutube key={post.title} />,
          ];
        } else if (idx === 29) {
          array = [
            ...array,
            <PostTemplate key={this.uniqueID()} {...post} />,
            <CommunityBanner key={this.uniqueID()} />,
          ];
        } else {
          array = [...array, <PostTemplate key={this.uniqueID()} {...post} />];
        }
        return array;
      });
      return array;
    }
    return posts.map(post => <PostTemplate key={this.uniqueID()} {...post} />);
  }

  render() {
    const { hasError, isLoading } = this.props;

    if (hasError) {
      return (<ErrorFeed
        onClick={this.handleClick}
      />);
    }
    return (
      <React.Fragment>
        <InfiniteScroll onLoadMore={this.loadMorePosts} threshold={150 * 10}>
          {this.renderCommunityHub()}
        </InfiniteScroll>
        {isLoading ? <Loading /> : null}
      </React.Fragment>
    );
  }
}

export default connect(null, { requestPostList })(PostListSnipet);
