import CONSTANT from './constant';
import {
  startLoading,
  finishLoading,
  errorLoading,
  appendLoadingStates,
  combinePopularPosts,
} from '../../utils/reducerUtils';

const initialState = appendLoadingStates({
  posts: [],
  popular: [],
});

const postListReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANT.REQUEST_POSTLIST: {
      return startLoading(state);
    }
    case CONSTANT.REQUEST_POSTLIST_ERROR: {
      return errorLoading(state, action);
    }
    case CONSTANT.REQUEST_POSTLIST_SUCCESS: {
      const finish = finishLoading(state, action.payload);
      const posts = [...state.posts, ...action.payload.posts];
      return {
        ...finish,
        ...action.payload,
        posts,
      };
    }
    case CONSTANT.CLEAR_POST_LIST: {
      return {
        posts: [],
      };
    }
    default: {
      return state;
    }
  }
};

const categoryPostListReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANT.REQUEST_CATEGORY_POSTLIST: {
      return startLoading(state);
    }
    case CONSTANT.REQUEST_CATEGORY_POSTLIST_ERROR: {
      return errorLoading(state, action);
    }
    case CONSTANT.REQUEST_CATEGORY_POSTLIST_SUCCESS: {
      const finish = finishLoading(state);
      const posts = [...state.posts, ...action.payload.posts];
      return {
        ...finish,
        ...action.payload,
        posts,
      };
    }
    case CONSTANT.CLEAR_CATEGORY_POSTLIST: {
      return {
        posts: [],
      };
    }

    case CONSTANT.REQUEST_POPULAR_POST: {
      return startLoading(state);
    }
    case CONSTANT.REQUEST_POPULAR_POST_ERROR: {
      return errorLoading(state, action);
    }
    case CONSTANT.REQUEST_POPULAR_POST_SUCCESS: {
      const finish = finishLoading(state);
      const popularCombine = combinePopularPosts(action.payload);
      return {
        ...finish,
        popularCombine,
      };
    }
    default: {
      return state;
    }
  }
};

export { postListReducer, categoryPostListReducer };
