import styled from 'styled-components';
import { Link } from 'react-router-dom';

const StyledLink = styled(Link)`
  display: block;
`;

const Post = styled.article`
  padding: 15px;
  padding-bottom: 10px;
  padding-bottom: 0.1em;
`;

const Title = styled.h6`
  width: calc(100% - 100px);
  float: left;
  margin: 0 0 8px;
  font-size: 1em;
  padding-right: 10px;
  color: #000;
  line-height: 18px;
`;

const Image = styled.img`
  width: 3.5em;
  margin-left: 2.7em;
  border-radius: 0.2em;
  float: left;
  font-size: 16px;
  height: 56px;
`;

const Categories = styled.a`
  font-family: Proxima;
  font-style: normal;
  padding: 0;
  margin: 2px 0 12px 0;
  display: block;
  font-size: 0.8em;
  text-transform: uppercase;
  line-height: 0.7em;
  font-weight: 500;
`;

const Time = styled.time`
  line-height: 1.2;
  font-size: 11px;
  display: flex;
  color: #555;
  clear: both;
  border-bottom: 0.1em solid rgba(0, 0, 0, 0.045);
  padding: 0 0 1em;
  p {
    margin: 0 12px 0 0;
    color: rgba(71,71,71,5);
  }
  li {
    margin: 0;
  }
`;

const Contain = styled.div`
  border-top: solid rgba(0,0,0,0.16) 1px 
`;

const TabContainer = styled.div`
  display: block;
  height: 48px;
  background: white;
  padding: 1px;
  width: 100%;

  .sticky {
    position: fixed;
    display: block;
    height: 44px;
    background: white;
    padding: 1px;
    width: 100%;
    z-index: 50;
    top: 0;
    left: 0;
    border-top: 0.1em solid rgba(0, 0, 0, 0.045);
    transition: all 0.2s;
    transform: translateY(0%);
  }

  .sub-sticky {
    position: fixed;
    display: block;
    height: 44px;
    background: white;
    padding: 1px;
    width: 100%;
    z-index: 1000;
    top: 0;
    left: 0;
    border-top: 0.1em solid rgba(0, 0, 0, 0.045);
    transition: all 0.5s;
  }

  .sub-sticky-scroll {
    transition: all 1s;
    transform: translateY(44px);
  }


  .normal {
    position: inherit;
    display: block;
    height: 44px;
    background: white;
    padding: 1px;
    width: 100%;
    z-index: 1000;
    top: 0;
    left: 0;
    border-top: 0.1em solid rgba(0, 0, 0, 0.045);
  }

`;

const SubTabContainer = styled.div`
  box-shadow: 0 0 0.2em 0 rgba(0, 0, 0, 0.16);
`;

const TabTitle = styled.div`
  display: flex;
  justify-content: space-around;
  -webkit-box-orient: horizontal;
  flex-direction: row;
  border-bottom: solid rgba(0, 0, 0, 0.16) 1px;
  background: white;
`;

const TabActive = styled.div`
  padding: 0 0.1em;
  color: #bf0016;
  line-height: 2.9em;
  font-size: 0.9em;
  font-weight: bold;
  color: e9203d;
  font-family: Proxima;
  text-transform: uppercase;
  border-bottom: solid #bf0016 0.15em;
`;

const TabItem = styled.div`
  padding: 0 0.1em;
  line-height: 2.9em;
  font-size: 0.9em;
  font-family: Proxima;
`;

const TabConActive = styled.div`
  display: block;
`;

const TabCon = styled.div`
  display: none;
`;

const SubTabActive = styled.div`
  padding: 0 0.1em;
  color: #bf0016;
  line-height: 2.9em;
  font-size: 0.9em;
  font-family: Proxima;
  border-bottom: solid #bf0016 0.1em;
`;

const Container = styled.div`
  margin-bottom: 60px;
`;

export {
  StyledLink,
  Post,
  Title,
  Image,
  Categories,
  Time,
  TabContainer,
  TabTitle,
  TabActive,
  TabItem,
  TabConActive,
  TabCon,
  SubTabActive,
  Container,
  SubTabContainer,
  Contain,
};
