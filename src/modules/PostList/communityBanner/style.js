import styled from 'styled-components';

const linkTo = props => (props.to);

const Container = styled.div`
  width: 100%;
  padding: 10px 10px 10px;
  margin-bottom: 10px;
  background-color: #ffffff;
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.16);
  h5 {
    margin: 10px 0 0;
    height: 74px;
    font-weight: 600;
    line-height: 1.32;
    color: #333333;
  }
  p {
    margin: 0;
    line-height: 1.31;
    height: 42px;
  }
`;

const ImageWrapper = styled.div`
  padding: 1em 1em 0.9em 0;
  border-bottom: 0.04em solid rgba(0, 0, 0, 0.045);
`;

const Image = styled.img`
  width: 158px;
  border-radius: 0.2em;
  font-size: 16px;
`;

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

const ButtonLink = styled.a.attrs({
  href: linkTo,
})`
  background: #bf0016;
  color: white;
  font-size: x-large;
  padding: 1px 0.2em 5px 0.2em;
  border-radius: 4px;
  border: solid 1px #eaeaea;
  margin: 2px 0 0 10px;
  line-height: 1.25;
  display: inline-block;
  span {
    font-size: small;
  }
`;

export { Container, ImageWrapper, Image, ButtonWrapper, ButtonLink };
