import React from 'react';

import { Container, ImageWrapper, Image, ButtonWrapper, ButtonLink } from './style';
import BaseButton from '../../../commons/ui-kit/Button/BaseButton';

const BannerPostSnippet = () => (
  <Container>
    <ImageWrapper>
      <Image
        src="https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2018/04/tia-community.png"
        alt="images"
      />
    </ImageWrapper>
    <h5>Mulai Menulis di Tech in Asia ID </h5>
    <p>Kamu bisa menulis artikel seputar startup, teknologi dan dunia profesional</p>
    <ButtonWrapper>
      <BaseButton to="/community-manual" label="Panduan Menulis di TIA ID" />
      <ButtonLink to="/create/article"> <span> Tulis artikel </span>  </ButtonLink>
    </ButtonWrapper>
  </Container>
);

export default BannerPostSnippet;
