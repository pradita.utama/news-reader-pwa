import React from 'react';

import Populer from './components/Populer';
import TabController from './components/TabController';
import Terbaru from './components/Terbaru';

const Container = () => {
  const tabs = [
    {
      text: 'TERBARU',
      component: <Terbaru />,
    },
    {
      text: 'POPULER',
      component: <Populer />,
    },
  ];
  return (
    <TabController isSubMenu={false}>
      {tabs.map(item => <React.Fragment key={item.text}>{item.component}</React.Fragment>)}
    </TabController>
  );
};

export default Container;
