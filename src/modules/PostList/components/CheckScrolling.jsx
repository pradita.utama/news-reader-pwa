import React, { Component } from 'react';
import debounce from 'lodash/debounce';

class CheckScrolling extends Component {
  constructor(props) {
    super(props);

    this.checkScrollListener = debounce(this.checkScrollListener.bind(this), 200);
    this.scrollListener = this.scrollListener.bind(this);
  }

  componentDidMount() {
    this.logScrollDirection();
  }

  componentWillUnmount() {
    this.detachScrollListener();
  }

  detachScrollListener() {
    window.removeEventListener('scroll', this.checkScrollListener);
    window.removeEventListener('resize', this.checkScrollListener);
    this.checkScrollListener.cancel();
  }

  checkScrollListener() {
    this.checkScrollListener.cancel();
    this.scrollListener();
  }
  /* eslint-disable */
  scrollListener() {
    const { body } = document;
    const html = document.documentElement;
    const bodyH = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      body.getBoundingClientRect().height,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight,
    );
    const element = document.getElementById('tab-container');
    // console.log(element.offsetTop);
    const bodyRect = document.body.getBoundingClientRect();
    const header = element.offsetTop;

    // const elemRect = element.getBoundingClientRect();
    // const offset = elemRect.top - bodyRect.top;

    // console.log(bodyRect.top, header);

    if (header <= 0) {
      this.logScrollDirection();
    } else {
      this.clearScrollDirection();
    }
  }

  logScrollDirection = () => {
    let previous = window.scrollY;
    window.addEventListener('scroll', () => {
      window.scrollY < previous ? this.changeClassSticky('up') : this.changeClassSticky('down');
      previous = window.scrollY;
    });
  };

  changeClassSticky = direction => {
    const header = document.getElementById('tab-container');
    const subHeader = document.getElementById('sub-tab-container');
    if(subHeader === null) {
      return;
    } else {
      const headerPos = header.offsetTop;
      const subHeaderPos = subHeader.offsetTop;
      if (direction === 'up') {
        if (subHeaderPos <= 0) {
          subHeader.classList.add('sub-sticky-scroll');
          header.classList.add('sticky');
        }
      } else {
        if(subHeaderPos <= 0) {
          subHeader.classList.remove('sub-sticky-scroll')
        }
        subHeader.classList.remove('sub-sticky-scroll');
      }
    }
  };

  render() {
    return <div />;
  }
}

export default CheckScrolling;
