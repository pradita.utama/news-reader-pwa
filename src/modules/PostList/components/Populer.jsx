/* eslint-disable */
import React from 'react';

import Semua from './Semua';
import Component from '../Component';
import Startup from './Startup';
import Profesional from './Profesional';
import Teknologi from './Teknologi';
import { ContainerStyle } from '../style';
import TabController from './TabController';

class Populer extends React.Component {
  render() {
    const { postListReducer } = this.props;
    const tabs = [
      // {
      //   text: 'Semua',
      //   component: <Semua />,
      // },
      {
        text: 'Startup',
        component: <Startup />,
      },
      {
        text: 'Profesional',
        component: <Profesional />,
      },
      {
        text: 'Teknologi',
        component: <Teknologi />,
      },
    ];
    return (
      <TabController isSubMenu>
        {tabs.map(item => <React.Fragment key={item.text}>{item.component}</React.Fragment>)}
      </TabController>
    );
  }
}
export default Populer;
