import React from 'react';
import ErrorFeed from 'commons/errors/ErrorFeed';

const Error = () => <ErrorFeed />;

export default Error;
