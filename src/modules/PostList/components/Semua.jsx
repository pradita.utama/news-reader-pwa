import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Loading from 'commons/loading/LoadingPreviewArticle';
import { requestPopularPost, clearCategoryPostList } from '../action';
import PostTemplate from './PostTemplate';

class Startup extends React.Component {
  static propTypes = {
    categoryPostListReducer: PropTypes.shape({
      isLoading: PropTypes.bool.isRequired,
    }),
    requestPopularPost: PropTypes.func.isRequired,
    clearCategoryPostList: PropTypes.func.isRequired,
  };

  static defaultProps = {
    categoryPostListReducer: {},
  };

  componentDidMount() {
    this.props.requestPopularPost();
  }

  render() {
    const { categoryPostListReducer } = this.props;
    console.log(categoryPostListReducer);

    if (categoryPostListReducer.isLoading) return <Loading />;

    return (
      <React.Fragment>
        {categoryPostListReducer.popular.map(post => (
          <PostTemplate isPopular {...post} key={post.id} />
        ))}
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ categoryPostListReducer }) => ({
  categoryPostListReducer,
});

export default connect(mapStateToProps, {
  requestPopularPost,
  clearCategoryPostList,
})(Startup);
