import styled from 'styled-components';

const Container = styled.div`
  display: block;
  position: relative;
  width: 100%;
  border: solid transparent;
`;

export { Container };
