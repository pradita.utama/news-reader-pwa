import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import InfiniteScroll from 'commons/infiniteScroll/';
import Loading from 'commons/loading/LoadingPreviewArticle';
import { requestCategoryPostList, clearCategoryPostList } from '../action';
import Component from '../Component';

class Teknologi extends React.Component {
  static propTypes = {
    categoryPostListReducer: PropTypes.shape({
      posts: PropTypes.arrayOf(PropTypes.object),
      currentPage: PropTypes.number,
      perPage: PropTypes.number,
      isLoading: PropTypes.bool,
    }),
    requestCategoryPostList: PropTypes.func.isRequired,
    clearCategoryPostList: PropTypes.func.isRequired,
  };

  static defaultProps = {
    categoryPostListReducer: {},
  };

  componentDidMount() {
    this.props.requestCategoryPostList('teknologi', 1, 15);
  }

  componentWillUnmount() {
    this.props.clearCategoryPostList();
  }

  loadMorePosts = () => {
    const { currentPage, perPage, isLoading } = this.props.categoryPostListReducer;

    if (isLoading) {
      return;
    }
    this.props.requestCategoryPostList('profesional', currentPage + 1, perPage);
  };

  render() {
    const { categoryPostListReducer } = this.props;
    return (
      <React.Fragment>
        <InfiniteScroll onLoadMore={this.loadMorePosts} threshold={150 * 10}>
          <Component {...categoryPostListReducer} />
        </InfiniteScroll>
        { categoryPostListReducer.isLoading ? <Loading /> : null }
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ categoryPostListReducer }) => ({
  categoryPostListReducer,
});

export default connect(mapStateToProps, {
  requestCategoryPostList,
  clearCategoryPostList,
})(Teknologi);
