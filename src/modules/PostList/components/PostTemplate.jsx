import React, { Component } from 'react';
import LazyLoad from 'react-lazyload';
import { Link } from 'react-router-dom';

import { diffForHumans } from 'utils/dateUtils';
import { getParentCategoryIndex } from 'utils/postUtils';
import { Post, Title, Image, Categories, Time } from '../style';

class PostTemplate extends Component {

  showCategory = (props) => {
    const parentCategoryIndex = getParentCategoryIndex(props);
    const arr = props.categories || props.category;
    
    if(arr.length) {
      return (
        <Categories> {arr[parentCategoryIndex].name} </Categories>
      )
    }
  };

  /* eslint-disable */
  showTime = (props) => {
    const { slug, id } = props;
    if(props.commentsCount > 0) {
      return (
        <React.Fragment>
          <Time>
            <p> {diffForHumans(props.date)} </p>
            <p> &middot; </p>
            <Link 
              to={`/${slug}#comments-${id}`} 
              >
                <p> {props.commentsCount} Komentar </p>
            </Link>
          </Time>
        </React.Fragment>
      );
    }
    return (
      <Time>
        <p> {diffForHumans(props.date)} </p>
      </Time>
    );
  };

  render() {
    const props = this.props;
    const { id, slug, title, featuredImage, isPopular, isSearch } = props
    if (isPopular) {
      return (
      <React.Fragment>
        <LazyLoad height={150} offset={10}>
          <Post>
            <Link
              key={ id }
              to={{
                pathname: `/${ slug }`,
                state: { ...props }
              }}
            >
              <Title
                dangerouslySetInnerHTML={{
                  __html: title
                }}
              />
            <Image
              src={ featuredImage.attachmentMeta.sizes.thumbnail.url }
              alt="featured"
              onError={(e)=>{e.target.src="/static/images/fallback-image.svg"}}
            />
            {/* {this.showTime(props)} */}
          </Link>
          </Post>
        </LazyLoad>
      </React.Fragment>
      )
    }
    if(isSearch) {
      return (
        <React.Fragment>
        <LazyLoad height={120} offset={10}>
        <Post>
          {this.showCategory(props)}
          <Link
            key={ id }
            to={{
              pathname: `/${ slug }`,
              state: { ...props }
            }}
          >
            <Title
              dangerouslySetInnerHTML={{
                __html: title
              }}
            />
            <Image
              src={ featuredImage.attachmentMeta.sizes.thumbnail.url }
              alt="featured"
              onError={(e)=>{e.target.src="/static/images/fallback-image.svg"}}
            />
            {/* {this.showTime(props)} */}
          </Link>
          {this.showTime(props)}
          </Post>
        </LazyLoad>
      </ React.Fragment>
      )
    }
     
     return (
      <React.Fragment>
        <LazyLoad height={150} offset={10}>
        <Post>
          {this.showCategory(props)}
          <Link
            key={ id }
            to={{
              pathname: `/${ slug }`,
              state: { ...props }
            }}
          >
            <Title
              dangerouslySetInnerHTML={{
                __html: title
              }}
            />
            <Image
              src={ featuredImage.attachmentMeta.sizes.thumbnail.url }
              alt="featured"
              onError={(e)=>{e.target.src="/static/images/fallback-image.svg"}}
            />
            {/* {this.showTime(props)} */}
          </Link>
          {this.showTime(props)}
          </Post>
        </LazyLoad>
      </ React.Fragment>
    )
  }
}

export default PostTemplate;
