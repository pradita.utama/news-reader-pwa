import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { requestPostList, clearPostList } from '../action';
import Component from '../Component';

class Terbaru extends React.Component {
  static propTypes = {
    postListReducer: PropTypes.shape({
      posts: PropTypes.arrayOf(PropTypes.object),
      isLoaded: PropTypes.bool,
    }).isRequired,
    clearPostList: PropTypes.func.isRequired,
    requestPostList: PropTypes.func.isRequired,
  }

  componentDidMount() {
    this.props.requestPostList(1, 20);
  }

  componentWillUnmount() {
    this.props.clearPostList();
  }

  render() {
    const { postListReducer } = this.props;
    return (
      <Component isHome {...postListReducer} />
    );
  }
}

const mapStateToProps = ({ postListReducer }) => ({
  postListReducer,
});

export default connect(mapStateToProps, {
  requestPostList,
  clearPostList,
})(Terbaru);
