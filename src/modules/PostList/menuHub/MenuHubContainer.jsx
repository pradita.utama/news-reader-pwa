import React from 'react';
import PropTypes from 'prop-types';

import { Container, Header, Body, Description } from './style';
import MenuHubComponent from './MenuHubComponent';

const MenuHubContainer = ({
  pageTitle, slug, description, menus,
}) => (
  <Container>
    <Header>
      <h1>
        {pageTitle}
        <span>.</span>
      </h1>
      <Description>{description}</Description>
    </Header>
    <Body>
      {menus.map(data => (
        <MenuHubComponent key={data.icon} title={data.title} slug={slug} icon={data.icon} />
        ))}
    </Body>
  </Container>
);

MenuHubContainer.propTypes = {
  pageTitle: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  menus: PropTypes.array.isRequired,
};

export default MenuHubContainer;
