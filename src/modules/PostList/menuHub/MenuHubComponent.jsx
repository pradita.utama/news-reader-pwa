import React from 'react';
// import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { StyledLink, ImagesTitle } from './style';

const MenuHubComponent = ({ title, slug, icon }) => (
  <StyledLink to={`/category/${slug}`}>
    <img src={icon} alt="images" />
    <ImagesTitle>{title}</ImagesTitle>
  </StyledLink>
);

MenuHubComponent.propTypes = {
  title: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
};

export default MenuHubComponent;
