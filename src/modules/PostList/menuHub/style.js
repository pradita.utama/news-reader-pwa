import styled from 'styled-components';
import { Link } from 'react-router-dom';

const Container = styled.div`
  background-color: #fff;
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.16);
  padding: 0.5em 0.2em 0;
  margin: 0.7em 0 0.7em;
`;

const Header = styled.div`
  margin-left: 0.7rem;
  h1 {
    font-size: 30px;
    margin-top: 0;
    margin-bottom: 8px;
    font-weight: 600;
  }
  span {
    color: #ff0000;
  }
`;

const Body = styled.div`
  display: -webkit-box;
  white-space: nowrap;
  overflow-x: auto;
  &::-webkit-scrollbar{
    display: none;
    width: 0;
    background: transparent;
  }
`;

const StyledLink = styled(Link)`
    display: inline-block;
    width: 230px;
    box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 3px 0px;
    margin: 0.5em 8px 28px;
    padding: 2em 1.5em;
    transition: all 0.2s cubic-bezier(0.14, 1.01, 1, 1);

    img{
      width: 50px;
    }
    p{
      color: $gray-base;
      margin-bottom: 0;
      margin-top: 1em;
      font-size: 16px;
      line-height: 1.5;
      font-weight: 600;
      color: #333333;
    }

    &:first-child{
      margin-left: 0.7em;
    }

    &:last-child{
      margin-right: 1em;
      border-right: solid 4px #bf1007;
    }
`;

const Description = styled.p`
  margin-right: 26px;
  margin-top: 1px;
  font-size: 16px;
  color: rgba(0,0,0.8);
`;

const ImagesTitle = styled.p`
  margin-right: 26px;
  margin-top: 1px;
`;

export { Container, Header, Body, StyledLink, Description, ImagesTitle };
