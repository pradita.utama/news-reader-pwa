import React from 'react';
import PropTypes from 'prop-types';

import MenuHubContainer from './MenuHubContainer';

const MenuHub = ({ data }) => {
  const {
    pageTitle, slug, description, menus,
  } = data;
  return (
    <MenuHubContainer pageTitle={pageTitle} description={description} menus={menus} slug={slug} />
  );
};

MenuHub.propTypes = {
  data: PropTypes.object.isRequired,
};

export default MenuHub;
