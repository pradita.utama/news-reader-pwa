export default [
  {
    pageTitle: 'Profesional',
    slug: 'profesional',
    description:
      'Kumpulan artikel untuk kamu yang tertarik di dunia startup sebagai seorang profesional',
    menus: [
      {
        title: 'Self Management',
        slug: 'self-management',
        icon:
          'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2017/11/icon-self-management.png',
      },
      {
        title: 'Team Management',
        slug: 'team-management',
        icon:
          'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2017/11/icon-team-management.png',
      },
      {
        title: 'Growth Hack',
        slug: 'growth-hack',
        icon:
          'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2017/11/icon-growth-hack.png',
      },
      {
        title: 'Digital Marketing',
        slug: 'digital-marketing',
        icon:
          'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2017/11/icon-digital-marketing.png',
      },
      {
        title: 'Engineering',
        slug: 'engineering',
        icon:
          'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2017/11/icon-engineering.png',
      },
      {
        title: 'Product Development',
        slug: 'product-development',
        icon:
          'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2017/11/icon-product-development.png',
      },
      {
        title: 'Semua Artikel Profesional',
        slug: 'profesional',
        icon:
          'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2017/11/icon-all-article.png',
      },
    ],
  },
  {
    pageTitle: 'Startup',
    slug: 'startup',
    description: 'Kumpulan artikel mengenai perkembangan Startup di Indonesia',
    menus: [
      {
        title: 'Startup Terbaru',
        slug: 'new-startup',
        icon:
          'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2017/11/icon-latest-startup.png',
      },
      {
        title: 'Startup Update',
        slug: 'update',
        icon:
          'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2017/11/icon-startup-update.png',
      },
      {
        title: 'Featured Startup',
        slug: 'featured-startup',
        icon:
          'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2017/11/icon-featured-startup.png',
      },
      {
        title: 'Entrepreneurship',
        slug: 'entrepreneur',
        icon:
          'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2017/11/icon-entrepreneur.png',
      },
      {
        title: 'Investment',
        slug: 'investasi',
        icon:
          'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2017/11/icon-investment.png',
      },
      {
        title: 'Industry Insight',
        slug: 'industry-insights',
        icon:
          'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2017/11/icon-industry-insight.png',
      },
      {
        title: 'Events',
        slug: 'event',
        icon: 'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2017/11/icon-event.png',
      },
      {
        title: 'Semua Artikel Startup',
        slug: 'startups',
        icon:
          'https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2017/11/icon-all-article.png',
      },
    ],
  },
];
