import React from 'react';
import { HubWrapper, HubContainer, ParagraphContent, Image, ImageWrapper } from './style';
import Button from '../../../commons/ui-kit/Button/BaseButton';

const MenuHub = () => {
  const link = 'http://techin.asia/leads-2';
  return (
    <HubWrapper>
      <ImageWrapper>
        <Image
          src="https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2018/04/tia-community.png"
          alt="images"
        />
      </ImageWrapper>
      <HubContainer>
        <h2>
          Jadi yang paling up-to-date di komunitas TIA ID
          <span className="reddot">.</span>
        </h2>
        <ParagraphContent>
          Temukan info <em>Startup</em> terbaru, kiat praktis dan berbagai <em>template</em> bisnis
          lainnya di komunitas. kamu juga bisa ngobrol seru dengan expert dan mentor.
        </ParagraphContent>
        <Button to={link} target="_BLANK" primary label="Subscribe hari ini, yuk!" />
      </HubContainer>
    </HubWrapper>
  );
};

export default MenuHub;
