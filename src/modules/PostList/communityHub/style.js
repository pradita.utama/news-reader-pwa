import styled from 'styled-components';
import { Link } from 'react-router-dom';

// import Button from '../../../commons/ui-kit/Button/BaseButton';

const HubWrapper = styled.div`
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.16);
  padding: 0.5em 0;
`;

const HubContainer = styled.div`
  padding: 0 1em;
  h2 {
    margin-bottom: 10px;
    line-height: 30px;
    font-size: 28px;
    margin: 21px 0 8px;
    font-weight: 600;
  }
`;

const ParagraphContent = styled.p`
  font-size: 16px;
  line-height: 1.3em;
  margin-bottom: 1em;
  color: rgba(0,0,0.8);
`;

const StyledLink = styled(Link)`
  padding: 8px 15px;
`;

const ImageWrapper = styled.div`
  padding: 1em 1em 0.9em 0.9em;;
  border-bottom: 0.04em solid rgba(0,0,0,0.045);
`;

const Image = styled.img`
  width: 158px;
  border-radius: 0.2em;
  font-size: 16px;
`;

const ButtonLink = styled(Link)`
  background: #bf0016;
  color: white;
  font-size: x-large;
  padding: 0 0.8em 4px 0.8em;
  border-radius: 4px;
  border: solid 1px #eaeaea;
  margin: 2px;
  line-height: 1.25;
`;

export { HubWrapper, HubContainer, StyledLink, ParagraphContent, Image, ImageWrapper, ButtonLink };
