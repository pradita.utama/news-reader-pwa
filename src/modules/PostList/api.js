import call from '../../commons/api/RestApi';

const getPostList = (page, perPage) => call(`/posts?page=${page}&perpage=${perPage}`);

const getCategoryPostList = (slug, page, perPage) => call(`/categories/${slug}/posts?page=${page}&per_page=${perPage}`);

const getPopularPost = () => call('/popular?categories=startups,profesional,teknologi');

export {
  getPostList,
  getCategoryPostList,
  getPopularPost,
};
