import { getStickyPost } from './api';
import CONSTANT from './constant';

const requestStickyPost = () => ({
  type: CONSTANT.REQUEST_STICKY_POST,
  payload: {
    promise: getStickyPost(),
  },
});

export default requestStickyPost;

