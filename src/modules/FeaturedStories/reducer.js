import CONSTANT from './constant';
import { startLoading, finishLoading, errorLoading } from '../../utils/reducerUtils';

const initialState = {
  posts: [],
};

const stickyPostReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANT.REQUEST_STICKY_POST: {
      return startLoading(state);
    }
    case CONSTANT.REQUEST_STICKY_POST_ERROR: {
      return errorLoading(state, action);
    }
    case CONSTANT.REQUEST_STICKY_POST_SUCCESS: {
      const finish = finishLoading(state);
      return {
        ...finish,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
};

export default stickyPostReducer;
