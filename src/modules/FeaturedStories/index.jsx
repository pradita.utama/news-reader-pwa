import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import requestStickyPost from './action';
import { Container, Featured, Header } from './style';
import FeaturedStories from './Components/FeaturedStories';

class FeaturedStoriesContainer extends Component {
  static propTypes = {
    stickyPostReducer: PropTypes.object.isRequired,
    requestStickyPost: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.requestStickyPost();
  }

  requestStickyPost = () => {
    this.props.requestStickyPost();
  }

  render() {
    const { stickyPostReducer } = this.props;
    return (
      <Container>
        <Header>
          <Featured> Featured </Featured>
        </Header>
        <FeaturedStories
          onError={this.requestStickyPost}
          {...stickyPostReducer}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ stickyPostReducer }) => ({
  stickyPostReducer,
});

export default connect(mapStateToProps, { requestStickyPost })(FeaturedStoriesContainer);
