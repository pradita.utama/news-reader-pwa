import call from '../../commons/api/RestApi';

const getStickyPost = () => call(`/sticky/posts`);

export {
  getStickyPost,
};
