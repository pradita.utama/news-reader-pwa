import styled from 'styled-components';
import { Link } from 'react-router-dom';

/* eslint-disable-next-line */
const gridColumn = props => (props.color === 1 ? 2 : '' || props.color === 2 ? 2 : '' || props.color === 4 ? 4 : '' || props.color === 5 ? 4 : '');
/* eslint-disable-next-line */
const gridRow = props => (props.color === 1 ? 1 : '' || props.color === 2 ? 2 : '' || props.color === 4 ? 1 : '' || props.color === 5 ? 2 : '');

const Container = styled.div`
  object-fit: contain;
  height: 237px;
  overflow: hidden;
  background-color: #fff;
`;

const Header = styled.div`
  margin-bottom: -11px;
  display: block;
  margin-left: 10px;
`;

const Featured = styled.span`
  font-size: 72px;
  font-weight: 550;
  font-size: 48px;
  line-height: 1;
  color: #cdcdcd;
  letter-spacing: -2px;
`;

const WrapperStyle = styled.div`
  display: grid;
  white-space: nowrap;
  overflow-x: auto;
  grid-template-rows: repeat(2, 100px);
  padding-left: 10px;
  scroll-snap-align: center none;
`;

const StyledLink = styled(Link)`
  display: block;
  width: 300px;
  box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 3px 0px;
  margin: 1px 10px 16px 1px;
  border-radius: 4px;
  grid-column: ${props => (props.color === 0 ? 1 : 3)}
  grid-row: 1 / 3;
  img {
    width: 100%;
    border-radius: 4px;
  }
`;

const StyledLinkWithoutImage = styled(Link)`
  display: block;
  min-width: 300px;
  box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 3px 0px;
  margin: 1px 10px 16px 1px;
  transition: all 0.2s cubic-bezier(0.14, 1.01, 1, 1);
  border-radius: 4px;
  grid-column: ${gridColumn};
  grid-row: ${gridRow}
  margin-right: 1em;
  border-right: solid 1px rgba(0, 0, 0, 0.16);
  background-color: #fff;
`;

const ContainerLink = styled.div`
  position: relative;
  margin: 0 auto;
  padding: 1px;
`;

const Contain = styled.div`
  position: absolute;
  bottom: 0;
  background: white;
  color: #f1f1f1;
  width: 100%;
  height: 50%;
  padding: 5px 0 0;
  margin-left: -1px;
`;

const Title = styled.div`
  white-space: normal;
  line-height: 18px;
  font-size: 16px;
  font-weight: 600;
  max-height: 45px;
  min-width: 300px;
  padding: 0 2px 0 15px;
  color: #333333;
`;

const Categories = styled.div`
  margin: 10px 0 5px 15px;
  font-size: 0.8em;
  text-transform: uppercase;
  line-height: 0.7em;
  color: #bf0a17;
`;

const Title1 = styled.div`
  padding: 0 2px 0 15px;
  color: #333333;
  min-width: 90px;
  max-width: 600px;
  min-height: 45px;
  max-height: 100px;
  word-break: word-wrap;
  overflow: hidden;
  white-space: normal;
  p {
    margin: 0;
    line-height: 18px;
    font-size: 14px;
    font-weight: 600;
  }
`;

const LoadingWrapper = styled.div`
  margin: auto;
  padding: 10px 0 0 0px;
  display: flex;
  justify-content: center;
  img: {
    display: block;
    margin-left: auto;
    margin-right: auto;
  }
`;

export {
  Container,
  Featured,
  StyledLink,
  WrapperStyle,
  Header,
  ContainerLink,
  Contain,
  Title,
  Categories,
  StyledLinkWithoutImage,
  Title1,
  LoadingWrapper,
};
