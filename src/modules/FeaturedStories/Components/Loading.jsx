import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  padding: 15px;
  width: 100%;
  height: 100px;

  .image-placeholder {
    float: right;
    width: 56px;
    height: 56px;
    background-color: #EDEDED;
    border-radius: 0.2em;
  }

  .category-placeholder {
    background-color: #EDEDED;
    width: 100px;
    height: 15px;
    margin-bottom: 10px;
  }

  .title-placeholder {
    float: left;
    background-color: #EDEDED;
    width: 70%;
    height: 40px; 
  }

  @keyframes placeHolderShimmer{
    0%{
        background-position: -468px 0
    }
    100%{
        background-position: 468px 0
    }
  }

  div {
    animation-duration: 1s;
    animation-fill-mode: forwards;
    animation-iteration-count: infinite;
    animation-name: placeHolderShimmer;
    animation-timing-function: linear;
    background: #f6f7f8;
    background: linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
    background-size: 800px 104px;
    position: relative;
  }

`;

const StyledLink = styled.div`
  display: block;
  width: 300px;
  /* min-height: 90px; */
  box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 3px 0px;
  margin: 1px 10px 16px 1px;
  transition: all 0.2s cubic-bezier(0.14, 1.01, 1, 1);
  border-radius: 4px;
  .image-placeholder {
    float: right;
    width: 100%;
    border-radius: 4px;
    background-color: #EDEDED;
    border-radius: 0.2em;
  }
  .category-placeholder {
    background-color: #EDEDED;
    width: 100px;
    height: 15px;
    margin-bottom: 10px;
  }
`;
const ContainerLink = styled.div`
  position: relative;
  margin: 0 auto;
  .title-placeholder {
    width: 100%;
    height: 50px;
    margin: 1px 0 5px;
    border-radius: 4px;
  }
`;

const Loading = () =>
  (
    <React.Fragment>
      <Container>
        <StyledLink>
          <ContainerLink>
            <div className="image-placeholder"> &nbsp; </div>
            <div className="title-placeholder"> &nbsp; </div>
          </ContainerLink>
        </StyledLink>
      </Container>
    </React.Fragment>
  );

export default Loading;
