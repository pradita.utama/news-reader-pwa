import React, { Component } from 'react';
import PropTypes from 'prop-types';

import FeaturedItem from './FeaturedItem';
import { WrapperStyle, LoadingWrapper } from '../style';
import Loading from '../../../images/loader-icon.gif';
import ErrorFeed from '../../../commons/errors/ErrorSticky';

class FeaturedStories extends Component {
  static propTypes = {
    posts: PropTypes.array.isRequired,
    isLoading: PropTypes.bool,
    hasError: PropTypes.bool,
    onError: PropTypes.func.isRequired,
  }

  static defaultProps = {
    isLoading: true,
    hasError: false,
  }

  handleClick = () => {
    this.props.onError();
  }

  render() {
    const { posts, isLoading, hasError } = this.props;
    if (isLoading) {
      return (
        <LoadingWrapper>
          <img src={Loading} alt="loading" />
        </LoadingWrapper>
      );
    }

    if (hasError) {
      return (<ErrorFeed
        onClick={this.handleClick}
      />);
    }

    return (
      <WrapperStyle>
        {posts.map((post, idx) => (
          <React.Fragment key={post.id}>
            <FeaturedItem index={idx} {...post} />
          </React.Fragment>
        ))}
      </WrapperStyle>
    );
  }
}

export default FeaturedStories;
