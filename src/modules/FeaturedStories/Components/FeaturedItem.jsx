import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { getParentCategoryIndex } from '../../../utils/postUtils';
import {
  StyledLink,
  ContainerLink,
  Contain,
  Title,
  Categories,
  StyledLinkWithoutImage,
  Title1,
} from '../style';

class FeaturedItem extends Component {
  showCategory = (props) => {
    const parentCategoryIndex = getParentCategoryIndex(props);
    return <Categories> {props.categories[parentCategoryIndex].name} </Categories>;
  };

  render() {
    const { slug, title, index } = this.props;
    return (
      <React.Fragment>
        {index === 0 || index === 3 ? (
          <StyledLink
            to={{
              pathname: `/${slug}`,
            }}
            color={index}
          >
            <ContainerLink>
              <img src={this.props.featuredImage.attachmentMeta.sizes.medium.url} alt="images" />
              <Contain>
                {this.showCategory(this.props)}
                <Title
                  dangerouslySetInnerHTML={{
                    __html: title,
                  }}
                />
              </Contain>
            </ContainerLink>
          </StyledLink>
        ) : (
          <StyledLinkWithoutImage
            to={{
              pathname: `/${slug}`,
            }}
            color={index}
          >
            {this.showCategory(this.props)}
            <Title1>
              <p
                dangerouslySetInnerHTML={{
                    __html: title,
                  }}
              />
            </Title1>
          </StyledLinkWithoutImage>
        )}
      </React.Fragment>
    );
  }
}

FeaturedItem.propTypes = {
  slug: PropTypes.string.isRequired,
  featuredImage: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
};

export default FeaturedItem;
