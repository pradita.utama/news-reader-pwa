import styled from 'styled-components';


const borderRadius = (props) => {
  if (props.prev) {
    return '4px 0 0 4px;';
  }

  if (props.next) {
    return '0 4px 4px 0;';
  }
  return '0';
};

const paddingTop = props => (props.prev || props.next ? '3px' : '0px');
const backgroundColor = props => (props.active ? '#bf0016' : '#ffffff');
const color = props => (props.active ? '#ffffff' : '#bf0016');

const Paginator = styled.div`
    width: 33.9px;
    height: 31px;
    background-color: ${backgroundColor};
    flex: 1;
    font-size: 14px;
    line-height: 2.3;
    text-align: center;
    color: ${color};
    box-shadow: 1px 1px 1px 0 rgba(0,0,0,.1);
    border-radius: ${borderRadius};
    padding-top: ${paddingTop};
`;

const FlexWrapper = styled.div`
    display: flex;
    flex-direction: row;
    padding: 1em 2em;
`;


export {
  Paginator,
  FlexWrapper,
};
