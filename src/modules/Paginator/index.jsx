import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';


import searchPosts from '../SearchPostList/action';
import { Paginator, FlexWrapper } from './style';
import iconPrev from '../../images/icons/icon-chevron-left.svg';
import iconNext from '../../images/icons/icon-chevron-right.svg';
import scrollToElement from './util';

// eslint-disable-next-line
class component extends React.Component {
  static propTypes = {
    defaultQuery: PropTypes.string.isRequired,
    searchReducer: PropTypes.object.isRequired,
    spread: PropTypes.number,
    searchPosts: PropTypes.func.isRequired,
  };

  static defaultProps = {
    spread: 3,
  };

  handleGoto = (query) => {
    this.props.searchPosts(query.keyword, query.page);
    const top = document.getElementById('pagination-scroll-to-this');
    scrollToElement(top);
  }


  render() {
    let paginationNextLink = null;
    let paginationPrevLink = null;
    const { handleGoto } = this;

    const {
      searchReducer: {
        currentPage,
        perPage,
        total,
      },
      defaultQuery, spread,
    } = this.props;

    const pageCount = Math.ceil(total / perPage);
    const paginationNumberLinks = [];
    const nextPage = Math.min(currentPage + 1, pageCount);
    const prevPage = Math.max(currentPage - 1, 1);

    const isNextLinkEnabled = currentPage < pageCount;
    const isPrevLinkEnabled = currentPage > 1;


    const nextQuery = {
      keyword: defaultQuery,
      page: nextPage,
    };

    const prevQuery = {
      keyword: defaultQuery,
      page: prevPage,
    };
    paginationPrevLink = (
      <Paginator prev onClick={() => { handleGoto(prevQuery); }} onKeyDown={() => { handleGoto(prevQuery); }}>
        {isPrevLinkEnabled ?
          <img src={iconPrev} width="16px" height="15px" alt="prev" />
           :
          <img src={iconPrev} width="16px" height="15px" alt="prev" />
        }
      </Paginator>
    );

    paginationNextLink = (
      <Paginator next onClick={() => { handleGoto(nextQuery); }} onKeyDown={() => { handleGoto(prevQuery); }}>
        {isNextLinkEnabled ?
          <img src={iconNext} width="16px" height="15px" alt="next" />
        :
          <img src={iconNext} width="16px" height="15px" alt="next" />
      }
      </Paginator>
    );

    for (let i = 0, j = currentPage - spread; i < spread * 2 + 1; i++, j++) {
      // Skip illegal page numbers
      if (j < 1) {
        continue;
      } else if (j > pageCount) {
        break;
      }

      const pageQuery = {
        keyword: defaultQuery,
        page: j,
      };

      paginationNumberLinks.push((

        <Paginator key={j} active={j === currentPage} div onClick={() => { handleGoto(pageQuery); }} onKeyDown={() => { handleGoto(prevQuery); }}>
          {j}
        </Paginator>
      ));
    }


    return (
      <React.Fragment>
        <FlexWrapper>
          {paginationPrevLink}
          {paginationNumberLinks}
          {paginationNextLink}
        </FlexWrapper>
      </React.Fragment>
    );
  }
}

export default connect(null, { searchPosts })(component);

