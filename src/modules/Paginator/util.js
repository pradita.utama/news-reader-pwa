
export default function scrollToElement(elementID) {
  return elementID.scrollIntoView();
};
