import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import CloseButton from 'commons/ui-kit/Button/IconButton/CloseButton';
import { Item, Title, SubItem, SubTitle, SubWrapper, ButtonWrapper, headerButtonIcon, StyledModal } from './style';

const Component = props => (
  <div>
    <StyledModal
      isOpen={props.open}
      onRequestClose={props.handleClose}
      closeTimeoutMS={250}
    >
      <Item>
        <Title>
              Lainnya
        </Title>
        <ButtonWrapper>
          <CloseButton handleClose={props.handleClose} style={headerButtonIcon} />
        </ButtonWrapper>
      </Item>
      <SubWrapper >
        <Link to="/about-us"><SubItem className="sub-item" ><SubTitle>Tentang Kami</SubTitle></SubItem></Link>
        <Link to="/business-partnerships/conference-sponsorships"><SubItem className="sub-item" ><SubTitle>Business Partnership</SubTitle></SubItem></Link>
        <Link to="/pernyataan-etika"><SubItem className="sub-item" ><SubTitle>Etika</SubTitle></SubItem></Link>
        <Link to="https://www.techinasia.com/companies/tech-in-asia-indonesia" target="_blank"><SubItem className="sub-item" ><SubTitle>Karir</SubTitle></SubItem></Link>
        <Link to="/about-us"><SubItem className="sub-item" ><SubTitle>Hubungi Kami</SubTitle></SubItem></Link>
        <Link to="http://bantuan.techinasia.com/" target="_blank"><SubItem className="sub-item" ><SubTitle>Bantuan</SubTitle></SubItem></Link>
      </SubWrapper>
    </StyledModal>
  </div>
);

Component.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default Component;
