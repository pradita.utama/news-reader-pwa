import styled from 'styled-components';
import ModalAdapter from './ModalAdapter';

const Item = styled.div`
  width: auto;
  height: 50px;
  background-color: #ffffff;
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.16);
`;

const SubItem = styled(Item)`
  box-shadow: none;
  height: 20px;
  transition: height 0.33s ease-in;
`;

const SubWrapper = styled.div`
  margin-top: 3px;
`;

const Title = styled.span`
  font-size: 1.1em;
  font-weight: 600;
  position: absolute;
  height: 50px;
  padding: 15px;
`;
const SubTitle = styled(Title)`
  color: #bf0016;
  font-weight: normal;
`;

const ButtonWrapper = styled.div`
  float: right;
`;


const headerButtonIcon = {
  borderRadius: '0',
  width: '50px',
  height: '50px',
  margin: '0',
  paddingTop: '4px',
  border: 'none',
};


const Close = styled.button`
  background: transparent;
  border: 0;
  float: right;
`;

const StyledModal = styled(ModalAdapter)`

  &__Overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 100;
    background-color: rgba(51, 51, 51, 0.5);
    opacity: 0;

  }

  &__Overlay--after-open {
    opacity:1;
    transition: opacity 0.2s ease-in;

     .sub-item{
       height: 50px;
       transition: height 0.33s ease-in;
     }
   
  }
  &__Overlay--before-close {
    opacity: 0;
    transition: opacity 0.2s ease-in;
  }

  &__Content {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: auto;
    background-color: white;
  }
`;

export {
  Close,
  StyledModal,
};


export {
  Item,
  Title,
  SubItem,
  SubTitle,
  SubWrapper,
  ButtonWrapper,
  headerButtonIcon,
};

