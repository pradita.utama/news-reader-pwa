import React from 'react';
import { withFormik, Form, Field } from 'formik';
import { object, string } from 'yup';
import { connect } from 'react-redux';

import Button from 'commons/ui-kit/Button/BaseButton';
import { InputWrapper, ErrorWrapper, ButtonWrapper } from './style';

const NormalForm = ({
  // eslint-disable-next-line
  values, errors, touched, className, errorUser
}) => (
  <Form className={className}>
    <InputWrapper value>
      <Field type="email" name="email" disabled />
      <label>Email</label>
      { touched.email && errors.email && (
        <ErrorWrapper>{ errors.email }</ErrorWrapper>
      )}
    </InputWrapper>
    <Button label="Verifikasi Alamat E-mail" disabled />

    <InputWrapper value>
      <Field type="password" name="password" disabled />
      <label>Password</label>
      { touched.password && errors.password && (
        <ErrorWrapper>{ errors.password }</ErrorWrapper>
      )}

      { errorUser && (
        <ErrorWrapper>{ errorUser.message }</ErrorWrapper>
      )}
    </InputWrapper>

    <ButtonWrapper>
      <Button primary type="submit" size="medium" label="Ubah" />
    </ButtonWrapper>
  </Form>
);

const FormAccount = withFormik({
  mapPropsToValues: ({ profile }) => ({
    email: profile.email,
    password: '',
  }),
  enableReinitialize: true,
  validationSchema: object().shape({
    email: string()
      .email('Invalid email address')
      .required('Email is required!'),
    password: string()
      .required('Password is required'),
  }),
  handleSubmit: (values, { props }) => {
    console.log(props, values.email);
  },
  // helps with React DevTools
  displayName: 'FormAccount',
})(NormalForm);

const mapStateToProps = ({ authReducer }) => ({
  errorUser: authReducer.error,
});

export default connect(mapStateToProps)(FormAccount);
