import styled, { css } from 'styled-components';

const EditCard = styled.div`
  padding: 0 20px;
  margin: 15px 5px;
  background-color: #fff;
  box-shadow: 0 1px 3px 1px rgba(0, 0, 0, 0.16);
  border-radius: 2px;
`;

const CardTitle = styled.h5`
  margin: 0 -20px 15px;
  padding: 0 20px;
  line-height: 37px;
  font-size: 12px;
  font-weight: 600;
  letter-spacing: 1.5px;
  background: #fafafa;
  color: #333;
  text-transform: uppercase;
`;

const AvatarWrapper = styled.div`
  display: flex;
  flex-grow: 1;
  align-items: flex-end;
  flex-wrap: wrap;
  margin: 0 -5px 30px;

  > * {
    margin: 0 5px;
  }

  > img {
    height: 88px;
    width: 88px;
    border-radius: 50%;
  }
`;

const InputWrapper = styled.div`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  margin: 0 0 25px;

  input {
    margin: 0 0 5px;
    display: block;
    width: 100%;
    height: 40px;
    line-height: 40px;
    border: 0;
    border-bottom: 1px solid #ddd;
    cursor: text;
    outline: none;

    &[type=radio] {
      display: none;
    }
  }
  input:disabled {
    background: transparent;
    opacity: 0.5;

    + label {
      opacity: 0.5;
    }
  }
  input[type=radio] + label {
    display: inline-block;
    position: relative;
    font-size: 16px;
    line-height: 24px;
    padding-left: 30px;
    margin: 25px 15px 0 0;
    width: 100%;

    &:before {
      content: '';
      position: absolute;
      top: 0;
      left: 0;
      width: 20px;
      height: 20px;
      border: 2px solid #18a0e0;
      border-radius: 50%;
    }
  }
  input[type=radio]:checked + label:after {
    content: '';
    position: absolute;
    top: 5px;
    left: 5px;
    width: 10px;
    height: 10px;
    background: #18a0e0;
    border-radius: 50%;
  }
  input:not([type=radio]) + label {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    color: rgba(0, 0, 0, .38);
    line-height: 40px;
    transition: all .2s ease-in;
    pointer-events: none;    
    
    span { color: #cdcdcd; }
  }

  label {
    text-transform: capitalize;
  }

  small {
    font-size: 12px;
    color: #999;
  }

  .text-label {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    font-size: 12px;
    line-height: 12px;
    color: #18a0e0;
    transform: translateY(-100%);
  }

  + button {
    position: relative;
    top: -25px;
    left: 0;
  }

  ${props => props.noWrap && css`
    flex-wrap: nowrap;
  `}

  ${props => props.emailBox && css`
    border: 1px solid #f6c62e;
    border-radius: 4px;
    padding: 10px 10px 5px;
    flex-direction: column;

    > p {
      font-size: 12px;
      margin: 0 0 25px;
    }

    > input {
      width: calc(100% - 10px);
    }

    input:not([type=radio]) + label {
      top: unset;
      bottom: 40px;
      right: 10px;
      left: 10px;
    }
  `}

  ${props => props.value && css`
    &&& > input:not([type=radio]) + label {
      font-size: 12px;
      line-height: 12px;
      color: #18a0e0;
      transform: translateY(-100%);
    }
  `}

  ${props => props.hasIcon && css`
    padding-left: 50px;

    input {
      height: 30px;
      line-height: 30px;
    }

    label { left: 50px; }

    > span {
      position: absolute;
      top: 0;
      left:0;
      width: 50px;
      height: 50px;
      background-size: 30px;
      background-position: left;
    }
  `}

  ${props => props.hasColumns && css`
    margin-right: -5px;
    margin-left: -5px;

    input, label {
      margin: 5px;
    }
  `}
`;

const IndentWrapper = styled.div`
  width: calc(100% - 30px);
  padding-left: 30px;
  margin-top: 20px;
`;

const LengthCharBio = styled.p`
  margin: 0;
  width: 100%;
  text-align: right;
  font-size: 12px;
  color: rgba(0, 0, 0, 0.38);
`;

const ErrorWrapper = styled.div`
  font-size: 14px;
  text-align: left;
  color: #bf0016;
`;

const ButtonWrapper = styled.div`
  text-align: right;
  background-color: #fafafa;
  margin: 0 -20px;
  padding: 15px 20px;
  box-shadow: 0 -1px 0 0 rgba(228, 228, 228, 0.6);

  > button {
    margin: 0 0 0 10px;
    border-radius: 4px;
    border: 0;
  }
`;

export {
  EditCard,
  CardTitle,
  AvatarWrapper,
  InputWrapper,
  IndentWrapper,
  LengthCharBio,
  ErrorWrapper,
  ButtonWrapper,
};
