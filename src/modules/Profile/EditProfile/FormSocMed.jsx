import React from 'react';
import { withFormik, Form, Field } from 'formik';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import Button from 'commons/ui-kit/Button/BaseButton';
import Icon from 'commons/ui-kit/Icon';
import { InputWrapper, ErrorWrapper, ButtonWrapper } from './style';
import { updateMe } from '../action';

const NormalForm = ({
  // eslint-disable-next-line
  values, errors, touched, className, errorUser
}) => (
  <Form className={className}>
    <InputWrapper value={values.facebook} hasIcon>
      <Icon name="facebook-dark" />
      <Field type="text" name="facebook" placeholder="URL Facebook" />
      <small>misal: facebook.com/example</small>
      { touched.facebook && errors.facebook && (
        <ErrorWrapper>{ errors.facebook }</ErrorWrapper>
      )}
    </InputWrapper>

    <InputWrapper value={values.twitter} hasIcon>
      <Icon name="twitter-dark" />
      <Field type="text" name="twitter" placeholder="Username Twitter" />
      <small>misal: twitter.com/example</small>

      { touched.twitter && errors.twitter && (
        <ErrorWrapper>{ errors.twitter }</ErrorWrapper>
      )}
    </InputWrapper>

    <InputWrapper value={values.linkedin} hasIcon>
      <Icon name="linkedin-dark" />
      <Field type="text" name="linkedin" placeholder="URL Linkedin" />
      <small>misal: linkedin.com/in/example</small>

      { touched.linkedin && errors.linkedin && (
        <ErrorWrapper>{ errors.linkedin }</ErrorWrapper>
      )}
    </InputWrapper>

    <InputWrapper value={values.website} hasIcon>
      <Icon name="website-dark" />
      <Field type="text" name="website" placeholder="Website" />
      <small>misal: example.com</small>

      { touched.website && errors.website && (
        <ErrorWrapper>{ errors.website }</ErrorWrapper>
      )}
    </InputWrapper>

    <InputWrapper value={values.google} hasIcon>
      <Icon name="google-plus-dark" style={{ backgroundSize: '40px' }} />
      <Field type="text" name="google" placeholder="Username Google+" />
      <small>misal: +example</small>

      { touched.google && errors.google && (
        <ErrorWrapper>{ errors.google }</ErrorWrapper>
      )}
    </InputWrapper>

    <ButtonWrapper>
      <Button type="button" size="medium" label="Reset" />
      <Button primary type="submit" size="medium" label="Simpan" />
    </ButtonWrapper>
  </Form>
);

const FormSocMed = withFormik({
  mapPropsToValues: ({ profile }) => ({
    facebook: profile.facebook,
    twitter: profile.twitter,
    linkedin: profile.linkedin,
    website: profile.website,
    google: profile.google,
  }),
  enableReinitialize: true,
  handleSubmit: (values, { props }) => {
    props.updateMe({
      facebook: values.facebook,
      twitter: values.twitter,
      linkedin: values.linkedin,
      website: values.website,
      google: values.google,
      section: 'socialmedia',
    });
    props.history.push('/');
  },
  // helps with React DevTools
  displayName: 'FormSocMed',
})(NormalForm);

const mapStateToProps = ({ authReducer }) => ({
  errorUser: authReducer.error,
});

const FormSocmedWithRouter = withRouter(connect(mapStateToProps, { updateMe })(FormSocMed));

export default FormSocmedWithRouter;
