import React from 'react';
import { withFormik, Form, Field } from 'formik';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import Button from 'commons/ui-kit/Button/BaseButton';
import { updateMe } from '../action';
import {
  // eslint-disable-next-line
  AvatarWrapper, InputWrapper, ErrorWrapper, ButtonWrapper, LengthCharBio
} from './style';

const NormalForm = ({
  // eslint-disable-next-line
  profile, values, errors, touched, className, errorUser
}) => (
  <Form className={className}>
    <AvatarWrapper>
      <img src={profile.avatarUrl} alt="avatar" />
      <Button label="Unggah Avatar" />
    </AvatarWrapper>

    <InputWrapper value>
      <Field type="text" name="firstName" />
      <label>Nama Depan <span>(Wajib)</span></label>
      { touched.firstName && errors.firstName && (
        <ErrorWrapper>{ errors.firstName }</ErrorWrapper>
      )}
    </InputWrapper>

    <InputWrapper value>
      <Field type="text" name="lastName" />
      <label>Name Belakang <span>(Wajib)</span></label>
      { touched.lastName && errors.lastName && (
        <ErrorWrapper>{ errors.lastName }</ErrorWrapper>
      )}

      { errorUser && (
        <ErrorWrapper>{ errorUser.message }</ErrorWrapper>
      )}
    </InputWrapper>

    <InputWrapper noWrap hasColumns value>
      <Field type="number" name="birthday.date" />
      <Field type="number" name="birthday.month" />
      <Field type="number" name="birthday.year" />
      <label>Tanggal Lahir</label>
      { touched.birthday && errors.birthday && (
        <ErrorWrapper>{ errors.birthday }</ErrorWrapper>
      )}

      { errorUser && (
        <ErrorWrapper>{ errorUser.message }</ErrorWrapper>
      )}
    </InputWrapper>

    <InputWrapper value emailBox>
      <p>E-mail kamu dibutuhkan untuk konfirmasi password dan kabar terbaru dari kami</p>

      <Field type="email" name="emailConfirm" />
      <label>Email <span>(Wajib)</span></label>
      { touched.emailConfirm && errors.emailConfirm && (
        <ErrorWrapper>{ errors.emailConfirm }</ErrorWrapper>
      )}
    </InputWrapper>

    <InputWrapper noWrap value>
      <p className="text-label">Jenis Kelamin</p>
      <Field
        type="radio"
        name="sex"
        id="female"
        value="female"
        checked={values.sex === 'female'}
      />
      <label htmlFor="female">Perempuan</label>

      <Field
        type="radio"
        name="sex"
        id="male"
        value="male"
        checked={values.sex === 'male'}
      />
      <label htmlFor="male">Laki-Laki</label>

      { errorUser && (
        <ErrorWrapper>{ errorUser.message }</ErrorWrapper>
      )}
    </InputWrapper>

    <InputWrapper value={values.location}>
      <Field type="text" name="location" />
      <label>Domisili</label>
      { touched.location && errors.location && (
        <ErrorWrapper>{ errors.location }</ErrorWrapper>
      )}
    </InputWrapper>
    <Button label="Gunakan Lokasi Saya" />

    <InputWrapper value={values.description}>
      <Field type="text" name="description" maxLength={120} />
      <label>Biografi Singkat</label>
      { touched.description && errors.description && (
        <ErrorWrapper>{ errors.description }</ErrorWrapper>
      )}
      <LengthCharBio>
        {`${values.description.length}/120`}
      </LengthCharBio>
    </InputWrapper>

    <ButtonWrapper>
      <Button type="button" size="medium" label="Reset" />
      <Button primary type="submit" size="medium" label="Simpan" />
    </ButtonWrapper>
  </Form>
);

const FormAccount = withFormik({
  mapPropsToValues: ({ profile }) => {
    const date = profile.birthdate.slice(0, 2);
    const month = profile.birthdate.slice(3, 5);
    const year = profile.birthdate.slice(6);

    return {
      firstName: profile.firstName,
      lastName: profile.lastName,
      birthday: {
        date,
        month,
        year,
      },
      emailConfirm: profile.email,
      sex: profile.sex,
      location: profile.location.trim(),
      description: profile.description.trim(),
    };
  },
  enableReinitialize: true,
  handleSubmit: (values, { props }) => {
    const birthdate = `${values.birthday.date}-${values.birthday.month}-${values.birthday.year}`

    const data = {
      first_name: values.firstName,
      last_name: values.lastName,
      email: values.emailConfirm,
      location: values.location,
      sex: values.sex,
      description: values.description,
      birthdate,
      section: 'bio',
    };

    props.updateMe(data);
    props.history.push('/');
  },
  // helps with React DevTools
  displayName: 'FormAccount',
})(NormalForm);

const mapStateToProps = ({ authReducer }) => ({
  errorUser: authReducer.error,
});

const FormBioWithRouter = withRouter(connect(mapStateToProps, { updateMe })(FormAccount));

export default FormBioWithRouter;
