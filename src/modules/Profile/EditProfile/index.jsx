import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {
  EditCard,
  CardTitle,
} from './style';
import { requestProfileBySlug } from '../action';

import FormBio from './FormBio';
import FormSocMed from './FormSocMed';
import FormJobType from './FormJobType';
// import FormAccount from './FormAccount';

class EditProfile extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    requestProfileBySlug: PropTypes.func.isRequired,
    profile: PropTypes.object,
  }

  static defaultProps = {
    profile: null,
  }

  componentDidMount() {
    const { name } = this.props.match.params;
    this.props.requestProfileBySlug(name);
  }

  render() {
    const profile = this.props.profile.users && this.props.profile.users[0];

    return profile && (
      <React.Fragment>
        <EditCard>
          <CardTitle>Bio</CardTitle>
          <FormBio profile={profile} />
        </EditCard>

        <EditCard>
          <CardTitle>Social Media</CardTitle>
          <FormSocMed profile={profile} />
        </EditCard>

        <EditCard>
          <CardTitle>Pekerjaan</CardTitle>
          <FormJobType profile={profile} />
        </EditCard>

        {/* <EditCard>
          <CardTitle>Pengaturan Akun</CardTitle>
          <FormAccount profile={profile} />
        </EditCard> */}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  profile: state.profileReducer,
});

export default connect(mapStateToProps, { requestProfileBySlug })(EditProfile);
