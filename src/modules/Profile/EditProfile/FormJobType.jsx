import React from 'react';
import { withFormik, Form, Field } from 'formik';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import Button from 'commons/ui-kit/Button/BaseButton';
import { InputWrapper, IndentWrapper, ButtonWrapper } from './style';
import { updateMe } from '../action';

const NormalForm = ({
  // eslint-disable-next-line
  values, errors, touched, className, errorUser
}) => {
  const valuesWorking = [
    'company',
    'position',
    'industry',
  ];

  const valuesMahasiswa = [
    'university',
    'jurusan',
    'semester',
  ];

  return (
    <Form className={className}>
      <InputWrapper value>
        <p className="text-label">Pekerjaan Kamu</p>
        <Field
          type="radio"
          name="jobtype"
          id="Working"
          value="Working"
          checked={values.jobtype === 'Working'}
        />
        <label htmlFor="Working">Profesional</label>
        {
          values.jobtype === 'Working' && (
            <IndentWrapper>
              { valuesWorking.map(val => (
                <InputWrapper value={values[val]} key={val}>
                  <Field type="text" name={val} />
                  <label>{val}</label>
                </InputWrapper>
              ))}
            </IndentWrapper>
          )
        }

        <Field
          type="radio"
          name="jobtype"
          id="Mahasiswa"
          value="Mahasiswa"
          checked={values.jobtype === 'Mahasiswa'}
        />
        <label htmlFor="Mahasiswa">Mahasiswa</label>
        {
          values.jobtype === 'Mahasiswa' && (
            <IndentWrapper>
              { valuesMahasiswa.map(val => (
                <InputWrapper value={values[val]} key={val}>
                  <Field type="text" name={val} />
                  <label>{val}</label>
                </InputWrapper>
              ))}
            </IndentWrapper>
          )
        }

        <Field
          type="radio"
          name="jobtype"
          id="NoWork"
          value="NoWork"
          checked={values.jobtype === 'NoWork'}
        />
        <label htmlFor="NoWork">Belum Bekerja</label>
      </InputWrapper>

      <ButtonWrapper>
        <Button type="button" size="medium" label="Reset" />
        <Button primary type="submit" size="medium" label="Simpan" />
      </ButtonWrapper>
    </Form>
  );
};

const FormJobType = withFormik({
  mapPropsToValues: ({ profile }) => ({
    jobtype: profile.jobtype,
    company: profile.company,
    position: profile.position,
    industry: profile.industry,
    university: profile.university,
    jurusan: profile.jurusan,
    semester: profile.semester,
  }),
  enableReinitialize: true,
  handleSubmit: (values, { props }) => {
    props.updateMe({
      jobtype: values.jobtype,
      company: values.company,
      position: values.position,
      industry: values.industry,
      university: values.university,
      jurusan: values.jurusan,
      semester: values.semester,
      section: 'job',
    });
    props.history.push('/');
  },
  // helps with React DevTools
  displayName: 'FormJobType',
})(NormalForm);

const mapStateToProps = ({ authReducer }) => ({
  errorUser: authReducer.error,
});

const FormJobWithRouter = withRouter(connect(mapStateToProps, { updateMe })(FormJobType));

export default FormJobWithRouter;
