import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Error from 'commons/errors/Error';
import LoadingBar from 'commons/loading/LoadingBar';
import MainProfile from './Components/MainProfile';
import TabWrapper from './Components/TabWrapper';
import { StyledContainer } from './style';
import { requestProfileReset, requestProfileBySlug, requestPosts, requestComments } from './action';
import CONSTANT from './constant';

class Profile extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    requestProfileReset: PropTypes.func.isRequired,
    requestProfileBySlug: PropTypes.func.isRequired,
    requestPosts: PropTypes.func.isRequired,
    requestComments: PropTypes.func.isRequired,
    profileReducer: PropTypes.object.isRequired,
    userPostReducer: PropTypes.object.isRequired,
    userCommentReducer: PropTypes.object.isRequired,
  }

  state = {
    profileLoaded: false,
    currentTab: CONSTANT.USER_POSTS,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const { profileReducer } = nextProps;
    const { profileLoaded } = prevState;

    if (profileReducer.isLoaded && profileReducer.isLoaded !== profileLoaded) {
      return { profileLoaded: true };
    }
    // No state update necessary
    return null;
  }

  componentDidMount() {
    const { name } = this.props.match.params;
    this.props.requestProfileBySlug(name);
  }

  componentDidUpdate(prevProps, prevState) {
    const { profileLoaded } = this.state;
    const { name } = this.props.match.params;

    // 1st condition: run on the first render after requestProfileBySlug
    // 2nd condition: on TablLink component click
    if (prevState.profileLoaded !== profileLoaded) {
      this.props.requestPosts(name, 1, 5);
      this.props.requestComments(name, 1, 5);
    }
  }

  componentWillUnmount() {
    this.props.requestProfileReset();
  }

  handleTab = (e) => {
    const currentTab = e.currentTarget.getAttribute('id');
    this.setState(() => ({ currentTab }));
  }

  handlePageByNumber = (page) => {
    const { name } = this.props.match.params;
    const { currentTab } = this.state;

    if (currentTab === CONSTANT.USER_POSTS) {
      this.props.requestPosts(name, page, 5);
    } else if (currentTab === CONSTANT.USER_COMMENTS) {
      this.props.requestComments(name, page, 5);
    }

    // 50 from header height
    const posts = document.getElementById(currentTab);
    const offsetTop = posts.offsetTop - 50;
    window.scrollTo(0, offsetTop);
  }

  renderTab = () => {
    const { profileReducer, userPostReducer, userCommentReducer } = this.props;
    const { currentTab } = this.state;

    // check twice on fetch requestPosts & requestComments
    if (userPostReducer.isLoading && userCommentReducer.isLoading) {
      return <LoadingBar />;
    }

    if (userPostReducer.hasError && userCommentReducer.hasError) {
      return <Error message={userPostReducer.message} />;
    }

    return (userPostReducer.posts !== null || userCommentReducer.comments !== null) && (
      <TabWrapper
        profile={profileReducer}
        userPost={userPostReducer}
        userComment={userCommentReducer}
        currentTab={currentTab}
        handleTab={this.handleTab}
        handlePageByNumber={this.handlePageByNumber}
      />
    );
  }

  render() {
    const {
      users,
      isLoading,
      hasError,
      error,
    } = this.props.profileReducer;

    if (isLoading) {
      return <LoadingBar />;
    }

    if (hasError) {
      return <Error message={error.message} />;
    }

    return users !== null && (
      <StyledContainer>
        <MainProfile {...users[0]} />

        { this.renderTab() }
      </StyledContainer>
    );
  }
}

const mapStateToProps = ({
  profileReducer,
  userPostReducer,
  userCommentReducer,
}) => ({
  profileReducer,
  userPostReducer,
  userCommentReducer,
});

const mapDispatchToProps = {
  requestProfileReset,
  requestProfileBySlug,
  requestPosts,
  requestComments,
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

