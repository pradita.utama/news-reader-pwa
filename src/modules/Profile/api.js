import call from 'commons/api/RestApi';
import wordpressCall from 'commons/api/WordPressApi';

const getProfileBySlug = slug =>
  call(`/users/${slug}/profile`);

const getPosts = (slug, page, perPage) =>
  call(`/users/${slug}/talk`, 'GET', { page, per_page: perPage || 10 });

const getComments = (slug, page, perPage) =>
  call(`/users/${slug}/comments`, 'GET', { page, per_page: perPage || 10 });

const updateMyProfile = data => wordpressCall('/users/me/profile/v2', 'PUT', data);

export {
  getProfileBySlug,
  getPosts,
  getComments,
  updateMyProfile,
};
