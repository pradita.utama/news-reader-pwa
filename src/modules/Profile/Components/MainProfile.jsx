import React from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';

import SocmedWrapper from './SocmedWrapper';
import {
  MainContent,
  ImageWrapper,
  TextWrapper,
  Description,
} from '../style';

class MainProfile extends React.Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
    position: PropTypes.string.isRequired,
    company: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    slug: PropTypes.string.isRequired,
  }

  shouldComponentUpdate(nextProps) {
    if (this.props.id !== nextProps.id) {
      return true;
    }
    return false;
  }

  render() {
    const {
      avatarUrl,
      displayName,
      position,
      company,
      description,
      slug,
    } = this.props;

    return (
      <React.Fragment>
        <MainContent>
          { avatarUrl === ''
            ? <ImageWrapper>{ displayName.charAt(0) }</ImageWrapper>
            : <ImageWrapper url={avatarUrl} />
          }

          <TextWrapper>
            <h2>{displayName}</h2>
            <p>{position} {company}</p>
          </TextWrapper>
        </MainContent>

        <SocmedWrapper {...this.props} />

        <Description>
          <p>
            { description.length < 3
              ? <small>{`Sepertinya ${displayName} masih malu-malu untuk menulis biografinya`}</small>
              : description
            }
          </p>
          <Link to={`/profile/${slug}/edit`}>Ubah Profile Saya</Link>
        </Description>
      </React.Fragment>
    );
  }
}

export default withRouter(MainProfile);
