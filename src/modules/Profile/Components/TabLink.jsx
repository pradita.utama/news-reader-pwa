import React from 'react';
import PropTypes from 'prop-types';

import CONSTANT from '../constant';
import { StyledTab, StyledLink } from '../style';

const TabLink = ({
  postsTotal,
  commentsTotal,
  onClick,
  currentTab,
}) => (
  <StyledTab>
    <li>
      <StyledLink
        id={CONSTANT.USER_POSTS}
        onClick={onClick}
        active={currentTab === CONSTANT.USER_POSTS}
      >
        <span>{ postsTotal || 0 }</span>
        <small>Artikel</small>
      </StyledLink>
    </li>
    <li>
      <StyledLink
        id={CONSTANT.USER_COMMENTS}
        onClick={onClick}
        active={currentTab === CONSTANT.USER_COMMENTS}
      >
        <span>{ commentsTotal || 0 }</span>
        <small>Komentar</small>
      </StyledLink>
    </li>
  </StyledTab>
);

TabLink.defaultProps = {
  postsTotal: undefined,
  commentsTotal: undefined,
};

TabLink.propTypes = {
  postsTotal: PropTypes.number,
  commentsTotal: PropTypes.number,
  onClick: PropTypes.func.isRequired,
  currentTab: PropTypes.string.isRequired,
};

export default TabLink;
