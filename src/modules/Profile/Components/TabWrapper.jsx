import React from 'react';
import PropTypes from 'prop-types';

import CONSTANT from '../constant';
import TabLink from './TabLink';
import TabContent from './TabContent';
import TabPage from './TabPage';

const TabWrapper = ({
  profile,
  userPost,
  userComment,
  currentTab,
  handleTab,
  handlePageByNumber,
}) => {
  const isUserPost = currentTab === CONSTANT.USER_POSTS;

  return (
    <React.Fragment>
      <TabLink
        postsTotal={userPost.total}
        commentsTotal={userComment.total}
        onClick={handleTab}
        currentTab={currentTab}
      />

      <TabContent
        displayName={profile.users[0].displayName}
        isUserPost={isUserPost}
        posts={isUserPost ? userPost.posts : userComment.comments}
        isLoading={isUserPost ? userPost.isLoading : userComment.isLoading}
      />

      <TabPage
        posts={isUserPost ? userPost : userComment}
        handlePageByNumber={handlePageByNumber}
      />
    </React.Fragment>
  );
};

TabWrapper.propTypes = {
  profile: PropTypes.object.isRequired,
  userPost: PropTypes.object.isRequired,
  userComment: PropTypes.object.isRequired,
  currentTab: PropTypes.string.isRequired,
  handleTab: PropTypes.func.isRequired,
  handlePageByNumber: PropTypes.func.isRequired,
};

export default TabWrapper;
