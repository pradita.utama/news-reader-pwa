import React from 'react';
import PropTypes from 'prop-types';

import { getTextWithoutHtml } from 'utils/stringUtils';
import { diffForHumans, formatDate } from 'utils/dateUtils';
import Icon from 'commons/ui-kit/Icon';
import LoadingScreen from 'commons/loading/LoadingPreviewArticle';
import {
  StyledContent,
  PostTitle,
  PostWrapper,
  PostImageWrapper,
  CommentInTitle,
  Comment,
} from '../style';


// POST KOMENTAR
const renderComment = (post) => {
  const comment = `"${getTextWithoutHtml(post.content)}"`;
  const slug = post.postLink.replace('https://id.techinasia.com', '');

  return (
    <PostWrapper
      key={post.id}
      to={{
        pathname: slug,
        hash: `#${post.post}`,
      }}
    >
      <PostImageWrapper inComment>
        <img src={post.featuredImage.source} alt={post.featuredImage.title} />
      </PostImageWrapper>

      <CommentInTitle dangerouslySetInnerHTML={{ __html: post.postTitle }} />

      <Comment dangerouslySetInnerHTML={{ __html: comment }} />

      <small>{ formatDate(post.date) }</small>
    </PostWrapper>
  );
};

// POST ARTIKEL
const renderPosts = post => (
  <PostWrapper key={post.id} to={`/${post.slug}`}>
    <span>{ post.gaType }</span>

    <PostTitle dangerouslySetInnerHTML={{ __html: post.title }} />

    <PostImageWrapper>
      <img src={post.featuredImage.source} alt={post.featuredImage.title} />
    </PostImageWrapper>

    <small>{ diffForHumans(post.date) }</small>
  </PostWrapper>
);

// COMPONENT FOR NO POST
const renderNoContent = (displayName, isUserPost) => (
  <React.Fragment>
    <Icon name={isUserPost ? 'article' : 'chatbox'} />
    <p>Belum ada {isUserPost ? 'artikel' : 'komentar'} dibuat oleh { displayName }</p>
  </React.Fragment>
);

// POST WRAPPER
class TabContent extends React.PureComponent {
  render() {
    const {
      displayName, posts, isUserPost, isLoading,
    } = this.props;

    return (
      <StyledContent>
        {
          // eslint-disable-next-line
          posts === null || posts.length === 0
            ? renderNoContent(displayName, isUserPost)
            : isLoading
              ? <LoadingScreen />
              : posts.map(post => (
                isUserPost ? renderPosts(post, isLoading) : renderComment(post, isLoading)
              ))
        }
      </StyledContent>
    );
  }
}

TabContent.defaultProps = {
  posts: null,
};

TabContent.propTypes = {
  displayName: PropTypes.string.isRequired,
  posts: PropTypes.array,
  isUserPost: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

export default TabContent;
