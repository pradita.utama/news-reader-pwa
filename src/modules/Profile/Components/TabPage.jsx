import React from 'react';
import PropTypes from 'prop-types';

import Icon from 'commons/ui-kit/Icon';
import { PageWrapper, PageNumber, LogoWrapper } from '../style';

const TabPage = ({ posts, handlePageByNumber }) => {
  const { currentPage, totalPages, total } = posts;

  // if pages < 5 generate array from totalPages
  let pages = totalPages >= 5
    ? [1, 2, 3, 4, 5]
    : Array.from({ length: totalPages }, (item, index) => index + 1);

  // handle active link to be placed on the center
  switch (true) {
    case ((currentPage === totalPages) && (totalPages >= 5)):
      pages = pages.map(page => page + (currentPage - 5));
      break;
    case (currentPage === (totalPages - 1)):
      pages = pages.map(page => page + (currentPage - 4));
      break;
    case (currentPage > 3):
      pages = pages.map(page => page + (currentPage - 3));
      break;
    default:
      pages = pages.map(page => page);
  }

  // hide pagination if total posts = 0
  return (
    <PageWrapper>
      { total !== 0 && (
        <ul>
          { totalPages >= 5 && (
            <li role="presentation" onClick={() => handlePageByNumber(1)}>«</li>
          )}

          { pages.map(page => (
            <PageNumber
              key={page}
              active={currentPage === page}
              onClick={() => handlePageByNumber(page)}
            >
              {page}
            </PageNumber>
          ))}

          { totalPages >= 5 && (
            <li role="presentation" onClick={() => handlePageByNumber(totalPages)}>»</li>
          )}
        </ul>
      )}

      <LogoWrapper>
        <Icon name="tia-logo" />
      </LogoWrapper>
    </PageWrapper>
  );
};

TabPage.propTypes = {
  posts: PropTypes.object.isRequired,
  handlePageByNumber: PropTypes.func.isRequired,
};

export default TabPage;
