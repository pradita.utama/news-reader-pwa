import React from 'react';

import Icon from 'commons/ui-kit/Icon';
import { StyledSocmedWrapper } from '../style';

const SocmedWrapper = (user) => {
  const socmed = [
    {
      url: user.facebook,
      name: 'facebook',
    },
    {
      url: user.twitter,
      name: 'twitter',
    },
    {
      url: user.linkedin,
      name: 'linkedin',
    },
    {
      url: user.website,
      name: 'website',
    },
    {
      url: user.email,
      name: 'email',
    },
  ];

  let countEmptyUrl = 0;

  return (
    <StyledSocmedWrapper>
      {
        socmed.map((media, index) => {
          if (countEmptyUrl === socmed.length) {
            return (
              <p>Sepertinya John masih malu-malu untuk menulis biografinya</p>
            );
          }

          // eslint-disable-next-line
          return (media.url === '')
            ? countEmptyUrl += 1
            : (
              <li key={index.toString()}>
                <a href={media.url}>
                  <Icon name={media.name} />
                </a>
              </li>
            );
          })
      }
    </StyledSocmedWrapper>
  );
};

export default SocmedWrapper;
