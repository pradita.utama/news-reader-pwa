import { getProfileBySlug, getPosts, getComments, updateMyProfile } from './api';
import CONSTANT from './constant';

const requestProfileReset = () => ({
  type: CONSTANT.REQUEST_PROFILE_RESET,
});

const requestProfileBySlug = slug => ({
  type: CONSTANT.REQUEST_PROFILE,
  payload: {
    promise: getProfileBySlug(slug),
  },
});

const requestPosts = (slug, page, perPage) => ({
  type: CONSTANT.REQUEST_USER_TALK_POSTS,
  payload: {
    promise: getPosts(slug, page, perPage),
    slug,
    page,
    perPage,
  },
});

const requestComments = (slug, page, perPage) => ({
  type: CONSTANT.REQUEST_USER_COMMENTS,
  payload: {
    promise: getComments(slug, page, perPage),
    slug,
    page,
    perPage,
  },
});

const updateMe = data => ({
  type: CONSTANT.UPDATE_ME,
  payload: {
    promise: updateMyProfile(data),
  },
});

export {
  requestProfileReset,
  requestProfileBySlug,
  requestPosts,
  requestComments,
  updateMe,
};
