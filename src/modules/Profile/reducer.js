import {
  appendLoadingStates,
  startLoading,
  finishLoading,
  errorLoading,
  fetchReducer,
} from 'utils/reducerUtils';
import CONSTANT from './constant';

const AsyncState = {
  posts: { posts: null },
  comments: { comments: null },
};

const userPostReducer = fetchReducer(AsyncState.posts, CONSTANT.REQUEST_USER_TALK_POSTS);
const userCommentReducer = fetchReducer(AsyncState.comments, CONSTANT.REQUEST_USER_COMMENTS);

const initialStateUsers = appendLoadingStates({ users: null });

const profileReducer = (state = initialStateUsers, action) => {
  switch (action.type) {
    case CONSTANT.REQUEST_PROFILE:
    case CONSTANT.UPDATE_ME: {
      return startLoading(state);
    }
    case CONSTANT.UPDATE_ME_ERROR:
    case CONSTANT.REQUEST_PROFILE_ERROR: {
      return errorLoading(state, action);
    }
    case CONSTANT.REQUEST_PROFILE_SUCCESS: {
      const finish = finishLoading(state);
      return {
        ...finish,
        ...action.payload,
      };
    }
    case CONSTANT.UPDATE_ME_SUCCESS: {
      const finish = finishLoading(state);
      return {
        ...finish,
        ...action.payload,
      };
    }
    // case CONSTANT.REQUEST_PROFILE_RESET: {
    //   return resetLoading(state);
    // }
    default: {
      return state;
    }
  }
};

export {
  userPostReducer,
  userCommentReducer,
  profileReducer,
};
