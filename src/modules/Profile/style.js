import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';

const StyledContainer = styled.div`
  padding-bottom: 4em;
`;

const MainContent = styled.div`
  height: 160px;
  display: flex;
  align-items: center;
  overflow: hidden;
  background: #333;  
`;

const ImageWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 160px;
  height: 100%;
  font-size: 52px;
  font-weight: bold;
  color: #333;
  background-color: #eaeaea;

  ${props => props.url && css`
    object-fit: cover;
    object-position: center;
    background-size: cover;
    background-position: center;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0) 50%, rgb(51, 51, 51)),
      url(${props.url});
  `}
`;

const TextWrapper = styled.div`
  width: calc(100% - 160px);  
  padding: 0 20px;
  color: #fff;

  h2 {
    font-size: 21px;
    line-height: 25px;
    margin: 0 0 5px;
  }

  p {
    font-size: 14px;
    margin: 0;
  }
`;

const Description = styled.div`
  color: #fff;
  background: #333;
  padding-bottom: 20px;
  margin-bottom: 15px;

  p {
    background: #2c2c2c;
    padding: 20px;
    margin: 0;
    font-size: 17px;
    line-height: 1.65;

    small {
      display: block;
      text-align: center;
      opacity: 0.5;
    }
  }

  a {
    display: block;
    width: calc(100% - 40px);
    border: 0;
    border-radius: 2px;
    padding: 8px 16px;
    background-color: #fff;
    color: #000;
    margin: 20px auto 0;
    text-align: center;
  }
`;

const StyledSocmedWrapper = styled.ul`
  list-style: none;
  display: flex;
  justify-content: center;
  margin: 0;
  padding: 0 20px;
  background: #2c2c2c;
  border-bottom: 1px solid rgba(255, 255, 255, 0.1);

  > li {
    margin: 0;
    padding: 0;
    height: 50px;
    min-width: 60px;
    text-align: center;
    
    a {
      display: flex;
      height: 50px;
      justify-content: center;
      align-items: center;
    }
  }
`;

const StyledTab = styled.ul`
  list-style: none;
  display: flex;
  margin: 0;
  padding: 0;

  li {
    flex: 1;
    padding: 0;
    margin: 0;
    text-align: center;
  }
`;

const StyledLink = styled.button`
  height: 72px;
  width: 100%;
  display: flex;
  flex-direction: column;  
  justify-content: center;
  align-items: center;
  border-radius: 2px;
  background: #fafafa;
  border: 0;
  outline: 0;
  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.16);

  ${props => props.active && css`
    border-top: 3px solid #bf0016;
    background: #fff;    
  `}

  span {
    display: block;
    font-size: 24px;
    font-weight: 600;
    color: #bf0016;
  }

  small {
    color: #474747;
    font-size: 12px;
  }
`;

const StyledContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 0 15px 25px;
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.16);

  > span {
    opacity: 0.5;
    margin-top: 70px;
    height: 40px;
    width: 40px;
    background-size: 60px;
  }

  > p {
    opacity: 0.5;    
    font-size: 12px;
    color: #474747;
  }
`;

const PostTitle = styled.p`
  display: inline-block;
  margin: 0;
  padding-bottom: 5px;
  width: calc(100% - 50px);
  padding-right: 15px;
  line-height: 1.13;
  font-weight: bold;
  color: #333;
`;

const PostWrapper = styled(Link)`
  width: 100%;
  display: block;
  padding: 15px 0;
  border-bottom: 1px solid #eee;

  span {
    display: block;
    font-size: 12px;
    color: #bf0016;
    margin-bottom: 8px;
    text-transform: uppercase;
  }

  small {
    font-size: 10px;
    color: #474747;
  }
`;

const PostImageWrapper = styled.div`
  width: 50px;
  height: 50px;
  float: right;
  border-radius: 4px;
  overflow: hidden;

  ${props => props.inComment && css`
    width: 25px;
    height: 25px;
    float: left;
  `}

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    object-position: center;
  }
`;

const PageWrapper = styled.div`
  height: 200px;
  color: #bf0016;
  position: relative;
  
  ul {
    position: absolute;
    top: 15px;
    left: 50%;
    transform: translateX(-50%);
    list-style: none;
    padding: 0;
    margin: 0;
    border-radius: 4px;
    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.16);
    overflow: hidden;
    background: #fff;
    white-space: nowrap;
    z-index: 1;
  }

  li {
    display: inline-block;
    padding: 0 12px;
    height: 33px;
    line-height: 33px;
    text-align: center;
  }
`;

const PageNumber = styled.li`
  ${props => props.active && css`
    background: #bf0016;
    color: #fff;
  `}
`;

const LogoWrapper = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;

  span {
    display: inline-block;
    
    &:before,
    &:after {
      position: absolute;
      content: '';
      top: calc(50% - 1px);
      width: 22%;
      height: 1px;
      background: #ededed;
    }

    &:before {
      left: 21%;
    }
    &:after {
      right: 21%;
    }
  }
`;

const CommentInTitle = styled.p`
  font-size: 12px;
  width: calc(100% - 35px);
  color: #474747;
  float: left;
  margin: 0 0 0 10px;
`;

const Comment = styled.p`
  clear: both;
  margin: 0;
  padding: 8px 0 5px;
  font-weight: bold;
  color: #333;

  p { margin: 0; }
`;

export {
  StyledContainer,
  MainContent,
  ImageWrapper,
  TextWrapper,
  Description,
  StyledSocmedWrapper,
  StyledTab,
  StyledLink,
  StyledContent,
  PostTitle,
  PostWrapper,
  PostImageWrapper,
  PageWrapper,
  PageNumber,
  LogoWrapper,
  CommentInTitle,
  Comment,
};
