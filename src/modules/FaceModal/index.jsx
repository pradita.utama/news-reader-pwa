import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import FaceButton from 'commons/ui-kit/Button/IconButton/FaceButton';
import CloseButton from 'commons/ui-kit/Button/IconButton/CloseButton';
import { logout } from '../Auth/action';
import { Item, SubTitle, SubItem, CloseWrapper, FaceWrapper, closeButtonIcon, SubWrapper, Logout, StyledModal } from './style';
import Icon from './iconComponent';

const FaceModal = (props) => {
  const basePath = 'https://id.techinasia.com/profile/';
  const slug = props.user.authorUrl && props.user.authorUrl.replace(basePath, '');

  return props.user && (
    <StyledModal
      isOpen={props.open}
      onRequestClose={props.handleClose}
      closeTimeoutMS={250}
    >
      <Item>
        <FaceWrapper className="sub-face-wrapper-animation">
          <FaceButton modalOpened={props.open} user={props.user} />
        </FaceWrapper>
        <CloseWrapper>
          <CloseButton handleClose={props.handleClose} style={closeButtonIcon} />
        </CloseWrapper>
      </Item>

      <Link to={`/profile/${slug}`}>
        <SubItem className="sub-item">
          <Icon icon="person" />
          <SubTitle>Profil Saya</SubTitle>
          <Icon icon="arrow" />
        </SubItem>
      </Link>

      <Link to={{ pathname: `/profile/${slug}/edit` }}>
        <SubItem className="sub-item">
          <Icon icon="gear" />
          <SubTitle>Ubah Profil</SubTitle>
          <Icon icon="arrow" />
        </SubItem>
      </Link>

      <SubWrapper>
        <Logout className="sub-item" onClick={props.logout}>
          <Icon icon="signout" />
          <SubTitle>Keluar</SubTitle>
        </Logout>
      </SubWrapper>
    </StyledModal>
  );
};

FaceModal.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired,
};

export default connect(null, { logout })(FaceModal);
