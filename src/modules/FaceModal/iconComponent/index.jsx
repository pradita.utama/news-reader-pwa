import React from 'react';
import propTypes from 'prop-types';

import IconPerson from 'images/Navigation/icon-person.svg';
import IconGear from 'images/Navigation/icon-gear.svg';
import IconSignOut from 'images/Navigation/icon-sign-out.svg';
import IconChevronRight from 'images/Navigation/icon-chevron-right.svg';
import { IconButton, IconWrapperRight, IconWrapperLeft } from './style';

const handleSource = (icon) => {
  const src = {
    person: IconPerson,
    gear: IconGear,
    signout: IconSignOut,
    arrow: IconChevronRight,
  };
  return src[icon];
};

const Component = (props) => {
  if (props.icon === 'arrow') {
    return (
      <IconWrapperRight>
        <IconButton src={handleSource(props.icon)} width="24px" alt="icon" />
      </IconWrapperRight>
    );
  }
  return (
    <IconWrapperLeft>
      <IconButton src={handleSource(props.icon)} width="24px" alt="icon" />
    </IconWrapperLeft>
  );
};

Component.propTypes = {
  icon: propTypes.string.isRequired,
};

export default Component;
