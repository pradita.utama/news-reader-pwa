import styled from 'styled-components';

const IconWrapperLeft = styled.span`
    // padding-left: 0.8em;
`;
const IconWrapperRight = styled.span`
    float: right;
    // padding-right: 8px;
`;

const IconButton = styled.img`
  height: 24px;
`;


export { IconButton, IconWrapperLeft, IconWrapperRight };

