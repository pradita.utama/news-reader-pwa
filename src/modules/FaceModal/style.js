import styled from 'styled-components';
import ModalAdapter from './ModalAdapter';

const Item = styled.div`
  width: 100%;
  height: 50px;
  background-color: #ffffff;
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.16);
`;

const SubItem = styled(Item)`
   height: 25px;
   transition: height 0.33s ease-in;
   padding: 10px 15px 10px 15px;
`;

const FaceWrapper = styled.span`
  position: absolute;
  height: 50px;
  width: 50px;
  right: 14.7%;
  padding: 7px 0 0 15px;
`;

const Logout = styled.button`
  display: block;
  width: 100%;
  border: 0;
  text-align: left;
  box-shadow: none;
  height: 50px;
  background-color: #ffffff;
  padding: 10px 15px 10px 15px;
`;

const SubWrapper = styled.div`
  margin-top: 1px;
`;

const Title = styled.span`
  font-size: 1.1em;
  font-weight: 600;
  margin-top: 3px;
  position: absolute;
  margin-left: 15px;
`;
const SubTitle = styled(Title)`
  color: #bf0016;
  font-size: 16px;
  font-weight: normal;
`;

const CloseWrapper = styled.div`
  float: right;
  height: 50px;
  width: 59px;
  text-align: center;
`;
const StyledModal = styled(ModalAdapter)`

  &__Overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 100;
    background-color: rgba(51, 51, 51, 0.5);
    opacity: 0;
  }

  &__Overlay--after-open {

      opacity: 1;
      transition: opacity 0.2s ease-in;

     .sub-item{
       height: 50px;
       transition: height 0.33s ease-in;
     }
     .sub-face-wrapper-animation{
      right: 86%;
      transition: right .3s ease;
      transition-delay: .2s;
     }
   
  }

  &__Overlay--before-close {
    opacity: 0;
    transition: opacity 0.2s ease-in;
  }

  &__Content {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: auto;
    background-color: white;
  }
`;

const closeButtonIcon = {
  borderRadius: '0',
  width: '50px',
  height: '48px',
  margin: '0',
  paddingTop: '4px',
  border: 'none',
};


export {
  Item,
  Title,
  Logout,
  SubTitle,
  SubItem,
  SubWrapper,
  CloseWrapper,
  FaceWrapper,
  closeButtonIcon,
  StyledModal,
};

