import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';

import { checkAuth } from './modules/Auth/action';
import Header from './commons/ui-kit/Header';
import Navigation from './commons/ui-kit/Navigation';
import asyncComponent from './AsyncComponent';
import Home from './pages/Home';
import Article from './pages/Article';
// const Home = asyncComponent(() => import('./pages/Home'));
// const Article = asyncComponent(() => import('./pages/Article'));
const About = asyncComponent(() => import('./pages/About'));
const Profile = asyncComponent(() => import('./pages/Profile'));
const Category = asyncComponent(() => import('./pages/Category'));
const Tag = asyncComponent(() => import('./pages/Tag'));
const Events = asyncComponent(() => import('./pages/Events'));

const Search = asyncComponent(() => import('./pages/Search'));
const Dummy = asyncComponent(() => import('./pages/Dummy'));

// eslint-disable-next-line
const RouteLoginOnly = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      rest.anyUserLogin
        ? <Component {...props} />
        : <Redirect to="/" />
      )
    }
  />
);

class Routes extends React.Component {
  state = {
    anyUserLogin: false,
  }

  componentDidMount() {
    // eslint-disable-next-line
    this.props.checkAuth();
  }

  componentDidUpdate(prevProps) {
    // eslint-disable-next-line
    const { user } = this.props;
    const isFilledObject = !isEmpty(user);

    if (prevProps.user !== user) {
      this.toggleAnyUserLogin(isFilledObject);
    }
  }

  toggleAnyUserLogin = (state) => {
    if (state) {
      this.setState({ anyUserLogin: true });
    } else {
      this.setState({ anyUserLogin: false });
    }
  }

  render() {
    return (
      <Router>
        <div style={{ height: '100%' }}>
          <Header />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/about-us" component={About} />
            <Route exact path="/events" component={Events} />
            <Route exact path="/community" component={Dummy} />
            <Route exact path="/city-chapter" component={About} />
            <RouteLoginOnly exact path="/profile/:name/edit" component={Profile} anyUserLogin={this.state.anyUserLogin} />
            {/* dynamic routes need to be placed below static route */}
            <Route exact path="/:slug" component={Article} />
            {/* redirects old author URL (/author/:name) to /profile/:name */}
            <Route path="/author/:name" render={({ match }) => (<Redirect to={`/profile/${match.params.name}`} />)} />
            <Route exact path="/profile/:name" component={Profile} />
            <Route exact path="/talk/:slug" component={Article} />
            <Route exact path="/category/:category" component={Category} />
            <Route exact path="/tag/:tag" component={Tag} />
            <Route exact path="/search/:keyword" component={Search} />
          </Switch>
          <Navigation />
        </div>
      </Router>
    );
  }
}

const mapsStateToProps = ({ authReducer }) => ({
  user: authReducer.user,
});

export default connect(mapsStateToProps, { checkAuth })(Routes);
