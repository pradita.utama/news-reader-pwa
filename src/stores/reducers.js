import { combineReducers } from 'redux';
import { postReducer } from '../modules/ArticlePost/reducer';

import { postListReducer, categoryPostListReducer } from '../modules/PostList/reducer';
import { profileReducer, userPostReducer, userCommentReducer } from '../modules/Profile/reducer';

import stickyPostReducer from '../modules/FeaturedStories/reducer';
import { relatedPostReducer } from '../modules/ArticlePost/Components/RelatedArticle/reducer';
import { popularPostReducer } from '../modules/ArticlePost/Components/PopularArticle/reducer';
import { commentsReducer } from '../modules/ArticlePost/Components/CommentsSection/reducer';
import authReducer, { resetPassword } from '../modules/Auth/reducer';
import { tagPostsReducer } from '../modules/TagPostList/reducer';
import searchReducer from '../modules/SearchPostList/reducer';


const rootReducer = combineReducers({
  postListReducer,
  postReducer,
  categoryPostListReducer,
  profileReducer,
  userPostReducer,
  userCommentReducer,
  stickyPostReducer,
  relatedPostReducer,
  commentsReducer,
  popularPostReducer,
  tagPostsReducer,
  authReducer,
  resetPassword,
  searchReducer,
});

export default rootReducer;
