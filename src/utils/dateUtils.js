// Date utilities
import moment from 'moment';
import 'moment/locale/id';

function translateToBahasa(dates) {
  const date = dates.split(' ');
  // console.log(date)
  if (date[0] === 'sehari' || date[0] === 'sebulan' || date[0] === 'setahun') {
    const interval = date[0].split('').splice(2).join('');
    /* eslint-disable */
    const newDate = date.splice(0, 1, '1', interval);
    return date.join(' ');
  }
  return dates;
}

/**
 * Converts a GMT timestamp to local.
 *
 * @param  {String} gmt
 * @return {Object}
 */
export function convertToLocalTime(gmt) {
  return moment(gmt, 'YYYY-MM-DDTHH:mm:ss').local().format('YYYY-MM-DD HH:mm:ss');
  // return moment.utc(gmt, 'yyyy-mm-ddThh:mm:ss.ffffff').local().format('YYYY-MM-DD HH:mm:ss');
}

/**
 * Converts WordPress date-time string into relative times for display.
 *
 * @param  {String} timestamp
 * @return {String}
 */
export function diffForHumans(timestamp) {
  const postDateTime = moment.utc(timestamp);
  const now = moment.utc();

  const localPostDateTime = convertToLocalTime(timestamp);

  if (Math.abs(now.diff(postDateTime, 'seconds')) < 120) {
    return 'Baru saja';
  }

  return translateToBahasa(moment(localPostDateTime).fromNow());
  // return moment(localPostDateTime).locale('id').fromNow();
}

/**
 * Explodes YYYY-MM-DD string into Year and Month integer representations.
 *
 * @param  {String} date
 * @return {Object}
 */
export function explodeDateString(date) {
  if (!date || !date.length) {
    return {};
  }

  const momentDate = moment(date, 'YYYY-MM-DD');
  return { month: momentDate.month() + 1, year: momentDate.year() };
}

/**
 * Exports WordPress date-time string into full date format.
 *
 * @param  {String} timestamp
 * @return {String}
 */
export function formatFullDate(timestamp) {
  // const postDateTime = convertToLocalTime(timestamp);
  return moment(timestamp, 'YYYY-MM-DDTHH:mm:ss').locale('id').format('D MMMM YYYY, HH:mm');
}

/**
 * Exports Wordpress date-time string into DD MMM YYYY format.
 *
 * @param  {String} timestamp
 * @return {String}
 */
export function formatShortDate(timestamp) {
  const postDateTime = convertToLocalTime(timestamp);
  return postDateTime.format('DD MMM YYYY');
}

/**
 * Exports Wordpress date-time string into DD MMM YYYY format.
 * Does not factor in timezone.
 *
 * @param  {String} timestamp
 * @return {String}
 */
export function formatShortDateWithoutConversion(timestamp) {
  const postDateTime = moment(timestamp);
  return postDateTime.format('DD MMM YYYY');
}

/**
 * Exports Wordpress date-time string into DD MMM YYYY format.
 *
 * @param  {String} timestamp
 * @return {String}
 */
export function formatDate(timestamp) {
  const postDateTime = moment(timestamp);
  return postDateTime.format('DD MMMM YYYY');
}

/**
 * Validates that End Date is later than Start Date. Returns true if valid,
 * false otherwise.
 *
 * @param  {Number} startedMonth
 * @param  {Number} startedYear
 * @param  {Number} endedMonth
 * @param  {Number} endedYear
 * @return {Boolean}
 */
export function validateStartEndDates(startedMonth, startedYear, endedMonth, endedYear) {
  if (endedYear > startedYear || (endedYear === startedYear && endedMonth >= startedMonth)) {
    return true;
  }

  return false;
}
