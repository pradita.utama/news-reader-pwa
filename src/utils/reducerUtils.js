// Reducer utilities

/**
 * Appends loading state properties to the initial state.
 *
 * @param  {Object} state Current state.
 * @return {Object}       Expanded state.
 */
const appendLoadingStates = state => (
  Object.assign(state, {
    isLoaded: false,
    isLoading: false,
    hasError: false,
    error: null,
  })
);

/**
 * Resets loading state of the reducer state.
 *
 * @param  {Object} state Current state.
 * @return {Object}       New state.
 */
const resetLoading = state => ({
  ...state,
  isLoaded: false,
  isLoading: false,
  hasError: false,
  error: null,
});

/**
 * Starts loading state of the reducer state.
 *
 * @param  {Object} state Current state.
 * @return {Object}       New state.
 */
const startLoading = state => ({
  ...state,
  isLoaded: false,
  isLoading: true,
  hasError: false,
  error: null,
});

/**
 * Finishes loading state of the reducer state.
 *
 * @param  {Object} state Current state.
 * @return {Object}       New state.
 */
const finishLoading = state => ({
  ...state,
  isLoaded: true,
  isLoading: false,
  hasError: false,
  error: null,
});

/**
 * Throws an error to the reducer state.
 *
 * @param  {Object} state Current state.
 * @return {Object}       New state.
 */
const errorLoading = (state, error) => ({
  ...state,
  isLoaded: false,
  isLoading: false,
  hasError: true,
  error,
});

/**
 * Throws an error to the reducer state.
 *
 * @param  {Object} initialState Current state.
 * @param  {String} constant import CONSTANT
 * @return {Object} New state.
 *
 * You need to create your own CONSTANT & ACTION for resetLoading
 */
const fetchReducer = (initialState, constant) => (state = initialState, action) => {
  switch (action.type) {
    case constant: {
      return startLoading(state);
    }
    case `${constant}_ERROR`: {
      return errorLoading(state, action);
    }
    case `${constant}_SUCCESS`: {
      const finish = finishLoading(state);
      return {
        ...finish,
        ...action.payload,
      };
    }
    case `${constant}_RESET`: {
      return resetLoading(state);
    }
    default: {
      return state;
    }
  }
};

const combinePopularPosts = (payload) => {
  const array = [];
  const popular = [];
  /* eslint-disable */
  for (const key in payload) {
    if (payload.hasOwnProperty(key)) {
      array.push(key)
    }
  }
  for(let i in array) {
    for(let j = 0; j < payload[array[i]].posts.length; j++) {
      popular.push(payload[array[i]].posts[j])
    }
  }
  /* eslint-enable */
  return popular;
};

export {
  appendLoadingStates,
  resetLoading,
  startLoading,
  finishLoading,
  errorLoading,
  fetchReducer,
  combinePopularPosts,
};
