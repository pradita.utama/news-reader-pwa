/* eslint-disable react/no-multi-comp */

import React from 'react';
import Helmet from 'react-helmet';
import { pathFromUrl, stripTrailingSlash } from '../helpers/UrlHelper';
import config from '../config';
import { getTextWithoutHtml, truncateString } from './stringUtils';

export function parsePageTitle(title) {
  return `${title} - Tech in Asia`;
}

export function parseTitle(title) {
  return title.replace(/<em>|<\/em>/gi, '');
}

export function cleanUpText(title) {
  return title.replace(/<em>|<\/em>|<p>|<\/p>/gi, '');
}

export function render404Helmet() {
  const pageTitle = parsePageTitle('404');
  const pageUrl = window.location.href;
  const meta = [
    {
      name: 'description',
      content: pageTitle,
    },
    {
      property: 'og:type',
      content: 'website',
    },
    {
      property: 'og:title',
      content: pageTitle,
    },
    {
      property: 'og:url',
      content: pageUrl,
    },
    {
      property: 'og:description',
      content: pageTitle,
    },
    {
      name: 'twitter:title',
      content: pageTitle,
    },
    {
      name: 'twitter:url',
      content: pageUrl,
    },
    {
      name: 'twitter:description',
      content: pageTitle,
    },
    {
      name: 'prerender-status-code',
      content: '404',
    },
  ];
  return (
    <Helmet title={pageTitle} meta={meta} />
  );
}

export function renderPageHelmet(title, description = title, url, overrideMeta = {}) {
  const pageTitle = parsePageTitle(getTextWithoutHtml(title));
  const pageDescription = truncateString(getTextWithoutHtml(description || pageTitle));
  const pageUrl = pathFromUrl(stripTrailingSlash(url || window.location.href));
  const siteImage = config.techinasiaSiteImageUrl;

  const defaultMeta = [
    {
      name: 'description',
      content: pageDescription,
    },
    {
      property: 'og:type',
      content: 'website',
    },
    {
      property: 'og:title',
      content: pageTitle,
    },
    {
      property: 'og:url',
      content: pageUrl,
    },
    {
      property: 'og:description',
      content: pageDescription,
    },
    {
      name: 'twitter:title',
      content: pageTitle,
    },
    {
      name: 'twitter:url',
      content: pageUrl,
    },
    {
      name: 'twitter:description',
      content: pageDescription,
    },
    {
      name: 'prerender-status-code',
      content: '200',
    },
  ];

  defaultMeta.push({
    name: 'robots',
    content: 'all',
  });

  defaultMeta.push({
    name: 'twitter:image',
    content: siteImage,
  });


  defaultMeta.push({
    property: 'og:image',
    content: siteImage,
  });

  return (
    <Helmet
      title={pageTitle}
      meta={[].concat(defaultMeta, overrideMeta)}
      link={[
        {
          name: 'canonical',
          href: pageUrl,
        },
      ]}
    />
  );
}

export function renderDefaultHelmet() {
  const siteTitle = 'Tech in Asia Indonesia';
  const siteByLine = 'Komunitas Online Startup di Asia';
  const siteTitleWithByLine = `${siteTitle} - ${siteByLine}`;
  /* eslint-disable max-len */
  const siteDescription = 'Tech in Asia adalah komunitas online pelaku startup di Asia. Temukan investor, founder, dan berita menarik seputar Asia di sini.';
  const siteKeywords = 'Tech in Asia, startups, community, media, IT, tech, technology, asia, event, conference, Singapore, Bangalore, Tokyo, Jakarta';
  const siteImage = config.techinasiaSiteImageUrl;
  const siteUrl = 'https://id.techinasia.com';
  const siteTwitter = '@techinasia_id';

  const helmetMeta = [
    {
      name: 'description',
      content: siteDescription,
    },
    {
      name: 'keywords',
      content: siteKeywords,
    },
    {
      property: 'og:type',
      content: 'website',
    },
    {
      property: 'og:site_name',
      content: siteTitle,
    },
    {
      property: 'og:title',
      content: siteTitleWithByLine,
    },
    {
      property: 'og:url',
      content: siteUrl,
    },
    {
      property: 'og:image',
      content: siteImage,
    },
    {
      property: 'og:description',
      content: siteDescription,
    },
    {
      name: 'twitter:card',
      content: 'summary',
    },
    {
      name: 'twitter:site',
      content: siteTwitter,
    },
    {
      name: 'twitter:creator',
      content: siteTwitter,
    },
    {
      name: 'twitter:title',
      content: siteTitleWithByLine,
    },
    {
      name: 'twitter:url',
      content: siteUrl,
    },
    {
      name: 'twitter:image',
      content: siteImage,
    },
    {
      name: 'twitter:description',
      content: siteDescription,
    },
    {
      name: 'robots',
      content: 'all',
    },
  ];
  const helmetLink = [
    {
      rel: 'author',
      href: 'https://plus.google.com/+TechInAsiaID',
    },
    {
      rel: 'publisher',
      href: 'https://plus.google.com/+TechInAsiaID',
    },
    {
      rel: 'alternate',
      href: 'http://feeds.feedburner.com/techinasia',
      type: 'application/rss+xml',
      title: 'Tech in Asia RSS Feed',
    },
    {
      rel: 'manifest',
      href: `${config.cdnUrl}/manifest.json`,
    },
    {
      rel: 'canonical',
      href: pathFromUrl(stripTrailingSlash(window.location.href)),
    },
  ];

  return (
    <Helmet title={siteTitleWithByLine} meta={helmetMeta} link={helmetLink} />
  );
}
