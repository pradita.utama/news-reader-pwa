/* eslint-disable */
function generateUUID(keepDashes = false) {
  let date = new Date().getTime();

  if (window.performance && typeof window.performance.now === 'function') {
    date += performance.now();
  }

  let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    const r = (date + Math.random() * 16) % 16 | 0;
    date = Math.floor(date / 16);

    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });

  if (!keepDashes) {
    uuid = uuid.replace(/\-/g, '');
  }

  return uuid;
}

export default generateUUID;
