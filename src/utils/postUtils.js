export function getParentCategoryIndex(post) {
  const arr = post.categories || post.category;
  let idx = 0;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].parent === null) {
      idx = i;
      break;
    }
  }
  return idx;
}

export function getParentCategory(categories) {
  let idx = 0;
  for (let i = 0; i < categories.length; i++) {
    if (categories[i].parent === null) {
      idx = i;
      break;
    }
  }
  return categories[idx];
}

export function getFeaturedImageUrl(featuredImage) {
  if (typeof (featuredImage) === 'undefined') {
    return '';
  }

  if (typeof (featuredImage.attachmentMeta) === 'undefined') {
    return featuredImage.source || '';
  }

  const sizes = featuredImage.attachmentMeta.sizes || [];
  if (!sizes.medium) {
    return featuredImage.source || '';
  }

  return sizes.mediumLarge.url;
}
