export function stripTrailingSlash(pathname) {
  if (pathname.length > 1 && /\/$/.test(pathname)) {
    return pathname.slice(0, -1);
  }

  return pathname;
}

export function pathFromUrl(url) {
  return url.split(/[?#]/)[0];
}
