import React from 'react';
import styled from 'styled-components';

const ContentStyled = styled.div`
  padding: 20px;

  .category-placeholder {
    background-color: #EDEDED;
    height: 20px;
    width: 100px;
    margin-bottom: 20px;
  }

  .title-placeholder {
    background-color: #EDEDED;
    height: 25px;
    margin-bottom: 10px;
  }

  .avatar-placeholder {
    background-color: #EDEDED;
    height: 40px;
    width: 40px;
    border-radius:100px;
    margin-bottom: 10px;
    margin-top: 15px;
  }

  .post-meta-placeholder {
    background-color: #EDEDED;
    height: 40px;
    width: 150px;
    margin-bottom: 10px;
    margin-top: 15px;
    float: right;
    position: absolute;
    left: 80px;
    top: 205px;
  }

  .featured-image-placeholder {
    background-color: #EDEDED;
    height: 210px;
    margin-top: 25px;
    margin-bottom: 20px;
  }

  .content-placeholder {
    background-color: #EDEDED;
    height: 30px;
  }

  .content-placeholder-short {
    background-color: #EDEDED;
    height: 30px;
    width: 250px;
  }

  .content-placeholder-medium {
    background-color: #EDEDED;
    height: 30px;
    width: 350px;
  }

  @keyframes placeHolderShimmer{
    0%{
        background-position: -468px 0
    }
    100%{
        background-position: 468px 0
    }
  }

  div {
    animation-duration: 1s;
    animation-fill-mode: forwards;
    animation-iteration-count: infinite;
    animation-name: placeHolderShimmer;
    animation-timing-function: linear;
    background: #f6f7f8;
    background: linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
    background-size: 800px 104px;
    position: relative;
  }
`;

const LoadingScreen = () => (
  <ContentStyled>
    <div className="category-placeholder">
      &nbsp;
    </div>
    <div className="title-placeholder">
      &nbsp;
    </div>
    <div className="title-placeholder">
      &nbsp;
    </div>
    <div className="title-placeholder">
      &nbsp;
    </div>
    <div className="avatar-placeholder">
      &nbsp;
    </div>
    <div className="post-meta-placeholder">
      &nbsp;
    </div>
    <div className="featured-image-placeholder">
      &nbsp;
    </div>
    <div className="content-placeholder">
      &nbsp;
    </div>
    <div className="content-placeholder">
      &nbsp;
    </div>
    <div className="content-placeholder-short">
      &nbsp;
    </div>
    <div className="content-placeholder-medium">
      &nbsp;
    </div>
  </ContentStyled>
);

export default LoadingScreen;
