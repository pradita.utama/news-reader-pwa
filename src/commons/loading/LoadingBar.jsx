import React from 'react';
import styled, { keyframes } from 'styled-components';

const slide = keyframes`
  0% {
    left: 0;
  }
  50% {
    left: 75%;
  }
  100% {
    left: 0;
  }
`;

const StyledLoading = styled.div`
  position: absolute;
  display: flex;
  height: calc(100% - 110px);
  width: 100%;
  align-items: center;
  justify-content: center;
`;

const StyledLine = styled.div`
  position: relative;
  display: inline-block;
  border-radius: 10px;
  width: 200px;
  height: 5px;
  background: #ddd;

  &:before{
    content: '';
    position: absolute;
    border-radius: 16px;
    width: 25%;
    height: 5px;
    left: 0;
    background: #cb0020;
    animation: ${slide} 1.5s infinite linear;
  }
`;

const Loading = () => (
  <StyledLoading>
    <StyledLine />
  </StyledLoading>
);

export default Loading;

