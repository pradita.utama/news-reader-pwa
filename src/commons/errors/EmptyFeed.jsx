import React from 'react';
import styled from 'styled-components';

import ErrorImage from 'images/feed-failed.svg';

const Container = styled.div`
  display: flex;
  align-content: center;
  justify-content: center;
  height: 100%;

  .error-container {
    margin-top: 30px;
    text-align: center;
  }

  .error-sub-title {
    font-size: 12px;
    color: #47474787;
    width: 250px;
    margin: 10px;
  }
`;

const EmptyFeed = () => (
  <Container>
    <div className="error-container">
      <img src={ErrorImage} alt="error" />
      <div className="error-sub-title">__KOSONG__</div>
    </div>
  </Container>
);


export default EmptyFeed;
