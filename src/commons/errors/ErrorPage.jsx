import React from 'react';
import styled from 'styled-components';

import ErrorImage from 'images/error-page.svg';
import Button from '../ui-kit/Button/BaseButton';

const Container = styled.div`
  display: flex;
  align-content: center;
  justify-content: center;
  height: 100%;

  .error-container {
    margin-top: 40%;
    text-align: center;
    height: -webkit-fill-available;
  }

  .error-title {
    font-size: 16px;
    color: #333333c9;
    margin: 10px;
  }

  .error-sub-title {
    font-size: 12px;
    color: #47474787;
    width: 250px;
    margin: 10px;
  }
`;

const ErrorPage = () => (
  <Container>
    <div className="error-container">
      <img src={ErrorImage} alt="error" />
      <div className="error-title">Terjadi kesalahan pada sistem</div>
      <div className="error-sub-title">Tenang, engineer kami sedang memperbaiki masalah ini</div>
      <Button
        to="mailto:support.id@techinasia.com?subject=I found an error!
        &body=Hi, i found an error in Tech in Asia Indonesia website. Help, please"
        primary
        label="Laporkan Masalah"
      />
    </div>
  </Container>
);


export default ErrorPage;
