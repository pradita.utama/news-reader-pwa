import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import ErrorImage from 'images/sticky-failed.svg';
import Button from '../ui-kit/Button/BaseButton';

const Container = styled.div`
  display: flex;
  align-content: center;
  justify-content: center;
  height: 100%;

  .error-image {
    object-fit: contain;
    color: #47474787;
  }

  .error-container {
    margin-top: 30px;
    text-align: center;
  }

  .error-sub-title {
    font-size: 12px;
    color: #47474787;
    width: 250px;
    margin: 5px 0 8px;
  }
`;

const ErrorFeed = ({ onClick, message }) => (
  <Container>
    <div className="error-container">
      <img src={ErrorImage} className="error-image" alt="error" />
      <div className="error-sub-title">{message}</div>
      <Button
        label="Coba lagi"
        onClick={onClick}
      />
    </div>
  </Container>
);

ErrorFeed.propTypes = {
  onClick: PropTypes.func,
  message: PropTypes.string,
};

ErrorFeed.defaultProps = {
  onClick: null,
  message: 'Maaf, gagal memuat artikel featured',
};


export default ErrorFeed;
