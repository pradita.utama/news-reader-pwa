import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledDiv = styled.div`
  padding: 20px;
`;

const Error = ({ message }) => (
  <StyledDiv>
    Request API, this should be replace with nicer message or
    image. <br /><br />Error: { message }
  </StyledDiv>
);

Error.defaultProps = {
  message: '',
};

Error.propTypes = {
  message: PropTypes.string,
};


export default Error;

