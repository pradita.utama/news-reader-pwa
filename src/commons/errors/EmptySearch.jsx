import React from 'react';
import styled from 'styled-components';

import EmptyIcon from 'images/empty-result.svg';

const Container = styled.div`
  display: flex;
  align-content: center;
  justify-content: center;
  height: 100%;

  .error-container {
    margin-top: 50%;
    text-align: center;
    height: -webkit-fill-available;
  }

  .error-sub-title {
    font-size: 12px;
    color: #47474787;
    width: 250px;
    margin: 10px;
  }

  .error-title{
    color: #333333;
    font-size: 16px;
    margin: 1em 0;
  }
`;

const Component = () => (
  <Container>
    <div className="error-container">
      <img src={EmptyIcon} alt="error" />
      <div className="error-title">
        Tidak ada hasil
      </div>
      <div className="error-sub-title">
        Coba kata Kunci lain, dan lebih spesifik.
      </div>
      <div className="error-sub-title">
        mungkin akan ketemu.
      </div>
    </div>
  </Container>
);


export default Component;
