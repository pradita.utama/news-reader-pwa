import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Close from '../../images/icons/close.svg';
import CloseGrey from '../../images/icons/close-grey.svg';
import Facebook from '../../images/icons/facebook.svg';
import FacebookDark from '../../images/icons/facebook-dark.svg';
import GooglePlus from '../../images/icons/google-plus.svg';
import GooglePlusDark from '../../images/icons/google-plus-dark.svg';
import Twitter from '../../images/icons/twitter.svg';
import TwitterDark from '../../images/icons/twitter-dark.svg';
import Linkedin from '../../images/icons/linkedin.svg';
import LinkedinDark from '../../images/icons/linkedin-dark.svg';
import Website from '../../images/icons/world.svg';
import WebsiteDark from '../../images/icons/world-dark.svg';
import Email from '../../images/icons/envelope.svg';
import TIALogo from '../../images/icons/logo.svg';
import TIALogoFull from '../../images/icons/logo-full.svg';
import Article from '../../images/icons/article.svg';
import Chatbox from '../../images/icons/chatbox.svg';
import Like from '../../images/icons/icon-like.svg';
import LikeUpvoted from '../../images/icons/icon-like-red.svg';

const StyledIcon = styled.span`
  display: inline-block; 
  width: 2em;
  height: 2em;
  background-size: 2em;
  background-repeat: no-repeat;
  background-position: center center;
  background-image: url(${props => props.path});
`;

const Icon = (props) => {
  switch (props.name) {
    case 'close':
      return <StyledIcon {...props} path={Close} />;
    case 'close-grey':
      return <StyledIcon {...props} path={CloseGrey} />;
    case 'facebook-dark':
      return <StyledIcon {...props} path={FacebookDark} />;
    case 'facebook':
      return <StyledIcon {...props} path={Facebook} />;
    case 'google-plus':
      return <StyledIcon {...props} path={GooglePlus} />;
    case 'google-plus-dark':
      return <StyledIcon {...props} path={GooglePlusDark} />;
    case 'twitter':
      return <StyledIcon {...props} path={Twitter} />;
    case 'twitter-dark':
      return <StyledIcon {...props} path={TwitterDark} />;
    case 'linkedin':
      return <StyledIcon {...props} path={Linkedin} />;
    case 'linkedin-dark':
      return <StyledIcon {...props} path={LinkedinDark} />;
    case 'website':
      return <StyledIcon {...props} path={Website} />;
    case 'website-dark':
      return <StyledIcon {...props} path={WebsiteDark} />;
    case 'email':
      return <StyledIcon {...props} path={Email} />;
    case 'tia-logo':
      return <StyledIcon {...props} path={TIALogo} />;
    case 'tia-logo-full':
      return <StyledIcon {...props} path={TIALogoFull} />;
    case 'article':
      return <StyledIcon {...props} path={Article} />;
    case 'chatbox':
      return <StyledIcon {...props} path={Chatbox} />;
    case 'like':
      return <StyledIcon {...props} path={Like} />;
    case 'like-upvoted':
      return <StyledIcon {...props} path={LikeUpvoted} />;
    default:
      return null;
  }
};

Icon.propTypes = {
  name: PropTypes.string.isRequired,
};

export default Icon;
