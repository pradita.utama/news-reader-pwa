/**
 * INLINE STYLE FOR CRITICAL RENDERING
 */
import iconHome from 'images/Navigation/icon-logo.svg';
import iconStartup from 'images/Navigation/icon-rocket.svg';
import iconProfesional from 'images/Navigation/icon-person.svg';
import iconTeknologi from 'images/Navigation/icon-display.svg';
import iconEvent from 'images/Navigation/icon-calendar.svg';
import iconActiveHome from 'images/Navigation/active-icon-logo.svg';
import iconActiveStartup from 'images/Navigation/active-icon-rocket.svg';
import iconActiveProfesional from 'images/Navigation/active-icon-person.svg';
import iconActiveTeknologi from 'images/Navigation/active-icon-display.svg';
import iconActiveEvent from 'images/Navigation/active-icon-calendar.svg';

const navigationWrapper = {
  position: 'fixed',
  bottom: '0',
  margin: '0',
  padding: '0',
  width: '100%',
  backgroundColor: 'white',
  height: '50px',
  boxShadow: '0 1px 3px 0 rgba(0, 0, 0, 0.5)',
  zIndex: 3,
};

const navigationContainer = {
  display: 'flex',
  flexWrap: 'wrap',
  flexDirection: 'row',
};

const navigationContentTitle = {
  active: {
    fontSize: '0.8em',
    fontWeight: 'bold',
    color: '#BB262A',
    padding: '7px 0',
    width: '100%',
    textAlign: 'center',
  },
  nonActive: {
    fontSize: '0.8em',
    fontWeight: 'normal',
    color: '#4A4A4A',
    padding: '7px 0',
    width: '100%',
    textAlign: 'center',
  },
};

const navigationContent = {
  display: 'flex',
  flex: '1',
  fontSize: 'small',
  flexDirection: 'column',
  alignItems: 'center',
};
const navigationIconHome = {
  active: {
    backgroundImage: `url(${iconActiveHome})`,
    backgroundSize: '2.2em',
  },
  nonActive: {
    backgroundImage: `url(${iconHome})`,
    backgroundSize: '2.2em',
  },
};
const navigationIconStartups = {
  active: {
    backgroundImage: `url(${iconActiveStartup})`,
  },
  nonActive: {
    backgroundImage: `url(${iconStartup})`,
  },
};
const navigationIconProfesional = {
  active: {
    backgroundImage: `url(${iconActiveProfesional})`,
  },
  nonActive: {
    backgroundImage: `url(${iconProfesional})`,
  },
};
const navigationIconTeknologi = {
  active: {
    backgroundImage: `url(${iconActiveTeknologi})`,
  },
  nonActive: {
    backgroundImage: `url(${iconTeknologi})`,
  },
};
const navigationIconEvent = {
  active: {
    backgroundImage: `url(${iconActiveEvent})`,
  },
  nonActive: {
    backgroundImage: `url(${iconEvent})`,
  },
};

const navigationIcon = {
  height: '2em',
  backgroundSize: '2em',
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat',
};

export {
  navigationWrapper,
  navigationContainer,
  navigationContentTitle,
  navigationContent,
  navigationIconHome,
  navigationIconStartups,
  navigationIconProfesional,
  navigationIconTeknologi,
  navigationIconEvent,
  navigationIcon,
};
