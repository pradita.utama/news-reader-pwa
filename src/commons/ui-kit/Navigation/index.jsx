import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as style from './style';
import { handleActiveIcon, handleActiveTitle, handleLinkTo } from './utils';

const route = ['Home', 'Startups', 'Profesional', 'Teknologi', 'Event'];

const Component = (props) => {
  const isHidden = props.location.pathname.includes('/profile/');

  return !isHidden && (
    <nav style={style.navigationWrapper}>
      <div style={style.navigationContainer}>
        { route.map((item, index) => {
            const iconStyles = handleActiveIcon(props.location.pathname, item);
            const titleStyles = handleActiveTitle(props.location.pathname, item);
            const linkTo = handleLinkTo(item);
            return (
              <div key={index.toString()} style={style.navigationContent}>
                <Link to={linkTo} style={titleStyles}>
                  <div style={iconStyles} />
                  {item === 'Startups' ? 'Startup' : item}
                </Link>
              </div>
            );
        })}
      </div>
    </nav>
  );
};

Component.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
};

const mapStateToProps = ({ props }) => ({
  props,
});

const Navigation = withRouter(connect(mapStateToProps)(Component));


export default Navigation;
