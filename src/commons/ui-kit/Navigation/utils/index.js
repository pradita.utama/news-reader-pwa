
import * as style from '../style';

const handleLowerCase = item => item.charAt(0).toLowerCase() + item.slice(1);

// handleActivelink via slug props
const handleActiveIcon = (slug, item) => {
  if (slug === '/' && item === 'Home') {
    return Object.assign({}, style.navigationIcon, style.navigationIconHome.active);
  }
  if (slug === '/events' && item === 'Event') {
    return Object.assign({}, style.navigationIcon, style.navigationIconEvent.active);
  }
  if (slug === `/category/${handleLowerCase(item)}` && item) {
    return Object.assign({}, style.navigationIcon, style[`navigationIcon${item}`].active);
  }
  return Object.assign({}, style.navigationIcon, style[`navigationIcon${item}`].nonActive);
};

// handleActiveTitle via slug props
const handleActiveTitle = (slug, item) => {
  const res = handleLowerCase(item);
  if ((slug === '/' && item === 'Home') || (slug === '/events' && item === 'Event')) {
    return style.navigationContentTitle.active;
  }
  if (slug === `/category/${res}` && item) {
    return style.navigationContentTitle.active;
  }

  return style.navigationContentTitle.nonActive;
};

const handleLinkTo = (item) => {
  const req = {
    Home: '/',
    Startups: '/category/startups',
    Profesional: '/category/profesional',
    Teknologi: '/category/teknologi',
    Event: '/events',
  };

  return req[item];
};

export {
  handleActiveIcon,
  handleActiveTitle,
  handleLinkTo,
};
