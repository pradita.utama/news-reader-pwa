import React from 'react';
import PropTypes from 'prop-types';

const DropdownItem = (props) => {
  const {
    show, value, handleToggle, handleBlur, handleChange, options,
  } = props;

  return (
    <div className="dropdown-container">
      <label className="arrow">
        <input
          type="button"
          value={value}
          className="dropdown-btn"
          onClick={handleToggle}
          onBlur={handleBlur}
        />
      </label>
      <ul className="dropdown-list" hidden={!show}>
        {options.map((option, index) => (
          // eslint-disable-next-line
          <li key={index} className="option" onClick={handleChange(option)}>
            {option}
          </li>
        ))}
      </ul>
    </div>
  );
};

DropdownItem.propTypes = {
  show: PropTypes.bool.isRequired,
  value: PropTypes.string.isRequired,
  handleToggle: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  options: PropTypes.array.isRequired,
};

export default DropdownItem;
