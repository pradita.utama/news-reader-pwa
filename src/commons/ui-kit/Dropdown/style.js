import styled from 'styled-components';

const Container = styled.div`
  width: 6rem;

  .container {
    min-height: 100%;
    text-align: center;
  }

  // dropdown css starts

  .dropdown-container {
    width: 100%;
    margin: 0 auto;

    .dropdown-btn {
      width: 100%;
      border-radius: 3px;
      background: none;
      border: none;
      text-align: left;
      padding: 10px 15px;
      outline: none;

      &:focus {
        background: none;
      }
    }

    .arrow {
      position: relative;

      &::after {
        content: '';
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 7px 4px 0 4px;
        border-color: black transparent transparent transparent;
        position: absolute;
        right: 17px;
        top: 6px;
      }
    }

    .dropdown-list {
      list-style-type: none;
      position: absolute;
      margin: 0;
      padding: 0;
      width: 100px;
      height: 120px;
      border: none;
      background: white;
      z-index: 9;
      overflow: auto;
      border-radius: 3px;
      border: solid rgba(0,0,0,.175) 0.5px;

      .option {
        margin: 15px 0 15px;
        cursor: pointer;
      }
    }
  }
`;

export default Container;
