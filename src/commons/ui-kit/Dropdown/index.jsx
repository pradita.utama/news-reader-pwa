import React, { Component } from 'react';
import PropTypes from 'prop-types';

import DropdownItem from './DropdownItem';
import Container from './style';

class Dropdown extends Component {
  static propTypes = {
    options: PropTypes.array.isRequired,
    changed: PropTypes.func.isRequired,
  }
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.options[0],
      show: false,
    };
  }

  handleChange = option => () => {
    this.setState({ value: option, show: false });
    this.props.changed(option);
  };

  handleToggle = (e) => {
    e.target.focus();
    this.setState({ show: !this.state.show });
  };

  handleBlur = (e) => {
    // firefox onBlur issue workaround
    if (
      e.nativeEvent.explicitOriginalTarget &&
      e.nativeEvent.explicitOriginalTarget === e.nativeEvent.originalTarget
    ) {
      return;
    }

    if (this.state.show) {
      // eslint-disable-next-line
      const timer = setTimeout(() => {
        this.setState({ show: false });
      }, 200);
    }
  };

  render() {
    const { options } = this.props;
    return (
      <Container>
        <div className="container">
          <DropdownItem
            options={options}
            show={this.state.show}
            value={this.state.value}
            handleToggle={this.handleToggle}
            handleBlur={this.handleBlur}
            handleChange={this.handleChange}
          />
        </div>
      </Container>
    );
  }
}

export default Dropdown;
