import styled, { css } from 'styled-components';

const primaryColor = '#C01820';
const baseColor = 'white';
const defaultColor = 'black';
const secondary = '#690000;';

const borderPrimary = 'solid 1px #bf0016';
const borderDefault = 'solid 1px #eaeaea';
const buttonType = (props) => {
  if (props.primary) return primaryColor;
  else if (props.secondary) return secondary;
  return baseColor;
};

const buttonColor = (props) => {
  if (props.primary) return baseColor;
  else if (props.secondary) return baseColor;
  else if (props.outlinePrimary) return primaryColor;
  return defaultColor;
};


const linkTo = props => (props.to);

const borderButton = props => (props.outlinePrimary ? borderPrimary : borderDefault);


const Button = styled.button`
  /* Adapt the colours based on primary prop */
  background: ${buttonType};
  color: ${buttonColor};
  font-size: small;
  font-weight: 100;
  margin: 2px; 
  padding: 0 16px;
  height: 32px;
  line-height: 32px;
  box-shadow: ${props => (props.label === 'Masuk' ? 'none' : '0 2px 0 rgba(0,0,0,0.08)')};
  border: solid 1px #eaeaea;
  border-radius: 4px;
  outline: none;

  ${props => props.size === 'medium' && css`
    font-size: medium;
    padding: 0 18px;
    height: 42px;
    line-height: 42px;
    
    > span {
      width: 20px;
      height: inherit;
      line-height: inherit;   
    }
  `}

  ${props => props.iconWithLabel && css`
    position: relative;
    padding-left: 45px;
    text-align: left;
    
    > span {
      position: absolute;
      top: -1px;
      left: 0;
      width: 40px;
      margin-top: 2px;
    }
  `}

  .loading {
    opacity: 0.7;
    font-size: small;
    color: ${buttonColor};
  }
  ${props => props.disabled && css`
    opacity: 0.5;
  `}
`;

const Icon = styled.img`
  height: 24px;
`;

const IconButton = styled(Button)`
  border: none;
  font-size: medium;
  padding: 0;
  margin-top: 7px;
  box-shadow: none;
  border-radius: 0;
`;

const ButtonWithLink = styled.a.attrs({
  href: linkTo,
})`
  background: ${buttonType};
  color: ${buttonColor};
  font-size: small;
  padding: 0 16px;
  border-radius: 4px;
  border: ${borderButton};
  margin: 2px;
  line-height: 32px;
  display: inline-block;
`;

export {
  Button,
  Icon,
  IconButton,
  ButtonWithLink,
};
