import React from 'react';
import PropTypes from 'prop-types';
import { Button, IconButton, ButtonWithLink, Icon } from './style';

const Content = (props) => {
  const {
    icon, src, iconWithLabel, label, to, isLoading,
  } = props;
  if (icon) {
    return (
      <IconButton {...props}>
        <Icon src={src} alt="icon" />
      </IconButton>
    );
  }

  if (to) {
    return (
      <ButtonWithLink {...props} >
        {label}
      </ButtonWithLink>
    );
  }

  if (isLoading) {
    return (
      <Button {...props} >
        <div className="loading">
          {label}
        </div>
      </Button>
    );
  }

  return (
    <Button {...props}>
      {/* if common button have an icon */}
      { iconWithLabel && iconWithLabel() }

      { label }
    </Button>
  );
};

const Component = props => (
  <React.Fragment>
    {Content(props)}
  </React.Fragment>
);

Content.defaultProps = {
  to: '',
  iconWithLabel: null,
};

Content.propTypes = {
  label: PropTypes.string.isRequired,
  icon: PropTypes.bool.isRequired,
  iconWithLabel: PropTypes.node,
  src: PropTypes.string.isRequired,
  iconSize: PropTypes.string.isRequired,
  to: PropTypes.string,
  style: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

export default Component;

