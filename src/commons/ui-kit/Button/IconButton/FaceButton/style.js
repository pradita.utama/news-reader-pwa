import styled from 'styled-components';


const baseColor = 'white';

const Button = styled.button`
  /* Adapt the colours based on primary prop */
  background: ${baseColor};
  border: none;
  margin-top: 4px;
  padding: 0;
  &:focus {
    outline: none;
  }
`;

const Pic = styled.img`
  width: 2em;
  height: 2em;
  border-radius: 50%;
`;


export {
  Button,
  Pic,
};
