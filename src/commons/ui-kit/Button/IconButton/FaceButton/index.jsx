import React from 'react';
import PropTypes from 'prop-types';
import { Button, Pic } from './style';
import asyncComponent from '../../../../../AsyncComponent';

const FaceModal = asyncComponent(() => import('../../../../../modules/FaceModal'));

class FaceButton extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      modalIsOpen: false,
    };
  }
  
  openModal = () => {
    this.setState({ modalIsOpen: true });
  }


  closeModal = () => {
    this.setState({ modalIsOpen: false });
  }

  render() {
    const { modalIsOpen } = this.state;
    const { modalOpened, user } = this.props;

    return (
      <React.Fragment>
        <Button onClick={this.openModal} animation={modalIsOpen} style={this.props.style}>
          <Pic src={user.avatarUrl} alt="profpic" />
        </Button>
        { !modalOpened &&
          <FaceModal
            handleClose={this.closeModal}
            animation={modalIsOpen}
            open={modalIsOpen}
            user={user}
          />
        }
      </React.Fragment>
    );
  }
}

FaceButton.defaultProps = {
  style: null,
  modalOpened: false,
  user: {},
};

FaceButton.propTypes = {
  style: PropTypes.object,
  modalOpened: PropTypes.bool,
  user: PropTypes.object,
};

export default FaceButton;

