import React from 'react';
import PropTypes from 'prop-types';
import icon from '../../../../../images/Navigation/icon-rocket.svg';
import Button from '../../BaseButton';

const Component = ({ style }) => (
  <Button
    icon="true"
    iconSize="medium"
    src={icon}
    style={style}
  />
);

Component.defaultProps = {
  style: null,
};

Component.propTypes = {
  style: PropTypes.object,
};

export default Component;
