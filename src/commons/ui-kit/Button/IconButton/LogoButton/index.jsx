import React from 'react';
import PropTypes from 'prop-types';

import icon from '../../../../../images/logo.png';

const style = {
  fontSize: 'large',
  height: '1.5em',
};


const Component = ({ onClick }) => (
  <img src={icon} style={style} alt="logo" onClick={onClick} role="presentation" />
);

Component.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default Component;

