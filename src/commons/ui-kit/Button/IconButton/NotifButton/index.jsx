import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import icon from '../../../../../images/Navigation/icon-bell.svg';
import Button from '../../BaseButton';

const Container = styled.div`
  button > img {
    height: 24px;
  }
`;

const Component = ({ style }) => (
  <Container>
    <Button
      icon="true"
      src={icon}
      style={style}
    />
  </Container>
);

Component.defaultProps = {
  style: null,
};

Component.propTypes = {
  style: PropTypes.object,
};

export default Component;
