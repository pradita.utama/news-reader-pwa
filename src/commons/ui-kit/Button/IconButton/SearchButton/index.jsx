import React from 'react';
import PropTypes from 'prop-types';
import icon from '../../../../../images/Navigation/icon-search.svg';
import Button from '../../BaseButton';
import asyncComponent from '../../../../../AsyncComponent';

const SearchModal = asyncComponent(() => import('../../../../../modules/SearchModal'));

class Component extends React.Component {
  state = {
    modalIsOpen: false,
  }

  openModal = () => {
    this.setState({ modalIsOpen: true });
  }


  closeModal = () => {
    this.setState({ modalIsOpen: false });
  }


  render() {
    const { modalIsOpen } = this.state;
    const {
      modalOpened,
      handleSubmit,
      style,
    } = this.props;
    const { openModal, closeModal } = this;

    return (
      <React.Fragment>
        {
          modalOpened ? (
            <Button
              icon="true"
              src={icon}
              onClick={handleSubmit}
              style={style}
            />
          ) : (
            <Button
              icon="true"
              src={icon}
              onClick={openModal}
              style={style}
            />
          )
        }

        {
          !modalOpened &&
          <SearchModal open={modalIsOpen} handleClose={closeModal} {...this.props} />
        }

      </React.Fragment>
    );
  }
}

Component.defaultProps = {
  style: null,
  modalOpened: null,
  handleSubmit: null,
};

Component.propTypes = {
  style: PropTypes.object,
  modalOpened: PropTypes.bool,
  handleSubmit: PropTypes.func,
};

export default Component;
