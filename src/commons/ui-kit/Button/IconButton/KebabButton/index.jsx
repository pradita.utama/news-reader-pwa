import React from 'react';
import PropTypes from 'prop-types';
import icon from '../../../../../images/Navigation/icon-kebab.svg';
import Button from '../../BaseButton';
import asyncComponent from '../../../../../AsyncComponent';

const KebabModal = asyncComponent(() => import('../../../../../modules/KebabModal'));

class Component extends React.Component {
  constructor() {
    super();

    this.state = {
      modalIsOpen: false,
    };
  }

  openModal = () => {
    this.setState({ modalIsOpen: true });
  }


  closeModal = () => {
    this.setState({ modalIsOpen: false });
  }

  render() {
    const { modalIsOpen } = this.state;
    return (
      <React.Fragment>
        <Button
          icon="true"
          src={icon}
          onClick={this.openModal}
          style={this.props.style}
        />
        <KebabModal open={modalIsOpen} handleClose={this.closeModal} />
      </React.Fragment>
    );
  }
}

Component.defaultProps = {
  style: null,
};

Component.propTypes = {
  style: PropTypes.object,
};

export default Component;
