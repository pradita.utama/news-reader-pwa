import React from 'react';
import PropTypes from 'prop-types';


import icon from '../../../../../images/icons/close-grey.svg';
import Button from '../../BaseButton';


const Component = props => (
  <Button type="button" icon="true" src={icon} onClick={props.handleClose} style={props.style} />
);

Component.propTypes = {
  handleClose: PropTypes.func.isRequired,
  style: PropTypes.object,
};

Component.defaultProps = {
  style: null,
};

export default Component;

