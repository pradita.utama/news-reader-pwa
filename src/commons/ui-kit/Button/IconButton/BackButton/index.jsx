import React from 'react';
import PropTypes from 'prop-types';

import icon from '../../../../../images/Navigation/arrow_back_black.png';
import Button from '../../BaseButton';


const navigateBack = props => props.history.goBack();


const Component = props => (
  <Button icon="true" src={icon} onClick={() => navigateBack(props)} style={props.style} />
);

Component.propTypes = {
  style: PropTypes.object,
};

Component.defaultProps = {
  style: null,
};

export default Component;
