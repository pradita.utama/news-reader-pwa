import React from 'react';
import PropTypes from 'prop-types';
import Button from '../../BaseButton';

const Component = props => (
  <Button
    primary
    id="register"
    label="Daftar"
    onClick={props.HandleClick}
  />
);

Component.propTypes = {
  HandleClick: PropTypes.func.isRequired,
};

export default Component;
