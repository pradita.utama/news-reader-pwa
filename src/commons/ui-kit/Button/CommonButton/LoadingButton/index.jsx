import React from 'react';
import PropTypes from 'prop-types';

import Button from '../../BaseButton';

const Component = props => (
  <Button
    label={props.label}
    {...props}
    isLoading
  />
);

Component.propTypes = {
  label: PropTypes.string,
};

Component.defaultProps = {
  label: '',
};

export default Component;
