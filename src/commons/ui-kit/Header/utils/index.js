const EndPointShouldBeMainHeader = (url) => {
  const requirement = ['/', '/category/startups', '/category/teknologi', '/category/profesional', '/events'];
  return requirement.some(item => item === url);
};

const EndPointShouldBeProfile = url => url.includes('/profile/');


export {
  EndPointShouldBeMainHeader,
  EndPointShouldBeProfile,
};
