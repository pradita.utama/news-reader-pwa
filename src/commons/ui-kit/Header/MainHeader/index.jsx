import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';

import AfterLogin from './Components/AfterLogin';
import BeforeLogin from './Components/BeforeLogin';

const MainHeader = (props) => {
  const isEmptyObject = isEmpty(props.user);

  return (
    isEmptyObject ? <BeforeLogin {...props} /> : <AfterLogin {...props} />
  );
};

MainHeader.propTypes = {
  user: PropTypes.object.isRequired,
};

export default MainHeader;
