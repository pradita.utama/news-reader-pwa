import React from 'react';
import PropTypes from 'prop-types';

import {
  headerTitle,
  headerContainer,
  headerContentTitle,
  headerContent,
  headerButtonIcon,
} from '../../style';
import NotifButton from '../../../Button/IconButton/NotifButton';
import SearchButton from '../../../Button/IconButton/SearchButton';
import KebabButton from '../../../Button/IconButton/KebabButton';
import FaceButton from '../../../Button/IconButton/FaceButton';
import handleTitle from './utils';

const facePaddingTop = {
  paddingTop: '5px',
};

const headerFaceButtonIcon = {
  ...headerButtonIcon,
  ...facePaddingTop,
};

const AfterLogin = props => (
  <div style={headerContainer} >
    <div style={headerContentTitle}>
      <div style={headerTitle}>
        {handleTitle(props)}
      </div>
    </div>
    <div style={headerContent}>
      <SearchButton {...this.props} style={headerButtonIcon} />
    </div>
    <div style={headerContent}>
      <NotifButton style={headerButtonIcon} />
    </div>
    <div style={headerContent}>
      <FaceButton style={headerFaceButtonIcon} user={props.user} />
    </div>
    <div style={headerContent}>
      <KebabButton style={headerButtonIcon} />
    </div>
  </div>
);

AfterLogin.propTypes = {
  user: PropTypes.object.isRequired,
};

export default AfterLogin;
