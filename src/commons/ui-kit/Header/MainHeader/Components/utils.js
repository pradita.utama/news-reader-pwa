
const handleTitle = (props) => {
  if (props.location.pathname === '/') {
    return 'Home';
  }
  if (props.location.pathname === '/events') {
    return 'Event';
  }
  if (props.location.pathname === '/category/startups') {
    return 'Startup';
  }
  const temp = props.location.pathname.split('/')['2'];
  return temp.charAt(0).toUpperCase() + temp.slice(1);
};


export default handleTitle;
