import React from 'react';

import ModalLoginOrRegister from 'modules/Auth';
import {
  headerTitle,
  headerContainer,
  headerContentTitle,
  headerContent,
} from '../../style';
import SearchButton from '../../../Button/IconButton/SearchButton';
import KebabButton from '../../../Button/IconButton/KebabButton';
import LoginButton from '../../../Button/CommonButton/LoginButton';
import RegisterButton from '../../../Button/CommonButton/RegisterButton';
import handleTitle from './utils';

class BeforeLogin extends React.Component {
  state = {
    modalIsOpen: false,
    modalId: 'login',
  };

  componentDidUpdate(prevProps) {
    // eslint-disable-next-line
    if (prevProps.user !== this.props.user) {
      this.closeModal();
    }
  }

  openModal = (e) => {
    const { id } = e.currentTarget;

    this.setState({
      modalIsOpen: true,
      modalId: id,
    });
  }

  changeModalId = (id) => {
    this.setState({ modalId: id });
  }

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  }

  render() {
    const { modalId, modalIsOpen } = this.state;

    return (
      <React.Fragment>
        <div style={headerContainer}>
          <div style={headerContentTitle}>
            <div style={headerTitle}>
              { handleTitle(this.props) }
            </div>
          </div>

          <div style={headerContent}>
            <SearchButton {...this.props} />
          </div>

          <div style={headerContent}>
            <LoginButton HandleClick={this.openModal} />
          </div>

          <div style={headerContent}>
            <RegisterButton HandleClick={this.openModal} />
          </div>

          <div style={headerContent}>
            <KebabButton />
          </div>
        </div>

        {/* LOGIN MODAL Or Register */}
        <ModalLoginOrRegister
          modalId={modalId}
          modalIsOpen={modalIsOpen}
          changeModalId={this.changeModalId}
          closeModal={this.closeModal}
        />
      </React.Fragment>
    );
  }
}

export default BeforeLogin;
