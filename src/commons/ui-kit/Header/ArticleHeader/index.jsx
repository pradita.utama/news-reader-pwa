import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import {
  headerContainer,
  headerLogo,
  headerButtonIcon,
  headerContentRight,
  headerContentLeft,
} from '../style';
import SearchButton from '../../Button/IconButton/SearchButton';
import BackButton from '../../Button/IconButton/BackButton';
import LogoButton from '../../Button/IconButton/LogoButton';

const Component = props => (
  <div style={headerContainer}>
    <div style={headerContentLeft}>
      <BackButton {...props} style={headerButtonIcon} />
    </div>
    <div style={headerLogo}>
      <LogoButton onClick={() => props.history.push('/')} />
    </div>
    <div style={headerContentRight}>
      <SearchButton style={headerButtonIcon} {...props} />
    </div>
  </div>
);

Component.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(Component);
