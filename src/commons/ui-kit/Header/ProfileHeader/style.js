
const headerTitle = {
  display: 'inline-block',
  lineHeight: 'inherit',
  fontSize: '1.075em',
  marginTop: '0.8em',
  marginLeft: '0.8em',
  marginBottom: '0.6em',
};

const TitleWrapper = {
  display: 'flex',
  flex: '7',
  alignItems: 'center',

};


export {
  headerTitle,
  TitleWrapper,
};
