import React from 'react';
import PropTypes from 'prop-types';

import {
  headerContainer,
  headerContent,
  headerButtonIcon,
} from '../style';

import {
  headerTitle,
  TitleWrapper,
} from './style';

import BackButton from '../../Button/IconButton/BackButton';

const ProfileHeader = (props) => {
  const { profile, location } = props;
  const isEditProfile = (location.pathname === '/profile/edit') === true;

  return (
    <div style={headerContainer}>
      <div style={headerContent}>
        <BackButton {...props} style={headerButtonIcon} />
      </div>

      <div style={TitleWrapper}>
        <div style={headerTitle}>
          {
            profile
              ? `${profile[0].firstName} ${profile[0].lastName}`
              : isEditProfile && 'Ubah Profile'
          }
        </div>
      </div>
    </div>
  );
};

ProfileHeader.defaultProps = {
  profile: null,
};

ProfileHeader.propTypes = {
  profile: PropTypes.array,
  location: PropTypes.object.isRequired,
};

export default ProfileHeader;
