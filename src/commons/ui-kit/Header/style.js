/**
 * INLINE STYLE FOR CRITICAL RENDERING
 */
// FOR CRITICAL RENDERING DO NOT USE STYLED-COMPONENTS
const headerWrapper = {
  height: '50px',
};

const headerStyle = {
  position: 'fixed',
  width: '100%',
  background: '#fff',
  height: '50px',
  boxShadow: '0 0 8px 0 #ccc',
  display: 'inline-flex',
  alignItems: 'center',
  zIndex: '100',
};

const headerTitle = {
  display: 'inline-block',
  lineHeight: 'inherit',
  fontSize: '1.075em',
  marginTop: '0.8em',
  marginLeft: '0.8em',
  marginBottom: '0.6em',
  fontWeight: '600',
};

const headerContainer = {
  display: 'flex',
  flexWrap: 'wrap',
  flexDirection: 'row',
  width: '100%',
  height: '50px',
};

const headerContentTitle = {
  display: 'flex',
  flex: '5',
  alignItems: 'center',
};

const headerContent = {
  display: 'flex',
  flex: '1',
  alignItems: 'center',
  justifyContent: 'center',
};

const headerContentLeft = {
  display: 'flex',
  flex: '1',
  alignItems: 'left',
  justifyContent: 'left',
};

const headerContentRight = {
  display: 'flex',
  flex: '1',
  alignItems: 'flex-end',
  justifyContent: 'flex-end',
};


const headerLogo = {
  display: 'flex',
  flex: '5',
  alignItems: 'center',
  justifyContent: 'center',
};

const headerButtonIcon = {
  borderRadius: '0',
  width: '50px',
  height: '50px',
  margin: '0',
  paddingTop: '12px',
  border: 'none',
};

export {
  headerWrapper,
  headerStyle,
  headerTitle,
  headerContainer,
  headerContentTitle,
  headerContent,
  headerLogo,
  headerButtonIcon,
  headerContentRight,
  headerContentLeft,
};
