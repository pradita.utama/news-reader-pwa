import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import {
  headerWrapper,
  headerStyle,
} from './style';

import MainHeader from './MainHeader';
import ArticleHeader from './ArticleHeader';
import ProfileHeader from './ProfileHeader';

import { EndPointShouldBeMainHeader, EndPointShouldBeProfile } from './utils';

class RenderContent extends React.Component {
  static propTypes = {
    location: PropTypes.shape({
      pathname: PropTypes.string,
    }).isRequired,
  };

  componentDidMount() {
    document.querySelector('header > div').style.animation = 'slideDownHeader 0.5s';
    window.addEventListener('scroll', this.handleScroll);
  }

  handleScroll = () => {
    if (document.documentElement.scrollTop > 100 || document.body.scrollTop > 100) {
      document.querySelector('header > div').style.animation = 'slideUpHeader 0.5s';
      document.querySelector('header > div').style.top = '-50px';
    } else {
      document.querySelector('header > div').style.animation = 'slideDownHeader 0.5s';
      document.querySelector('header > div').style.top = '0';
    }
  };

  render() {
    const { location } = this.props;

    if (EndPointShouldBeMainHeader(location.pathname)) {
      return (
        <MainHeader {...this.props} />
      );
    }

    if (EndPointShouldBeProfile(location.pathname)) {
      return (
        <ProfileHeader {...this.props} />
      );
    }

    return (
      <ArticleHeader {...this.props} />
    );
  }
}

const HeaderWrapper = props => (
  <header style={headerWrapper}>
    <div style={headerStyle}>
      <RenderContent {...props} />
    </div>
  </header>
);

const mapStateToProps = state => ({
  user: state.authReducer.user,
  profile: state.profileReducer.users,
});


const Header = withRouter(connect(mapStateToProps)(HeaderWrapper));

export default Header;
