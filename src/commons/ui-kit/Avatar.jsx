import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ImageLoader from 'react-load-image';

const Image = styled.img`
  border-radius: 100px;
  height: 36px;
  width: 36px;
`;

const Div = styled.div`
  height: 36px;
  width: 36px;
  border-radius: 100px;
  background-color: #cdcdcd;
  color: #fff;
  text-align: center;
  padding-top: 7px;
  font-size: 20px;
`;

const LoadingAvatar = ({ displayName }) => {
  const initialChar = displayName.charAt(0);
  return <Div>{initialChar}</Div>;
};

const Avatar = ({ src, alt }) => (
  <React.Fragment>
    <ImageLoader src={src}>
      <Image alt={alt} />
      <LoadingAvatar displayName={alt} />
      <LoadingAvatar displayName={alt} />
    </ImageLoader>
  </React.Fragment>
);

LoadingAvatar.propTypes = {
  displayName: PropTypes.string,
};

LoadingAvatar.defaultProps = {
  displayName: 'Tech in Asia',
};

Avatar.defaultProps = {
  alt: 'User Avatar',
};

Avatar.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string,
};

export default Avatar;
