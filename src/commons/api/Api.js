import camelCaseRecursive from 'camelcase-keys-recursive';

/**
 * Converts object into a URL param for GET methods.
 *
 * @param  {Object} data
 * @return {String}
 */
const buildUrlParams = (data) => {
  if (!data) {
    return '';
  }

  const params = [];

  Object.keys(data).forEach((key) => {
    const value = data[key];

    if (value instanceof Array) {
      value.map((item) => {
        params.push(`${encodeURIComponent(key)}[]=${encodeURIComponent(item)}`);
        return item;
      });
    } else if (value && value !== '') {
      params.push(`${encodeURIComponent(key)}=${encodeURIComponent(value)}`);
    }
  });

  return params.join('&');
};

/**
 * Parses the error response.
 *
 * @param  {Object} errorResponse
 * @return {Object}
 */
const parseErrorResponse = (errorResponse) => {
  // Return an object if the error is a 1-element array
  if (Array.isArray(errorResponse) && errorResponse.length === 1) {
    return errorResponse[0];
  }

  return errorResponse;
};

/**
 * Checks the response's HTTP status and returns a response
 * or error accordingly.
 *
 * @param  {Object} response
 * @return {mixed}
 */
const checkStatus = (response) => {
  if (response.status >= 200 && response.status < 400) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = response;

  throw error;
};

/**
 * Make an API call to the specified rest api (golang) API endpoint.
 *
 * @param  {string} endpoint
 *
 * @return {Promise}
 */

const api = (url, method = 'GET', data = null, headers = {}) => {
  const options = {
    method,
    headers,
  };

  if (method !== 'GET') {
    if (data instanceof FormData) {
      options.body = data !== null ? data : null;
    } else {
      options.body = data !== null ? JSON.stringify(data) : null;
    }
  }

  // Include cookies
  options.credentials = 'include';

  const builtUrl = (method === 'GET' && data !== null)
    ? `${url}?${buildUrlParams(data)}`
    : url;

  return new Promise((resolve, reject) => {
    // Request timeout.
    const requestTimeout = setTimeout(() => {
      reject(new Error('Request timeout.'));
    }, 12000);

    fetch(builtUrl, options)
      .then(checkStatus)
      .then(response => response.json())
      .then(response => camelCaseRecursive(response))
      .then((response) => {
        clearTimeout(requestTimeout);
        return response;
      })
      .then(response => resolve(response))
      .catch((error) => {
        if (error.response) {
          error.response.json().then((errorResponse) => {
            const appendedError = error;

            // Return an object if the error is a 1-element array
            appendedError.responseBody = parseErrorResponse(errorResponse);

            clearTimeout(requestTimeout);
            reject(appendedError.responseBody);
          });
        } else {
          clearTimeout(requestTimeout);
          reject(error);
        }
      });
  });
};

export default api;
