import config from 'config';
import api from './Api';

/**
 * Make an API call to the specified WordPress API endpoint.
 *
 * @param  {string} endpoint
 *
 * @return {Promise}
 */
const call = (endpoint, method = 'GET', data = null, headers = {}) => {
  const url = (endpoint.indexOf() === -1 ? config.apis.wordpress.url : '') + endpoint;

  // By default add custom headers for WordPress to accept our data.
  const requestHeaders = (method !== 'GET' && !(data instanceof FormData)) ? Object.assign({
    Accept: 'application/json',
    'Content-Type': 'application/json',
  }, headers) : {};

  return api(url, method, data, requestHeaders);
};

export default call;
