import React, { Component } from 'react';
import debounce from 'lodash/debounce';
import PropTypes from 'prop-types';

class InfiniteScroll extends Component {
  static propTypes = {
    active: PropTypes.bool,
    children: PropTypes.node.isRequired,
    onLoadMore: PropTypes.func.isRequired,
    threshold: PropTypes.number,
  };

  static defaultProps = {
    active: true,
    threshold: 300,
  };

  constructor(props) {
    super(props);

    this.checkScrollListener = debounce(this.checkScrollListener.bind(this), 200);
    this.scrollListener = this.scrollListener.bind(this);
  }

  componentDidMount() {
    this.attachScrollListener();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.active) {
      this.attachScrollListener();
    } else if (!nextProps.active) {
      this.detachScrollListener();
    }
  }

  componentWillUnmount() {
    this.detachScrollListener();
  }

  attachScrollListener() {
    window.addEventListener('scroll', this.checkScrollListener);
    window.addEventListener('resize', this.checkScrollListener);
  }

  detachScrollListener() {
    window.removeEventListener('scroll', this.checkScrollListener);
    window.removeEventListener('resize', this.checkScrollListener);
    this.checkScrollListener.cancel();
  }

  checkScrollListener() {
    this.checkScrollListener.cancel();
    this.scrollListener();
  }

  scrollListener() {
    if (!this.props.active) {
      return;
    }
    const { body } = document;
    const html = document.documentElement;
    const bodyH = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      body.getBoundingClientRect().height,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight,
    );
    if (window.scrollY >= bodyH - this.props.threshold) {
      this.detachScrollListener();
      this.props.onLoadMore();
    }
  }
  render() {
    return <div>{this.props.children}</div>;
  }
}

export default InfiniteScroll;
