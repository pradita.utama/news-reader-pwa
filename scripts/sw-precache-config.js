module.exports = {
  staticFileGlobs: [
    'build/static/images/*.*',
    'build/logo.png',
    'build/static/media/*.*',
    'build/static/css/*.css',
    'build/static/js/*.js',
    'build/index.html',
  ],
  stripPrefix: 'build',
  navigateFallback: '/index.html',
  runtimeCaching: [{
    urlPattern: /wp-json/,
    handler: 'networkFirst',
    options: {
      cache: {
        maxEntries: 50,
        name: 'json-cache',
      },
    },
  }, {
    urlPattern: /cloudfront.net/,
    handler: 'cacheFirst',
    options: {
      cache: {
        maxEntries: 50,
        name: 'cdn-cache',
      },
    },
  }],
};
