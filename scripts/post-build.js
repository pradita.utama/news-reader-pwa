/* eslint-disable */
var mime = require('mime-types')

const testFolder = './build/static/media/';
const testFolderJS = './build/static/js/';
const path = '/static/media/';
const fs = require('fs');
var text = '';
fs.readdirSync(testFolder).forEach((file) => {
  var patt = new RegExp('woff');
  if (patt.test(file)) {
    text = text + '<link rel="preload" href="' + path + file + '" as="font" type="font/woff">';
  }
})

// fs.readdirSync(testFolderJS).forEach((file) => {
//   var pattJS = new RegExp('chunk.js');
//   if (pattJS.test(file)) {
//     text = text + '<link rel="preload" href="' + path + file + '" as="script">';
//   }
// })

// console.log(text);

fs.readFile('./build/index.html', 'utf8', function(err, data) {
  if (err) throw err;
  console.log('OK: ' + './build/index.html');

  var newText = data.replace('</head>', text + '</head>');
  // console.log(newText);

  fs.writeFile('./build/index.html', newText, function (err) {
    if (err) return console.log(err);
    // console.log('HTML');
  });
});


{/* <link rel="preload" href="fonts/cicle_fina-webfont.eot" as="font" type="application/vnd.ms-fontobject" crossorigin="anonymous">
<link rel="preload" href="fonts/cicle_fina-webfont.woff2" as="font" type="font/woff2" crossorigin="anonymous">
<link rel="preload" href="fonts/cicle_fina-webfont.woff" as="font" type="font/woff" crossorigin="anonymous">
<link rel="preload" href="fonts/cicle_fina-webfont.ttf" as="font" type="font/ttf" crossorigin="anonymous">
<link rel="preload" href="fonts/cicle_fina-webfont.svg" as="font" type="image/svg+xml" crossorigin="anonymous">

<link rel="preload" href="fonts/zantroke-webfont.eot" as="font" type="application/vnd.ms-fontobject" crossorigin="anonymous">
<link rel="preload" href="fonts/zantroke-webfont.woff2" as="font" type="font/woff2" crossorigin="anonymous">
<link rel="preload" href="fonts/zantroke-webfont.woff" as="font" type="font/woff" crossorigin="anonymous">
<link rel="preload" href="fonts/zantroke-webfont.ttf" as="font" type="font/ttf" crossorigin="anonymous">
<link rel="preload" href="fonts/zantroke-webfont.svg" as="font" type="image/svg+xml" crossorigin="anonymous"> */}